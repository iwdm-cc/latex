package com.ruoyi.web.controller.map;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.TemplateExportUtil;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MapMarks;
import com.ruoyi.system.domain.vo.levelAnalysisQueryVo;
import com.ruoyi.system.service.IMapMarksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 企业管理Controller
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@RestController
@RequestMapping("/system/marks")
public class MapMarksController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(MapMarksController.class);
    @Autowired
    private IMapMarksService mapMarksService;


    @Autowired
    private TemplateExportUtil templateExportUtil;

    @GetMapping("/query")
    public TableDataInfo query(MapMarks mapMarks) {
        List<MapMarks> list = mapMarksService.queryMapMarksList(mapMarks);
        return getDataTable(list);
    }
    //三级界面
    @GetMapping(value = "/detailQuery/{marksId}")
    public AjaxResult getInfoModel(@PathVariable("marksId") Long marksId) {
        return AjaxResult.success(mapMarksService.selectMapMarksByMarksId(marksId));
    }




    /*
     * # 统计企业星级
     * */
    @GetMapping("/levelAnalysisQuery")
    public AjaxResult levelAnalysisQuery(MapMarks mapMarks) {
        startPage();
        List<levelAnalysisQueryVo> list = mapMarksService.levelAnalysisQuery(mapMarks);
        return AjaxResult.success(list);
    }

    /*
     * # 统计企业总数
     * */
    @GetMapping("/quantityQuery")
    public AjaxResult quantityQuery(MapMarks mapMarks) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("amount", mapMarksService.quantityQuery(mapMarks));
        return ajaxResult;
    }

    /*
     * #企业个数统计
     * */
    @GetMapping("/statisticsQuery")
    public AjaxResult statisticsQuery(MapMarks mapMarks) {
        List<levelAnalysisQueryVo> list = mapMarksService.statisticsQuery(mapMarks);
        return AjaxResult.success(list);
    }

    /*
     * 制造业 企业个数统计
     * */
    @GetMapping("/manufacturingQuery")
    public AjaxResult manufacturingQuery(MapMarks mapMarks) {
        List<levelAnalysisQueryVo> list = mapMarksService.manufacturingQuery(mapMarks);
        return AjaxResult.success(list);
    }

    /**
     * 查询企业管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:marks:list')")
    @GetMapping("/list")
    public TableDataInfo list(MapMarks mapMarks) {
        startPage();
        List<MapMarks> list = mapMarksService.selectMapMarksList(mapMarks);
        return getDataTable(list);
    }


    /**
     * 导出企业管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:marks:export')")
    @Log(title = "企业管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapMarks mapMarks) {
        List<MapMarks> list = mapMarksService.selectMapMarksList(mapMarks);
        ExcelUtil<MapMarks> util = new ExcelUtil<>(MapMarks.class);
        util.exportExcel(response, list, "企业管理数据");
    }


    @RequestMapping("/importTemplate")
    public void importTemplate(String fileName, HttpServletResponse response, HttpServletRequest request) {

        try {
            if (!FileUtils.checkAllowDownload(fileName)) {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            templateExportUtil.exportExcel(response, request,fileName);
        } catch (Exception e) {
            log.error("下载文件失败", e);
        }
    }

    @Log(title = "企业管理批量导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:marks:add')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<MapMarks> util = new ExcelUtil<>(MapMarks.class);
        List<MapMarks> baseList = util.importExcel(file.getInputStream());
        String message = mapMarksService.importData(baseList, SecurityUtils.getLoginUser().getUsername());
        return AjaxResult.success(message);
    }




    /**
     * 获取企业管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:marks:query')")
    @GetMapping(value = "/{marksId}")
    public AjaxResult getInfo(@PathVariable("marksId") Long marksId) {
        return AjaxResult.success(mapMarksService.selectMapMarksByMarksId(marksId));
    }


    /**
     * 新增企业管理
     */
    @PreAuthorize("@ss.hasPermi('system:marks:add')")
    @Log(title = "企业管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapMarks mapMarks) {
        mapMarksService.insertMapMarks(mapMarks);
        return AjaxResult.success(mapMarks.getMarksId());

    }

    /**
     * 修改企业管理
     */
    @PreAuthorize("@ss.hasPermi('system:marks:edit')")
    @Log(title = "企业管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapMarks mapMarks) {
        return toAjax(mapMarksService.updateMapMarks(mapMarks));
    }

    /**
     * 删除企业管理
     */
    @PreAuthorize("@ss.hasPermi('system:marks:remove')")
    @Log(title = "企业管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{marksIds}")
    public AjaxResult remove(@PathVariable Long[] marksIds) {
        return toAjax(mapMarksService.deleteMapMarksByMarksIds(marksIds));
    }
}
