package com.ruoyi.web.controller.map;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MapBanner;
import com.ruoyi.system.service.IMapBannerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 轮播图Controller
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@RestController
@RequestMapping("/system/banner")
public class MapBannerController extends BaseController {
    @Autowired
    private IMapBannerService mapBannerService;


    /**
     * 查询 地图轮播图
     */
    @GetMapping("/query")
    public AjaxResult query(MapBanner mapBanner) {
        startPage();
        List<MapBanner> list = mapBannerService.queryMapBannerList(mapBanner);
        return AjaxResult.success(list);
    }

    /**
     * 查询轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('system:banner:list')")
    @GetMapping("/list")
    public TableDataInfo list(MapBanner mapBanner) {
        startPage();
        List<MapBanner> list = mapBannerService.selectMapBannerList(mapBanner);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('system:banner:export')")
    @Log(title = "轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapBanner mapBanner) {
        List<MapBanner> list = mapBannerService.selectMapBannerList(mapBanner);
        ExcelUtil<MapBanner> util = new ExcelUtil<>(MapBanner.class);
        util.exportExcel(response, list, "轮播图数据");
    }

    /**
     * 获取轮播图详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:banner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(mapBannerService.selectMapBannerById(id));
    }

    /**
     * 新增轮播图
     */
    @PreAuthorize("@ss.hasPermi('system:banner:add')")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapBanner mapBanner) {
        if (!StringUtils.isEmpty(mapBanner.getBannerLink())) {
            if (!StringUtils.ishttp(mapBanner.getBannerLink())) {
                return AjaxResult.error("新增失败，地址必须以http(s)://开头");
            }
        }
        mapBannerService.insertMapBanner(mapBanner);
        return AjaxResult.success(mapBanner.getId());
    }

    /**
     * 修改轮播图
     */
    @PreAuthorize("@ss.hasPermi('system:banner:edit')")
    @Log(title = "轮播图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapBanner mapBanner) {
        return toAjax(mapBannerService.updateMapBanner(mapBanner));
    }

    /**
     * 删除轮播图
     */
    @PreAuthorize("@ss.hasPermi('system:banner:remove')")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(mapBannerService.deleteMapBannerByIds(ids));
    }
}
