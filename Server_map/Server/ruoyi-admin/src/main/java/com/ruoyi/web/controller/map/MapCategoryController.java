package com.ruoyi.web.controller.map;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.domain.entity.MapCategory;
import com.ruoyi.system.service.IMapCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 企业类别Controller
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@RestController
@RequestMapping("/system/category")
public class MapCategoryController extends BaseController {
    @Autowired
    private IMapCategoryService mapCategoryService;

    /**
     * 查询企业类别列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:list')")
    @GetMapping("/list")
    public AjaxResult list(MapCategory mapCategory) {
        List<MapCategory> list = mapCategoryService.selectMapCategoryList(mapCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出企业类别列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:export')")
    @Log(title = "企业类别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapCategory mapCategory) {
        List<MapCategory> list = mapCategoryService.selectMapCategoryList(mapCategory);
        ExcelUtil<MapCategory> util = new ExcelUtil<>(MapCategory.class);
        util.exportExcel(response, list, "企业类别数据");
    }

    /**
     * 获取下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(MapCategory mapCategory)
    {
        List<MapCategory> mapCategoryList = mapCategoryService.selectMapCategoryList(mapCategory);
        return AjaxResult.success(mapCategoryService.buildTreeSelect(mapCategoryList));
    }
    /**
     * 获取企业类别详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:category:query')")
    @GetMapping(value = "/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId) {
        return AjaxResult.success(mapCategoryService.selectMapCategoryByCategoryId(categoryId));
    }

    /**
     * 新增企业类别
     */
    @PreAuthorize("@ss.hasPermi('system:category:add')")
    @Log(title = "企业类别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapCategory mapCategory) {
        return toAjax(mapCategoryService.insertMapCategory(mapCategory));
    }

    /**
     * 修改企业类别
     */
    @PreAuthorize("@ss.hasPermi('system:category:edit')")
    @Log(title = "企业类别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapCategory mapCategory) {
        return toAjax(mapCategoryService.updateMapCategory(mapCategory));
    }

    /**
     * 删除企业类别
     */
    @PreAuthorize("@ss.hasPermi('system:category:remove')")
    @Log(title = "企业类别", businessType = BusinessType.DELETE)
    @DeleteMapping("/{categoryIds}")
    public AjaxResult remove(@PathVariable Long[] categoryIds) {
        return toAjax(mapCategoryService.deleteMapCategoryByCategoryIds(categoryIds));
    }
}
