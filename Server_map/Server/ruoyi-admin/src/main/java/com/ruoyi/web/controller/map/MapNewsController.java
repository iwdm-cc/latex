package com.ruoyi.web.controller.map;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MapNews;
import com.ruoyi.system.service.IMapNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 新闻Controller
 *
 * @author cqust_icc
 * @date 2022-04-19
 */
@RestController
@RequestMapping("/system/news")
public class MapNewsController extends BaseController {
    @Autowired
    private IMapNewsService mapNewsService;


    @GetMapping("/query")
    public TableDataInfo query(MapNews mapNews) {
        startPage();
        List<MapNews> list = mapNewsService.selectMapNewsList(mapNews);
        return getDataTable(list);
    }

    /**
     * 查询新闻列表
     */
    @PreAuthorize("@ss.hasPermi('system:news:list')")
    @GetMapping("/list")
    public TableDataInfo list(MapNews mapNews) {
        startPage();
        List<MapNews> list = mapNewsService.selectMapNewsList(mapNews);
        return getDataTable(list);
    }

    /**
     * 导出新闻列表
     */
    @PreAuthorize("@ss.hasPermi('system:news:export')")
    @Log(title = "新闻", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapNews mapNews) {
        List<MapNews> list = mapNewsService.selectMapNewsList(mapNews);
        ExcelUtil<MapNews> util = new ExcelUtil<MapNews>(MapNews.class);
        util.exportExcel(response, list, "新闻数据");
    }

    /**
     * 获取新闻详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:news:query')")
    @GetMapping(value = "/{newsId}")
    public AjaxResult getInfo(@PathVariable("newsId") Integer newsId) {
        return AjaxResult.success(mapNewsService.selectMapNewsByNewsId(newsId));
    }

    /**
     * 新增新闻
     */
    @PreAuthorize("@ss.hasPermi('system:news:add')")
    @Log(title = "新闻", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody MapNews mapNews) {
        return toAjax(mapNewsService.insertMapNews(mapNews));
    }

    /**
     * 修改新闻
     */
    @PreAuthorize("@ss.hasPermi('system:news:edit')")
    @Log(title = "新闻", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody MapNews mapNews) {
        return toAjax(mapNewsService.updateMapNews(mapNews));
    }

    /**
     * 删除新闻
     */
    @PreAuthorize("@ss.hasPermi('system:news:remove')")
    @Log(title = "新闻", businessType = BusinessType.DELETE)
    @DeleteMapping("/{newsIds}")
    public AjaxResult remove(@PathVariable Integer[] newsIds) {
        return toAjax(mapNewsService.deleteMapNewsByNewsIds(newsIds));
    }
}
