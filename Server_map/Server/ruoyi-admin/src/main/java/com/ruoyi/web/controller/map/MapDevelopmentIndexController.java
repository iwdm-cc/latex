package com.ruoyi.web.controller.map;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MapDevelopmentIndex;
import com.ruoyi.system.service.IMapDevelopmentIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 产业发展指数Controller
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
@RestController
@RequestMapping("/system/DevelopmentIndex")
public class MapDevelopmentIndexController extends BaseController
{
    @Autowired
    private IMapDevelopmentIndexService mapDevelopmentIndexService;

    /**
     * 查询产业发展指数列表
     */
//    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:list')")
    @GetMapping("/list")
    public TableDataInfo list(MapDevelopmentIndex mapDevelopmentIndex)
    {
        startPage();
        List<MapDevelopmentIndex> list = mapDevelopmentIndexService.selectMapDevelopmentIndexList(mapDevelopmentIndex);
        return getDataTable(list);
    }
//    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:list')")
    @GetMapping("/listAllCity")
    public TableDataInfo listAllCity(MapDevelopmentIndex mapDevelopmentIndex)
    {
        startPage();
        List<MapDevelopmentIndex> list = mapDevelopmentIndexService.selectMapDevelopmentIndexAllCity(mapDevelopmentIndex);
        return getDataTable(list);
    }

    /**
     * 导出产业发展指数列表
     */
    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:export')")
    @Log(title = "产业发展指数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapDevelopmentIndex mapDevelopmentIndex)
    {
        List<MapDevelopmentIndex> list = mapDevelopmentIndexService.selectMapDevelopmentIndexList(mapDevelopmentIndex);
        ExcelUtil<MapDevelopmentIndex> util = new ExcelUtil<MapDevelopmentIndex>(MapDevelopmentIndex.class);
        util.exportExcel(response, list, "产业发展指数数据");
    }

    /**
     * 获取产业发展指数详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mapDevelopmentIndexService.selectMapDevelopmentIndexById(id));
    }

    /**
     * 新增产业发展指数
     */
    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:add')")
    @Log(title = "产业发展指数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapDevelopmentIndex mapDevelopmentIndex)
    {
        return toAjax(mapDevelopmentIndexService.insertMapDevelopmentIndex(mapDevelopmentIndex));
    }

    /**
     * 修改产业发展指数
     */
    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:edit')")
    @Log(title = "产业发展指数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapDevelopmentIndex mapDevelopmentIndex)
    {
        return toAjax(mapDevelopmentIndexService.updateMapDevelopmentIndex(mapDevelopmentIndex));
    }

    /**
     * 删除产业发展指数
     */
    @PreAuthorize("@ss.hasPermi('system:DevelopmentIndex:remove')")
    @Log(title = "产业发展指数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mapDevelopmentIndexService.deleteMapDevelopmentIndexByIds(ids));
    }
    @Log(title = "企业管理批量导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:marks:add')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        ExcelUtil<MapDevelopmentIndex> util = new ExcelUtil<>(MapDevelopmentIndex.class);
        List<MapDevelopmentIndex> baseList = util.importExcel("首页",file.getInputStream(),0);
        String message = mapDevelopmentIndexService.importData(baseList);
        return AjaxResult.success(message);
    }
}
