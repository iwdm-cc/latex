package com.ruoyi.web.controller.map;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MapLinks;
import com.ruoyi.system.service.IMapLinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 底部友情链接Controller
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
@RestController
@RequestMapping("/system/links")
public class MapLinksController extends BaseController
{
    @Autowired
    private IMapLinksService mapLinksService;

    /**
     * 查询底部友情链接列表
     */
//    @PreAuthorize("@ss.hasPermi('system:links:list')")
    @GetMapping("/list")
    public TableDataInfo list(MapLinks mapLinks)
    {
        startPage();
        List<MapLinks> list = mapLinksService.selectMapLinksList(mapLinks);
        return getDataTable(list);
    }

    /**
     * 导出底部友情链接列表
     */
    @PreAuthorize("@ss.hasPermi('system:links:export')")
    @Log(title = "底部友情链接", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapLinks mapLinks)
    {
        List<MapLinks> list = mapLinksService.selectMapLinksList(mapLinks);
        ExcelUtil<MapLinks> util = new ExcelUtil<MapLinks>(MapLinks.class);
        util.exportExcel(response, list, "底部友情链接数据");
    }

    /**
     * 获取底部友情链接详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:links:query')")
    @GetMapping(value = "/{linkId}")
    public AjaxResult getInfo(@PathVariable("linkId") Long linkId)
    {
        return AjaxResult.success(mapLinksService.selectMapLinksByLinkId(linkId));
    }

    /**
     * 新增底部友情链接
     */
    @PreAuthorize("@ss.hasPermi('system:links:add')")
    @Log(title = "底部友情链接", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapLinks mapLinks)
    {
        return toAjax(mapLinksService.insertMapLinks(mapLinks));
    }

    /**
     * 修改底部友情链接
     */
    @PreAuthorize("@ss.hasPermi('system:links:edit')")
    @Log(title = "底部友情链接", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapLinks mapLinks)
    {
        return toAjax(mapLinksService.updateMapLinks(mapLinks));
    }

    /**
     * 删除底部友情链接
     */
    @PreAuthorize("@ss.hasPermi('system:links:remove')")
    @Log(title = "底部友情链接", businessType = BusinessType.DELETE)
	@DeleteMapping("/{linkIds}")
    public AjaxResult remove(@PathVariable Long[] linkIds)
    {
        return toAjax(mapLinksService.deleteMapLinksByLinkIds(linkIds));
    }
}
