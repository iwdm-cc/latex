package com.ruoyi.web.controller.map;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.MapCity;
import com.ruoyi.system.service.IMapCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 城市管理Controller
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
@RestController
@RequestMapping("/system/city")
public class MapCityController extends BaseController
{
    @Autowired
    private IMapCityService mapCityService;

    /**
     * 查询城市管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:city:list')")
    @GetMapping("/list")
    public TableDataInfo list(MapCity mapCity)
    {
        startPage();
        List<MapCity> list = mapCityService.selectMapCityList(mapCity);
        return getDataTable(list);
    }

    /**
     * 导出城市管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:city:export')")
    @Log(title = "城市管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapCity mapCity)
    {
        List<MapCity> list = mapCityService.selectMapCityList(mapCity);
        ExcelUtil<MapCity> util = new ExcelUtil<MapCity>(MapCity.class);
        util.exportExcel(response, list, "城市管理数据");
    }

    /**
     * 获取城市管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:city:query')")
    @GetMapping(value = "/{cityCode}")
    public AjaxResult getInfo(@PathVariable("cityCode") String cityCode)
    {
        return AjaxResult.success(mapCityService.selectMapCityByCityCode(cityCode));
    }

    /**
     * 新增城市管理
     */
    @PreAuthorize("@ss.hasPermi('system:city:add')")
    @Log(title = "城市管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapCity mapCity)
    {
        return toAjax(mapCityService.insertMapCity(mapCity));
    }

    /**
     * 修改城市管理
     */
    @PreAuthorize("@ss.hasPermi('system:city:edit')")
    @Log(title = "城市管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapCity mapCity)
    {
        return toAjax(mapCityService.updateMapCity(mapCity));
    }

    /**
     * 删除城市管理
     */
    @PreAuthorize("@ss.hasPermi('system:city:remove')")
    @Log(title = "城市管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cityCodes}")
    public AjaxResult remove(@PathVariable String[] cityCodes)
    {
        return toAjax(mapCityService.deleteMapCityByCityCodes(cityCodes));
    }
}
