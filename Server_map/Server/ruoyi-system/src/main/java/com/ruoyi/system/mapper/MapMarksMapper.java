package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.MapMarks;
import com.ruoyi.system.domain.vo.MapMarksDetailVo;
import com.ruoyi.system.domain.vo.levelAnalysisQueryVo;

import java.util.List;

/**
 * 企业管理Mapper接口
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
public interface MapMarksMapper {
    /**
     * 查询企业管理
     *
     * @param marksId 企业管理主键
     * @return 企业管理
     */
    public MapMarksDetailVo selectMapMarksByMarksId(Long marksId);

    /**
     * 查询企业管理列表
     *
     * @param mapMarks 企业管理
     * @return 企业管理集合
     */
    public List<MapMarks> selectMapMarksList(MapMarks mapMarks);

    /**
     * 新增企业管理
     *
     * @param mapMarks 企业管理
     * @return 结果
     */
    public int insertMapMarks(MapMarks mapMarks);

    /**
     * 修改企业管理
     *
     * @param mapMarks 企业管理
     * @return 结果
     */
    public int updateMapMarks(MapMarks mapMarks);

    /**
     * 删除企业管理
     *
     * @param marksId 企业管理主键
     * @return 结果
     */
    public int deleteMapMarksByMarksId(Long marksId);

    /**
     * 批量删除企业管理
     *
     * @param marksIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapMarksByMarksIds(Long[] marksIds);

    List<levelAnalysisQueryVo> levelAnalysisQuery(MapMarks mapMarks);

    long quantityQuery(MapMarks mapMarks);

    List<levelAnalysisQueryVo> statisticsQuery(MapMarks mapMarks);

    List<levelAnalysisQueryVo> manufacturingQuery(MapMarks mapMarks);

    List<MapMarks> queryMapMarksList(MapMarks mapMarks);
}
