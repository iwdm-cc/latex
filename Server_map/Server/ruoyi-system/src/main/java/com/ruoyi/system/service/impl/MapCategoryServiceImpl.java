package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MapCategoryMapper;
import com.ruoyi.common.core.domain.entity.MapCategory;
import com.ruoyi.system.service.IMapCategoryService;

/**
 * 企业类别Service业务层处理
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Service
public class MapCategoryServiceImpl implements IMapCategoryService
{
    @Autowired
    private MapCategoryMapper mapCategoryMapper;

    /**
     * 查询企业类别
     *
     * @param categoryId 企业类别主键
     * @return 企业类别
     */
    @Override
    public MapCategory selectMapCategoryByCategoryId(Long categoryId)
    {
        return mapCategoryMapper.selectMapCategoryByCategoryId(categoryId);
    }

    /**
     * 查询企业类别列表
     *
     * @param mapCategory 企业类别
     * @return 企业类别
     */
    @Override
    public List<MapCategory> selectMapCategoryList(MapCategory mapCategory)
    {
        return mapCategoryMapper.selectMapCategoryList(mapCategory);
    }

    /**
     * 新增企业类别
     *
     * @param mapCategory 企业类别
     * @return 结果
     */
    @Override
    public int insertMapCategory(MapCategory mapCategory)
    {
        mapCategory.setCreateTime(DateUtils.getNowDate());
        return mapCategoryMapper.insertMapCategory(mapCategory);
    }

    /**
     * 修改企业类别
     *
     * @param mapCategory 企业类别
     * @return 结果
     */
    @Override
    public int updateMapCategory(MapCategory mapCategory)
    {
        mapCategory.setUpdateTime(DateUtils.getNowDate());
        return mapCategoryMapper.updateMapCategory(mapCategory);
    }

    /**
     * 批量删除企业类别
     *
     * @param categoryIds 需要删除的企业类别主键
     * @return 结果
     */
    @Override
    public int deleteMapCategoryByCategoryIds(Long[] categoryIds)
    {
        return mapCategoryMapper.deleteMapCategoryByCategoryIds(categoryIds);
    }

    /**
     * 删除企业类别信息
     *
     * @param categoryId 企业类别主键
     * @return 结果
     */
    @Override
    public int deleteMapCategoryByCategoryId(Long categoryId)
    {
        return mapCategoryMapper.deleteMapCategoryByCategoryId(categoryId);
    }
    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    /**
     * 递归列表
     */
    private void recursionFn(List<MapCategory> list, MapCategory t)
    {
        // 得到子节点列表
        List<MapCategory> childList = getChildList(list, t);
        t.setChildren(childList);
        for (MapCategory tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }
    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<MapCategory> list, MapCategory t)
    {
        return getChildList(list, t).size() > 0;
    }
    /**
     * 得到子节点列表
     */
    private List<MapCategory> getChildList(List<MapCategory> list, MapCategory t)
    {
        List<MapCategory> tlist = new ArrayList<>();
        Iterator<MapCategory> it = list.iterator();
        while (it.hasNext())
        {
            MapCategory n = (MapCategory) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getCategoryId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }
    public List<MapCategory> buildDeptTree(List<MapCategory> depts)
    {
        List<MapCategory> returnList = new ArrayList<>();
        List<Long> tempList = new ArrayList<Long>();
        for (MapCategory dept : depts)
        {
            tempList.add(dept.getCategoryId());
        }
        for (MapCategory dept : depts)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId()))
            {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = depts;
        }
        return returnList;
    }
    @Override
    public List<TreeSelect> buildTreeSelect(List<MapCategory> mapCategoryList) {
        List<MapCategory> deptTrees = buildDeptTree(mapCategoryList);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }
}
