package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.MapDevelopmentIndex;

import java.util.List;

/**
 * 产业发展指数Mapper接口
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
public interface MapDevelopmentIndexMapper
{
    /**
     * 查询产业发展指数
     *
     * @param id 产业发展指数主键
     * @return 产业发展指数
     */
    public MapDevelopmentIndex selectMapDevelopmentIndexById(Long id);

    /**
     * 查询产业发展指数列表
     *
     * @param mapDevelopmentIndex 产业发展指数
     * @return 产业发展指数集合
     */
    public List<MapDevelopmentIndex> selectMapDevelopmentIndexList(MapDevelopmentIndex mapDevelopmentIndex);

    /**
     * 新增产业发展指数
     *
     * @param mapDevelopmentIndex 产业发展指数
     * @return 结果
     */
    public int insertMapDevelopmentIndex(MapDevelopmentIndex mapDevelopmentIndex);

    /**
     * 修改产业发展指数
     *
     * @param mapDevelopmentIndex 产业发展指数
     * @return 结果
     */
    public int updateMapDevelopmentIndex(MapDevelopmentIndex mapDevelopmentIndex);

    /**
     * 删除产业发展指数
     *
     * @param id 产业发展指数主键
     * @return 结果
     */
    public int deleteMapDevelopmentIndexById(Long id);

    /**
     * 批量删除产业发展指数
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapDevelopmentIndexByIds(Long[] ids);

    List<MapDevelopmentIndex> selectMapDevelopmentIndexAllCity(MapDevelopmentIndex mapDevelopmentIndex);
}
