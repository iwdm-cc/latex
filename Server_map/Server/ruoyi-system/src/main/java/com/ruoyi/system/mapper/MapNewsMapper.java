package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MapNews;

/**
 * 新闻Mapper接口
 * 
 * @author cqust_icc
 * @date 2022-04-19
 */
public interface MapNewsMapper 
{
    /**
     * 查询新闻
     * 
     * @param newsId 新闻主键
     * @return 新闻
     */
    public MapNews selectMapNewsByNewsId(Integer newsId);

    /**
     * 查询新闻列表
     * 
     * @param mapNews 新闻
     * @return 新闻集合
     */
    public List<MapNews> selectMapNewsList(MapNews mapNews);

    /**
     * 新增新闻
     * 
     * @param mapNews 新闻
     * @return 结果
     */
    public int insertMapNews(MapNews mapNews);

    /**
     * 修改新闻
     * 
     * @param mapNews 新闻
     * @return 结果
     */
    public int updateMapNews(MapNews mapNews);

    /**
     * 删除新闻
     * 
     * @param newsId 新闻主键
     * @return 结果
     */
    public int deleteMapNewsByNewsId(Integer newsId);

    /**
     * 批量删除新闻
     * 
     * @param newsIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapNewsByNewsIds(Integer[] newsIds);
}
