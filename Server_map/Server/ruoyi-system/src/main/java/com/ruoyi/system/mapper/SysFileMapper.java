package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SysUploadFile;

/**
 * 附件信息Mapper接口
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
public interface SysFileMapper
{
    /**
     * 查询附件信息
     *
     * @param fileId 附件信息主键
     * @return 附件信息
     */
    public SysUploadFile selectSysFileByFileId(Long fileId);

    /**
     * 查询附件信息列表
     *
     * @param sysUploadFile 附件信息
     * @return 附件信息集合
     */
    public List<SysUploadFile> selectSysFileList(SysUploadFile sysUploadFile);

    /**
     * 新增附件信息
     *
     * @param sysUploadFile 附件信息
     * @return 结果
     */
    public int insertSysFile(SysUploadFile sysUploadFile);

    /**
     * 修改附件信息
     *
     * @param sysUploadFile 附件信息
     * @return 结果
     */
    public int updateSysFile(SysUploadFile sysUploadFile);

    /**
     * 删除附件信息
     *
     * @param fileId 附件信息主键
     * @return 结果
     */
    public int deleteSysFileByFileId(Long fileId);

    /**
     * 批量删除附件信息
     *
     * @param fileIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysFileByFileIds(Long[] fileIds);





    void deleteSysFileByMarks(Map<String, Object> param);
}
