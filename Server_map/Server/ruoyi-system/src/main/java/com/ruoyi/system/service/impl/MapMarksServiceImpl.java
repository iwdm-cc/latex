package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.MapCategory;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.MapMarks;
import com.ruoyi.system.domain.SysUploadFile;
import com.ruoyi.system.domain.UploadFileType;
import com.ruoyi.system.domain.vo.MapMarksDetailVo;
import com.ruoyi.system.domain.vo.levelAnalysisQueryVo;
import com.ruoyi.system.mapper.MapCategoryMapper;
import com.ruoyi.system.mapper.MapMarksMapper;
import com.ruoyi.system.mapper.SysFileMapper;
import com.ruoyi.system.service.IMapMarksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 企业管理Service业务层处理
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Service
public class MapMarksServiceImpl implements IMapMarksService {
    @Autowired
    private MapMarksMapper mapMarksMapper;

    @Autowired
    private MapCategoryMapper mapCategoryMapper;

    @Autowired
    private SysFileMapper sysFileMapper;

    /**
     * 查询企业管理
     *
     * @param marksId 企业管理主键
     * @return 企业管理
     */
    @Override
    public MapMarksDetailVo selectMapMarksByMarksId(Long marksId) {
        List<SysUploadFile> bannerFile = sysFileMapper.selectSysFileList(new SysUploadFile(UploadFileType.MARKS_BANNER,marksId));
        List<SysUploadFile> imageFile = sysFileMapper.selectSysFileList(new SysUploadFile(UploadFileType.MARKS_PICTURE,marksId));
        List<SysUploadFile> modelFile = sysFileMapper.selectSysFileList(new SysUploadFile(UploadFileType.MARKS_MODEL,marksId));

        MapMarksDetailVo marksDetailVo = mapMarksMapper.selectMapMarksByMarksId(marksId);

        if (imageFile != null) {
            marksDetailVo.setImageFile(imageFile.stream().map(v-> new SysUploadFile.SysFileVo(v.getFilePath(),v.getFileId())).collect(Collectors.toList()));
        }
        if (modelFile != null) {
            marksDetailVo.setModelFile(modelFile.stream().map(v-> new SysUploadFile.SysFileVo(v.getFilePath(),v.getFileId())).collect(Collectors.toList()));
        }

        if (marksDetailVo != null) {
            marksDetailVo.setBannerFile(bannerFile.stream().map(v-> new SysUploadFile.SysFileVo(v.getFilePath(),v.getFileId())).collect(Collectors.toList()));
        }
        return marksDetailVo;
    }

    /**
     * 查询企业管理列表
     *
     * @param mapMarks 企业管理
     * @return 企业管理
     */
    @Override
    public List<MapMarks> selectMapMarksList(MapMarks mapMarks) {
        return mapMarksMapper.selectMapMarksList(mapMarks);
    }

    /**
     * 新增企业管理
     *
     * @param mapMarks 企业管理
     * @return 结果
     */
    @Override
    public int insertMapMarks(MapMarks mapMarks) {
        mapMarks.setCreateTime(DateUtils.getNowDate());
        return mapMarksMapper.insertMapMarks(mapMarks);
    }

    /**
     * 修改企业管理
     *
     * @param mapMarks 企业管理
     * @return 结果
     */
    @Override
    public int updateMapMarks(MapMarks mapMarks) {
        mapMarks.setUpdateTime(DateUtils.getNowDate());
        return mapMarksMapper.updateMapMarks(mapMarks);
    }

    /**
     * 批量删除企业管理
     *
     * @param marksIds 需要删除的企业管理主键
     * @return 结果
     */
    @Override
    public int deleteMapMarksByMarksIds(Long[] marksIds) {
        return mapMarksMapper.deleteMapMarksByMarksIds(marksIds);
    }

    /**
     * 删除企业管理信息
     *
     * @param marksId 企业管理主键
     * @return 结果
     */
    @Override
    public int deleteMapMarksByMarksId(Long marksId) {
        return mapMarksMapper.deleteMapMarksByMarksId(marksId);
    }

    @Override
    @Transactional
    public String importData(List<MapMarks> mapMarksList, String username) {

        List<MapCategory> mapCategory = mapCategoryMapper.selectMapCategoryList(null);
        StringBuilder stringBuilder = new StringBuilder();
        for (MapMarks mapMarks : mapMarksList) {
            if (!StringUtils.isEmpty(mapMarks.getCompanyName())) {

                List<MapCategory> list = mapCategory.stream().filter(item -> item.getCategoryName().trim().equals(mapMarks.getCategoryName().trim())).collect(Collectors.toList());
                try {
                    mapMarks.setCategoryId(list.get(0).getCategoryId());
                } catch (Exception e) {
                    System.out.println("类型不存在" + e.getMessage());
                    stringBuilder.append(mapMarks.getCompanyName()).append(":");
                    stringBuilder.append(mapMarks.getCompanyName());
                    stringBuilder.append("类型异常");
                    return stringBuilder.toString();

//                 throw new RuntimeException(e);
                }
                insertMapMarks(mapMarks);
                System.out.println(mapMarks);
//             stringBuilder.append(mapMarks);
//             stringBuilder.append("</br>");
            }


        }


        return stringBuilder.toString();
    }

    @Override
    public List<levelAnalysisQueryVo> levelAnalysisQuery(MapMarks mapMarks) {
        return mapMarksMapper.levelAnalysisQuery(mapMarks);
    }

    @Override
    public long quantityQuery(MapMarks mapMarks) {
        return mapMarksMapper.quantityQuery(mapMarks);
    }

    @Override
    public List<levelAnalysisQueryVo> statisticsQuery(MapMarks mapMarks) {
        return mapMarksMapper.statisticsQuery(mapMarks);
    }

    @Override
    public List<levelAnalysisQueryVo> manufacturingQuery(MapMarks mapMarks) {
        return mapMarksMapper.manufacturingQuery(mapMarks);
    }

    @Override
    public List<MapMarks> queryMapMarksList(MapMarks mapMarks) {
        return mapMarksMapper.queryMapMarksList(mapMarks);
    }
}
