package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.MapDevelopmentIndex;
import com.ruoyi.system.mapper.MapDevelopmentIndexMapper;
import com.ruoyi.system.service.IMapDevelopmentIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 产业发展指数Service业务层处理
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
@Service
public class MapDevelopmentIndexServiceImpl implements IMapDevelopmentIndexService
{
    @Autowired
    private MapDevelopmentIndexMapper mapDevelopmentIndexMapper;

    /**
     * 查询产业发展指数
     *
     * @param id 产业发展指数主键
     * @return 产业发展指数
     */
    @Override
    public MapDevelopmentIndex selectMapDevelopmentIndexById(Long id)
    {
        return mapDevelopmentIndexMapper.selectMapDevelopmentIndexById(id);
    }

    /**
     * 查询产业发展指数列表
     *
     * @param mapDevelopmentIndex 产业发展指数
     * @return 产业发展指数
     */
    @Override
    public List<MapDevelopmentIndex> selectMapDevelopmentIndexList(MapDevelopmentIndex mapDevelopmentIndex)
    {
        return mapDevelopmentIndexMapper.selectMapDevelopmentIndexList(mapDevelopmentIndex);
    }

    /**
     * 新增产业发展指数
     *
     * @param mapDevelopmentIndex 产业发展指数
     * @return 结果
     */
    @Override
    public int insertMapDevelopmentIndex(MapDevelopmentIndex mapDevelopmentIndex)
    {
        return mapDevelopmentIndexMapper.insertMapDevelopmentIndex(mapDevelopmentIndex);
    }

    /**
     * 修改产业发展指数
     *
     * @param mapDevelopmentIndex 产业发展指数
     * @return 结果
     */
    @Override
    public int updateMapDevelopmentIndex(MapDevelopmentIndex mapDevelopmentIndex)
    {
        return mapDevelopmentIndexMapper.updateMapDevelopmentIndex(mapDevelopmentIndex);
    }

    /**
     * 批量删除产业发展指数
     *
     * @param ids 需要删除的产业发展指数主键
     * @return 结果
     */
    @Override
    public int deleteMapDevelopmentIndexByIds(Long[] ids)
    {
        return mapDevelopmentIndexMapper.deleteMapDevelopmentIndexByIds(ids);
    }

    /**
     * 删除产业发展指数信息
     *
     * @param id 产业发展指数主键
     * @return 结果
     */
    @Override
    public int deleteMapDevelopmentIndexById(Long id)
    {
        return mapDevelopmentIndexMapper.deleteMapDevelopmentIndexById(id);
    }

    @Override
    public List<MapDevelopmentIndex> selectMapDevelopmentIndexAllCity(MapDevelopmentIndex mapDevelopmentIndex) {
        return mapDevelopmentIndexMapper.selectMapDevelopmentIndexAllCity(mapDevelopmentIndex);
    }

    @Override
    @Transactional
    public String importData(List<MapDevelopmentIndex> baseList) {

        List<MapDevelopmentIndex> mapDevelopmentIndices = mapDevelopmentIndexMapper.selectMapDevelopmentIndexAllCity(null);

        for (MapDevelopmentIndex item : baseList) {

            String trim = item.getCityName().trim();

            /*
            * 转换城市code
            * */
            boolean flag = true;
            for (MapDevelopmentIndex developmentIndex : mapDevelopmentIndices) {
                if (developmentIndex.getCityName().contains(trim)) {
                    flag = false;
                    item.setCityCode(developmentIndex.getCityCode());
                    break;
                }
            }

            if(flag){
                item.setCityCode("500000");
            }

            insertMapDevelopmentIndex(item);

        }

     return "导入成功！";
    }
}
