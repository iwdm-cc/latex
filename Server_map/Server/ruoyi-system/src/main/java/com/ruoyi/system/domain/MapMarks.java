package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 企业管理对象 map_marks
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Data
@ToString
public class MapMarks extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long marksId;
    private Long talentAmount;

    /**
     * 公司名称
     */
    @Excel(name = "企业名称")
    private String companyName;

    @Excel(name = "地址")
    private String companyAddress;
    /**
     * 公司简介
     */
    @Excel(name = "公司简介")
    private String companyProfile;

    @Excel(name = "联系人")
    private String companyPrincipal;

    @Excel(name = "联系方式")
    private String companyPhone;

    @Excel(name = "终端产品")
    private String companyProducts;

    @Excel(name = "供应量")
    private String designCapacity;

    @Excel(name = "市场区域")
    private String marketArea;

    @Excel(name = "实际产能")
    private String actualCapacity;



    /**
     * 公司星级
     */
    @Excel(name = "公司星级")
    private BigDecimal companyLevel;

    /**
     * 坐标经度
     */
    @Excel(name = "坐标经度")
    private BigDecimal longitude;

    /**
     * 坐标纬度
     */
    @Excel(name = "坐标纬度")
    private BigDecimal latitude;

    @Excel(name = "城市code")
    private String cityCode;

    /**
     * 工厂类别
     */

    private Long categoryId;

    @Excel(name = "类型标签")
    private String categoryName;

    private Long categoryParentId;

    private Long categoryColorIndex;

    private Long categoryParentColorIndex;

}
