package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 附件信息对象 sys_file
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
public class SysUploadFile implements Serializable {
    public SysUploadFile() {

    }

    public SysUploadFile(String fileType, Long relationId) {
    }

    @Data
    public static class SysFileVo {
        String path;
        Long fileId;

        public SysFileVo(String filePath, Long fileId) {
            this.path = filePath;
            this.fileId = fileId;
        }
    }

    private static final long serialVersionUID = 1L;

    /**
     * 附件ID
     */
    private Long fileId;



    /**
     * 附件大小
     */
    @Excel(name = "附件大小")
    private Long fileSize;


    private String result;

    public Date getCreateTime() {
        return createTime;
    }

    public SysUploadFile setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public String getResult() {
        return result;
    }

    public SysUploadFile setResult(String result) {
        this.result = result;
        return this;
    }

    /**
     * 附件储存类型
     */
    @Excel(name = "附件储存类型")
    private String fileStorageType;

    /**
     * 附件名称
     */
    @Excel(name = "附件名称")
    private String fileName;

    /**
     * 附件路径
     */
    @Excel(name = "附件路径")
    private String filePath;


    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileId() {
        return fileId;
    }



    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getFileSize() {
        return fileSize;
    }



    public void setFileStorageType(String fileStorageType) {
        this.fileStorageType = fileStorageType;
    }

    public String getFileStorageType() {
        return fileStorageType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }



    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("fileId", getFileId())
                .append("fileSize", getFileSize())
                .append("fileStorageType", getFileStorageType())
                .append("fileName", getFileName())
                .append("filePath", getFilePath())
                .append("delFlag", getDelFlag())
                .toString();
    }
}
