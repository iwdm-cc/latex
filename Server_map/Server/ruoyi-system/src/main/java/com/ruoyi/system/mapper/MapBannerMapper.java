package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MapBanner;

/**
 * 轮播图Mapper接口
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
public interface MapBannerMapper
{
    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    public MapBanner selectMapBannerById(Long id);

    /**
     * 查询轮播图列表
     *
     * @param mapBanner 轮播图
     * @return 轮播图集合
     */
    public List<MapBanner> selectMapBannerList(MapBanner mapBanner);

    /**
     * 新增轮播图
     *
     * @param mapBanner 轮播图
     * @return 结果
     */
    public int insertMapBanner(MapBanner mapBanner);

    /**
     * 修改轮播图
     *
     * @param mapBanner 轮播图
     * @return 结果
     */
    public int updateMapBanner(MapBanner mapBanner);

    /**
     * 删除轮播图
     *
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteMapBannerById(Long id);

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapBannerByIds(Long[] ids);

    List<MapBanner> queryMapBannerList(MapBanner mapBanner);
}
