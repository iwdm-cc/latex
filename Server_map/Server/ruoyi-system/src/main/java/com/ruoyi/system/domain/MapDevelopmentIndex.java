package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 产业发展指数对象 map_development_index
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
public class MapDevelopmentIndex extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 产业发展指数 */
    @Excel(name = "产业发展指数")
    private BigDecimal developmentIndex;

    /** 产业规模 */
    @Excel(name = "产业规模")
    private BigDecimal industrialScale;

    /** 产业环境 */
    @Excel(name = "产业环境")
    private BigDecimal industrialEnvironment;

    /** 区县名称 */
    @Excel(name = "区县名称")
    private String cityName;

    /** 区县code */
    @Excel(name = "区县code")
    private String cityCode;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDevelopmentIndex(BigDecimal developmentIndex)
    {
        this.developmentIndex = developmentIndex;
    }

    public BigDecimal getDevelopmentIndex()
    {
        return developmentIndex;
    }
    public void setIndustrialScale(BigDecimal industrialScale)
    {
        this.industrialScale = industrialScale;
    }

    public BigDecimal getIndustrialScale()
    {
        return industrialScale;
    }
    public void setIndustrialEnvironment(BigDecimal industrialEnvironment)
    {
        this.industrialEnvironment = industrialEnvironment;
    }

    public BigDecimal getIndustrialEnvironment()
    {
        return industrialEnvironment;
    }
    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getCityName()
    {
        return cityName;
    }
    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    public String getCityCode()
    {
        return cityCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("developmentIndex", getDevelopmentIndex())
            .append("industrialScale", getIndustrialScale())
            .append("industrialEnvironment", getIndustrialEnvironment())
            .append("cityName", getCityName())
            .append("cityCode", getCityCode())
            .toString();
    }
}
