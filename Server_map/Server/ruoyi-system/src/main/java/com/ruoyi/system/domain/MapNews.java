package com.ruoyi.system.domain;

import com.ruoyi.common.xss.Xss;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 新闻对象 map_news
 *
 * @author cqust_icc
 * @date 2022-04-19
 */
public class MapNews extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 新闻ID */
    private Integer newsId;

    /** 新闻标题 */
    @Excel(name = "新闻标题")
    @Xss(message = "新闻标题不能包含脚本字符")
    @NotBlank(message = "新闻标题不能为空")
    @Size(min = 0, max = 50, message = "新闻标题不能超过50个字符")
    private String newsTitle;

    /** 新闻类型（sys_map_news_type） */
    @Excel(name = "新闻类型", readConverterExp = "s=ys_map_news_type")
    private String newsType;

    /** 新闻内容 */
   
    private String newsContent;

    /** 新闻状态（0正常 1关闭） */
    @Excel(name = "新闻状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setNewsId(Integer newsId)
    {
        this.newsId = newsId;
    }

    public Integer getNewsId()
    {
        return newsId;
    }
    public void setNewsTitle(String newsTitle)
    {
        this.newsTitle = newsTitle;
    }

    public String getNewsTitle()
    {
        return newsTitle;
    }
    public void setNewsType(String newsType)
    {
        this.newsType = newsType;
    }

    public String getNewsType()
    {
        return newsType;
    }
    public void setNewsContent(String newsContent)
    {
        this.newsContent = newsContent;
    }

    public String getNewsContent()
    {
        return newsContent;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("newsId", getNewsId())
            .append("newsTitle", getNewsTitle())
            .append("newsType", getNewsType())
            .append("newsContent", getNewsContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
