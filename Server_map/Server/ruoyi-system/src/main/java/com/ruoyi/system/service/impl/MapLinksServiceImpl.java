package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MapLinksMapper;
import com.ruoyi.system.domain.MapLinks;
import com.ruoyi.system.service.IMapLinksService;

/**
 * 底部友情链接Service业务层处理
 * 
 * @author cqust_icc
 * @date 2022-06-16
 */
@Service
public class MapLinksServiceImpl implements IMapLinksService 
{
    @Autowired
    private MapLinksMapper mapLinksMapper;

    /**
     * 查询底部友情链接
     * 
     * @param linkId 底部友情链接主键
     * @return 底部友情链接
     */
    @Override
    public MapLinks selectMapLinksByLinkId(Long linkId)
    {
        return mapLinksMapper.selectMapLinksByLinkId(linkId);
    }

    /**
     * 查询底部友情链接列表
     * 
     * @param mapLinks 底部友情链接
     * @return 底部友情链接
     */
    @Override
    public List<MapLinks> selectMapLinksList(MapLinks mapLinks)
    {
        return mapLinksMapper.selectMapLinksList(mapLinks);
    }

    /**
     * 新增底部友情链接
     * 
     * @param mapLinks 底部友情链接
     * @return 结果
     */
    @Override
    public int insertMapLinks(MapLinks mapLinks)
    {
        return mapLinksMapper.insertMapLinks(mapLinks);
    }

    /**
     * 修改底部友情链接
     * 
     * @param mapLinks 底部友情链接
     * @return 结果
     */
    @Override
    public int updateMapLinks(MapLinks mapLinks)
    {
        return mapLinksMapper.updateMapLinks(mapLinks);
    }

    /**
     * 批量删除底部友情链接
     * 
     * @param linkIds 需要删除的底部友情链接主键
     * @return 结果
     */
    @Override
    public int deleteMapLinksByLinkIds(Long[] linkIds)
    {
        return mapLinksMapper.deleteMapLinksByLinkIds(linkIds);
    }

    /**
     * 删除底部友情链接信息
     * 
     * @param linkId 底部友情链接主键
     * @return 结果
     */
    @Override
    public int deleteMapLinksByLinkId(Long linkId)
    {
        return mapLinksMapper.deleteMapLinksByLinkId(linkId);
    }
}
