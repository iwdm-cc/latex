package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.common.core.domain.entity.MapCategory;

/**
 * 企业类别Mapper接口
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
public interface MapCategoryMapper
{
    /**
     * 查询企业类别
     *
     * @param categoryId 企业类别主键
     * @return 企业类别
     */
    public MapCategory selectMapCategoryByCategoryId(Long categoryId);

    /**
     * 查询企业类别列表
     *
     * @param mapCategory 企业类别
     * @return 企业类别集合
     */
    public List<MapCategory> selectMapCategoryList(MapCategory mapCategory);

    /**
     * 新增企业类别
     *
     * @param mapCategory 企业类别
     * @return 结果
     */
    public int insertMapCategory(MapCategory mapCategory);

    /**
     * 修改企业类别
     *
     * @param mapCategory 企业类别
     * @return 结果
     */
    public int updateMapCategory(MapCategory mapCategory);

    /**
     * 删除企业类别
     *
     * @param categoryId 企业类别主键
     * @return 结果
     */
    public int deleteMapCategoryByCategoryId(Long categoryId);

    /**
     * 批量删除企业类别
     *
     * @param categoryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapCategoryByCategoryIds(Long[] categoryIds);
}
