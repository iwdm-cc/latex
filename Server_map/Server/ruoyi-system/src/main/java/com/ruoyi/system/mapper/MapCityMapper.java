package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MapCity;

/**
 * 城市管理Mapper接口
 * 
 * @author cqust_icc
 * @date 2022-06-16
 */
public interface MapCityMapper 
{
    /**
     * 查询城市管理
     * 
     * @param cityCode 城市管理主键
     * @return 城市管理
     */
    public MapCity selectMapCityByCityCode(String cityCode);

    /**
     * 查询城市管理列表
     * 
     * @param mapCity 城市管理
     * @return 城市管理集合
     */
    public List<MapCity> selectMapCityList(MapCity mapCity);

    /**
     * 新增城市管理
     * 
     * @param mapCity 城市管理
     * @return 结果
     */
    public int insertMapCity(MapCity mapCity);

    /**
     * 修改城市管理
     * 
     * @param mapCity 城市管理
     * @return 结果
     */
    public int updateMapCity(MapCity mapCity);

    /**
     * 删除城市管理
     * 
     * @param cityCode 城市管理主键
     * @return 结果
     */
    public int deleteMapCityByCityCode(String cityCode);

    /**
     * 批量删除城市管理
     * 
     * @param cityCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapCityByCityCodes(String[] cityCodes);
}
