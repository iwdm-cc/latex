package com.ruoyi.system.domain;

/**
 * 附件类型
 */
public final class UploadFileType {

    /**
     * banner上传图片
     */
    public static String BANNER = "1";

    /**
     * marks 模型
     */
    public static String MARKS_MODEL = "2";

    /**
     * marks 大图
     */
    public static String MARKS_PICTURE = "3";

    /**
     * marks 轮播图
     */
    public static String MARKS_BANNER = "4";


}
