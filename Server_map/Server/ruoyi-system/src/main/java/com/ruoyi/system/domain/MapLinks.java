package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 底部友情链接对象 map_links
 *
 * @author cqust_icc
 * @date 2022-06-16
 */
public class MapLinks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long linkId;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String linkLabel;

    /** 显示次序 */
    @Excel(name = "显示次序")
    private Long linkSort;

    /** 友链URL */
    @Excel(name = "友链URL")
    private String linkValue;

    /** 区县名称 */
    @Excel(name = "区县名称")
    private String cityName;

    public String getCityName() {
        return cityName;
    }

    public MapLinks setCityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    /** 区县code */
    @Excel(name = "区县code")
    private String cityCode;

    public void setLinkId(Long linkId)
    {
        this.linkId = linkId;
    }

    public Long getLinkId()
    {
        return linkId;
    }
    public void setLinkLabel(String linkLabel)
    {
        this.linkLabel = linkLabel;
    }

    public String getLinkLabel()
    {
        return linkLabel;
    }
    public void setLinkSort(Long linkSort)
    {
        this.linkSort = linkSort;
    }

    public Long getLinkSort()
    {
        return linkSort;
    }
    public void setLinkValue(String linkValue)
    {
        this.linkValue = linkValue;
    }

    public String getLinkValue()
    {
        return linkValue;
    }
    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    public String getCityCode()
    {
        return cityCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("linkId", getLinkId())
            .append("linkLabel", getLinkLabel())
            .append("linkSort", getLinkSort())
            .append("linkValue", getLinkValue())
            .append("cityCode", getCityCode())
            .toString();
    }
}
