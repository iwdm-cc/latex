package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysUploadFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 附件信息Service接口
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
public interface ISysFileService
{
    /**
     * 查询附件信息
     *
     * @param fileId 附件信息主键
     * @return 附件信息
     */
    public SysUploadFile selectSysFileByFileId(Long fileId);

    /**
     * 查询附件信息列表
     *
     * @param sysUploadFile 附件信息
     * @return 附件信息集合
     */
    public List<SysUploadFile> selectSysFileList(SysUploadFile sysUploadFile);

    /**
     * 新增附件信息
     *
     * @param sysUploadFile 附件信息
     * @return 结果
     */
    public int insertSysFile(SysUploadFile sysUploadFile);

    /**
     * 修改附件信息
     *
     * @param sysUploadFile 附件信息
     * @return 结果
     */
    public int updateSysFile(SysUploadFile sysUploadFile);

    /**
     * 批量删除附件信息
     *
     * @param fileIds 需要删除的附件信息主键集合
     * @return 结果
     */
    public int deleteSysFileByFileIds(Long[] fileIds);

    /**
     * 删除附件信息信息
     *
     * @param fileId 附件信息主键
     * @return 结果
     */
    public int deleteSysFileByFileId(Long fileId);


    Object SavaFileToLocalStorage(Long userID, Long relationId, String banner, MultipartFile file) throws IOException;

    void deleteSysFileByMarks(Long relationId, String fileType);

    Object SavaLatexFileToLocalStorage(String result, MultipartFile file) throws IOException;
}
