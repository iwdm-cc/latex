package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.MapMarks;
import com.ruoyi.system.domain.SysUploadFile;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 企业管理对象 map_marks
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Data
@ToString
public class MapMarksDetailVo extends MapMarks {


    private static final long serialVersionUID = 1L;

    private  List<SysUploadFile.SysFileVo> bannerFile;
    private  List<SysUploadFile.SysFileVo> imageFile;
    private  List<SysUploadFile.SysFileVo> modelFile;


}



