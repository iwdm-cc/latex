package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MapCityMapper;
import com.ruoyi.system.domain.MapCity;
import com.ruoyi.system.service.IMapCityService;

/**
 * 城市管理Service业务层处理
 * 
 * @author cqust_icc
 * @date 2022-06-16
 */
@Service
public class MapCityServiceImpl implements IMapCityService 
{
    @Autowired
    private MapCityMapper mapCityMapper;

    /**
     * 查询城市管理
     * 
     * @param cityCode 城市管理主键
     * @return 城市管理
     */
    @Override
    public MapCity selectMapCityByCityCode(String cityCode)
    {
        return mapCityMapper.selectMapCityByCityCode(cityCode);
    }

    /**
     * 查询城市管理列表
     * 
     * @param mapCity 城市管理
     * @return 城市管理
     */
    @Override
    public List<MapCity> selectMapCityList(MapCity mapCity)
    {
        return mapCityMapper.selectMapCityList(mapCity);
    }

    /**
     * 新增城市管理
     * 
     * @param mapCity 城市管理
     * @return 结果
     */
    @Override
    public int insertMapCity(MapCity mapCity)
    {
        return mapCityMapper.insertMapCity(mapCity);
    }

    /**
     * 修改城市管理
     * 
     * @param mapCity 城市管理
     * @return 结果
     */
    @Override
    public int updateMapCity(MapCity mapCity)
    {
        return mapCityMapper.updateMapCity(mapCity);
    }

    /**
     * 批量删除城市管理
     * 
     * @param cityCodes 需要删除的城市管理主键
     * @return 结果
     */
    @Override
    public int deleteMapCityByCityCodes(String[] cityCodes)
    {
        return mapCityMapper.deleteMapCityByCityCodes(cityCodes);
    }

    /**
     * 删除城市管理信息
     * 
     * @param cityCode 城市管理主键
     * @return 结果
     */
    @Override
    public int deleteMapCityByCityCode(String cityCode)
    {
        return mapCityMapper.deleteMapCityByCityCode(cityCode);
    }
}
