package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MapNewsMapper;
import com.ruoyi.system.domain.MapNews;
import com.ruoyi.system.service.IMapNewsService;

/**
 * 新闻Service业务层处理
 * 
 * @author cqust_icc
 * @date 2022-04-19
 */
@Service
public class MapNewsServiceImpl implements IMapNewsService 
{
    @Autowired
    private MapNewsMapper mapNewsMapper;

    /**
     * 查询新闻
     * 
     * @param newsId 新闻主键
     * @return 新闻
     */
    @Override
    public MapNews selectMapNewsByNewsId(Integer newsId)
    {
        return mapNewsMapper.selectMapNewsByNewsId(newsId);
    }

    /**
     * 查询新闻列表
     * 
     * @param mapNews 新闻
     * @return 新闻
     */
    @Override
    public List<MapNews> selectMapNewsList(MapNews mapNews)
    {
        return mapNewsMapper.selectMapNewsList(mapNews);
    }

    /**
     * 新增新闻
     * 
     * @param mapNews 新闻
     * @return 结果
     */
    @Override
    public int insertMapNews(MapNews mapNews)
    {
        mapNews.setCreateTime(DateUtils.getNowDate());
        return mapNewsMapper.insertMapNews(mapNews);
    }

    /**
     * 修改新闻
     * 
     * @param mapNews 新闻
     * @return 结果
     */
    @Override
    public int updateMapNews(MapNews mapNews)
    {
        mapNews.setUpdateTime(DateUtils.getNowDate());
        return mapNewsMapper.updateMapNews(mapNews);
    }

    /**
     * 批量删除新闻
     * 
     * @param newsIds 需要删除的新闻主键
     * @return 结果
     */
    @Override
    public int deleteMapNewsByNewsIds(Integer[] newsIds)
    {
        return mapNewsMapper.deleteMapNewsByNewsIds(newsIds);
    }

    /**
     * 删除新闻信息
     * 
     * @param newsId 新闻主键
     * @return 结果
     */
    @Override
    public int deleteMapNewsByNewsId(Integer newsId)
    {
        return mapNewsMapper.deleteMapNewsByNewsId(newsId);
    }
}
