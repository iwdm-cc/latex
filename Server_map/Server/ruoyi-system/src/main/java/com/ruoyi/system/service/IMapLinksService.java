package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MapLinks;

/**
 * 底部友情链接Service接口
 * 
 * @author cqust_icc
 * @date 2022-06-16
 */
public interface IMapLinksService 
{
    /**
     * 查询底部友情链接
     * 
     * @param linkId 底部友情链接主键
     * @return 底部友情链接
     */
    public MapLinks selectMapLinksByLinkId(Long linkId);

    /**
     * 查询底部友情链接列表
     * 
     * @param mapLinks 底部友情链接
     * @return 底部友情链接集合
     */
    public List<MapLinks> selectMapLinksList(MapLinks mapLinks);

    /**
     * 新增底部友情链接
     * 
     * @param mapLinks 底部友情链接
     * @return 结果
     */
    public int insertMapLinks(MapLinks mapLinks);

    /**
     * 修改底部友情链接
     * 
     * @param mapLinks 底部友情链接
     * @return 结果
     */
    public int updateMapLinks(MapLinks mapLinks);

    /**
     * 批量删除底部友情链接
     * 
     * @param linkIds 需要删除的底部友情链接主键集合
     * @return 结果
     */
    public int deleteMapLinksByLinkIds(Long[] linkIds);

    /**
     * 删除底部友情链接信息
     * 
     * @param linkId 底部友情链接主键
     * @return 结果
     */
    public int deleteMapLinksByLinkId(Long linkId);
}
