package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MapBannerMapper;
import com.ruoyi.system.domain.MapBanner;
import com.ruoyi.system.service.IMapBannerService;

/**
 * 轮播图Service业务层处理
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Service
public class MapBannerServiceImpl implements IMapBannerService {
    @Autowired
    private MapBannerMapper mapBannerMapper;

    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public MapBanner selectMapBannerById(Long id) {
        return mapBannerMapper.selectMapBannerById(id);
    }

    /**
     * 查询轮播图列表
     *
     * @param mapBanner 轮播图
     * @return 轮播图
     */
    @Override
    public List<MapBanner> selectMapBannerList(MapBanner mapBanner) {
        return mapBannerMapper.selectMapBannerList(mapBanner);
    }

    /**
     * 新增轮播图
     *
     * @param mapBanner 轮播图
     * @return 结果
     */
    @Override
    public int insertMapBanner(MapBanner mapBanner) {
        return mapBannerMapper.insertMapBanner(mapBanner);
    }

    /**
     * 修改轮播图
     *
     * @param mapBanner 轮播图
     * @return 结果
     */
    @Override
    public int updateMapBanner(MapBanner mapBanner) {
        return mapBannerMapper.updateMapBanner(mapBanner);
    }

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteMapBannerByIds(Long[] ids) {
        return mapBannerMapper.deleteMapBannerByIds(ids);
    }

    /**
     * 删除轮播图信息
     *
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteMapBannerById(Long id) {
        return mapBannerMapper.deleteMapBannerById(id);
    }

    @Override
    public List<MapBanner> queryMapBannerList(MapBanner mapBanner) {
        return mapBannerMapper.queryMapBannerList(mapBanner);
    }
}
