package com.ruoyi.system.service.impl;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.SysUploadFile;
import com.ruoyi.system.mapper.SysFileMapper;
import com.ruoyi.system.service.ISysFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 附件信息Service业务层处理
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Service
public class SysFileServiceImpl implements ISysFileService {
    @Autowired
    private SysFileMapper sysFileMapper;

    /**
     * 查询附件信息
     *
     * @param fileId 附件信息主键
     * @return 附件信息
     */
    @Override
    public SysUploadFile selectSysFileByFileId(Long fileId) {
        return sysFileMapper.selectSysFileByFileId(fileId);
    }

    /**
     * 查询附件信息列表
     *
     * @param sysUploadFile 附件信息
     * @return 附件信息
     */
    @Override
    public List<SysUploadFile> selectSysFileList(SysUploadFile sysUploadFile) {
        return sysFileMapper.selectSysFileList(sysUploadFile);
    }

    /**
     * 新增附件信息
     *
     * @param sysUploadFile 附件信息
     * @return 结果
     */
    @Override
    public int insertSysFile(SysUploadFile sysUploadFile) {
        return sysFileMapper.insertSysFile(sysUploadFile);
    }

    /**
     * 修改附件信息
     *
     * @param sysUploadFile 附件信息
     * @return 结果
     */
    @Override
    public int updateSysFile(SysUploadFile sysUploadFile) {
        return sysFileMapper.updateSysFile(sysUploadFile);
    }

    /**
     * 批量删除附件信息
     *
     * @param fileIds 需要删除的附件信息主键
     * @return 结果
     */
    @Override
    public int deleteSysFileByFileIds(Long[] fileIds) {
        return sysFileMapper.deleteSysFileByFileIds(fileIds);
    }

    /**
     * 删除附件信息信息
     *
     * @param fileId 附件信息主键
     * @return 结果
     */
    @Override
    public int deleteSysFileByFileId(Long fileId) {
        return sysFileMapper.deleteSysFileByFileId(fileId);
    }
    @Override
    public Long SavaFileToLocalStorage(Long UserID, Long RelationId, String FileTypeName, MultipartFile file) throws IOException {

        // 上传文件路径
        String filePath = RuoYiConfig.getUploadPath();
        // 上传并返回新文件名称

        String fileName = FileUploadUtils.upload(filePath, file);

        SysUploadFile sysFileInfo = new SysUploadFile();

        String originalFilename = file.getOriginalFilename();
        //获取最后一个.的位置
        int lastIndexOf = originalFilename.lastIndexOf(".");
        //获取文件的后缀名 .jpg
        String suffix = originalFilename.substring(lastIndexOf);
        //文件名
        sysFileInfo.setFileName(file.getOriginalFilename());

        //储存方式(0本地存储)
        sysFileInfo.setFileStorageType("0");

        //文件路径
        sysFileInfo.setFilePath(fileName);

        //文件大小
        sysFileInfo.setFileSize(file.getSize());

        //附件类型
        System.out.println(sysFileInfo);
        //保存到数据库
        insertSysFile(sysFileInfo);

        return sysFileInfo.getFileId();
    }

    @Override
    public void deleteSysFileByMarks(Long relationId, String fileType) {
        Map<String, Object> param = new HashMap<>();
        param.put("relationId", relationId);
        param.put("fileType", fileType);
         sysFileMapper.deleteSysFileByMarks(param);
    }

    @Override
    public Object SavaLatexFileToLocalStorage(String result, MultipartFile file) throws IOException {

        // 上传文件路径
        String filePath = RuoYiConfig.getUploadPath();
        // 上传并返回新文件名称

        String fileName = FileUploadUtils.upload(filePath, file);

        SysUploadFile sysFileInfo = new SysUploadFile();

        String originalFilename = file.getOriginalFilename();
        //获取最后一个.的位置
        int lastIndexOf = originalFilename.lastIndexOf(".");
        //获取文件的后缀名 .jpg
        String suffix = originalFilename.substring(lastIndexOf);
        //文件名
        sysFileInfo.setFileName(file.getOriginalFilename());

        //储存方式(0本地存储)
        sysFileInfo.setFileStorageType("0");
        //上传者ID
        //文件路径
        sysFileInfo.setFilePath(fileName);
        sysFileInfo.setResult(result);
        //文件大小
        sysFileInfo.setFileSize(file.getSize());
        //保存到数据库
        insertSysFile(sysFileInfo);

        return sysFileInfo.getFileId();
    }
}
