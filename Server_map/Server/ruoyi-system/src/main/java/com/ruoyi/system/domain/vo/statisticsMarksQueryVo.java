package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * @author 张成
 * @version 1.0
 * @date 2022/4/21 20:20
 */
@Data
public class statisticsMarksQueryVo {

    @Excel(name = "数量")
    private  long amount;

    @Excel(name = "类别ID")
    private  long categoryId;

    @Excel(name = "类别名称")
    private  String categoryName;

    @Excel(name = "总数")
    private  long total;

    @Excel(name = "城市ID")
    private  String cityCode;
}
