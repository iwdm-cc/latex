package com.ruoyi.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 轮播图对象 map_banner
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Data
public class MapBanner extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 超链接地址
     */
    @Excel(name = "超链接地址")
    private String bannerLink;


    /**
     * 轮播图类型(0：首页，1:区县)
     */
    @Excel(name = "轮播图类型")
    private Integer bannerType;

    /**
     * 使能标志
     */
    @Excel(name = "使能标志")
    private Integer enable;

    //文件属性

    private String fileId;

    private String filePath;

    private String fileName;


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("bannerLink", getBannerLink())
                .append("fileId", getFileId())
                .append("enable", getEnable())
                .toString();
    }
}
