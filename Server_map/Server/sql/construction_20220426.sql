/*
 Navicat Premium Data Transfer

 Source Server         : construction
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 81.68.190.172:3306
 Source Schema         : construction

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 26/04/2022 23:42:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'sys_file', '附件信息表', NULL, NULL, 'SysFile', 'crud', 'com.ruoyi.system', 'system', 'file', '附件信息', 'cqust_icc', '0', '/', NULL, 'admin', '2022-04-16 19:24:05', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (2, 'map_banner', '轮播图', NULL, NULL, 'MapBanner', 'crud', 'com.ruoyi.system', 'system', 'banner', '轮播图', 'cqust_icc', '0', '/', NULL, 'admin', '2022-04-16 19:32:34', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (3, 'map_category', '企业类别', '', '', 'MapCategory', 'tree', 'com.ruoyi.system', 'system', 'category', '企业类别', 'cqust_icc', '0', '/', '{\"treeCode\":\"category_id\",\"treeName\":\"category_name\",\"treeParentCode\":\"parent_id\",\"parentMenuId\":1}', 'admin', '2022-04-16 19:32:34', '', '2022-04-16 19:33:35', NULL);
INSERT INTO `gen_table` VALUES (4, 'map_marks', '企业管理', NULL, NULL, 'MapMarks', 'crud', 'com.ruoyi.system', 'system', 'marks', '企业管理', 'cqust_icc', '0', '/', NULL, 'admin', '2022-04-16 19:32:35', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (5, 'map_news', '新闻表', NULL, NULL, 'MapNews', 'crud', 'com.ruoyi.system', 'system', 'news', '新闻', 'cqust_icc', '0', '/', '{\"parentMenuId\":\"2024\"}', 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:13', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'file_id', '附件ID', 'bigint(20)', 'Long', 'fileId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-16 19:24:05', '', NULL);
INSERT INTO `gen_table_column` VALUES (2, '1', 'file_relation_id', '附件关联ID', 'bigint(20)', 'Long', 'fileRelationId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-16 19:24:05', '', NULL);
INSERT INTO `gen_table_column` VALUES (3, '1', 'file_size', '附件大小', 'bigint(20)', 'Long', 'fileSize', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-04-16 19:24:05', '', NULL);
INSERT INTO `gen_table_column` VALUES (4, '1', 'file_type', '附件类型', 'char(2)', 'String', 'fileType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 4, 'admin', '2022-04-16 19:24:05', '', NULL);
INSERT INTO `gen_table_column` VALUES (5, '1', 'file_storage_type', '附件储存类型', 'char(1)', 'String', 'fileStorageType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2022-04-16 19:24:05', '', NULL);
INSERT INTO `gen_table_column` VALUES (6, '1', 'file_name', '附件名称', 'varchar(255)', 'String', 'fileName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2022-04-16 19:24:05', '', NULL);
INSERT INTO `gen_table_column` VALUES (7, '1', 'file_path', '附件路径', 'varchar(255)', 'String', 'filePath', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (8, '1', 'file_upload_user_id', '附件上传者', 'bigint(20)', 'Long', 'fileUploadUserId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (9, '1', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (10, '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (11, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (12, '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (13, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-04-16 19:24:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (14, '2', 'id', 'id', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-16 19:32:34', '', NULL);
INSERT INTO `gen_table_column` VALUES (15, '2', 'banner_link', '超链接地址', 'varchar(255)', 'String', 'bannerLink', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-16 19:32:34', '', NULL);
INSERT INTO `gen_table_column` VALUES (16, '2', 'file_id', '图片id', 'varchar(255)', 'String', 'fileId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-04-16 19:32:34', '', NULL);
INSERT INTO `gen_table_column` VALUES (17, '2', 'enable', '使能标志', 'tinyint(4)', 'Integer', 'enable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-04-16 19:32:34', '', NULL);
INSERT INTO `gen_table_column` VALUES (18, '3', 'category_id', NULL, 'int(11)', 'Long', 'categoryId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-16 19:32:34', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (19, '3', 'parent_id', '父id', 'int(11)', 'Long', 'parentId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-16 19:32:35', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (20, '3', 'category_name', '类别名称', 'varchar(255)', 'String', 'categoryName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-04-16 19:32:35', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (21, '3', 'category_description', '类别描述', 'varchar(255)', 'String', 'categoryDescription', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-04-16 19:32:35', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (22, '3', 'serial_number', '序号', 'int(11)', 'Long', 'serialNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-04-16 19:32:35', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (23, '3', 'create_time', NULL, 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-04-16 19:32:35', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (24, '3', 'update_time', NULL, 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-04-16 19:32:35', '', '2022-04-16 19:33:35');
INSERT INTO `gen_table_column` VALUES (25, '4', 'marks_id', NULL, 'int(11)', 'Long', 'marksId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-16 19:32:35', '', NULL);
INSERT INTO `gen_table_column` VALUES (26, '4', 'company_name', '公司名称', 'varchar(255)', 'String', 'companyName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-04-16 19:32:35', '', NULL);
INSERT INTO `gen_table_column` VALUES (27, '4', 'company_profile', '公司简介', 'varchar(255)', 'String', 'companyProfile', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'fileUpload', '', 3, 'admin', '2022-04-16 19:32:35', '', NULL);
INSERT INTO `gen_table_column` VALUES (28, '4', 'company_level', '公司星级', 'int(11)', 'Long', 'companyLevel', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-04-16 19:32:35', '', NULL);
INSERT INTO `gen_table_column` VALUES (29, '4', 'longitude', '坐标经度', 'float', 'Long', 'longitude', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-04-16 19:32:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (30, '4', 'latitude', '坐标纬度', 'float', 'Long', 'latitude', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-04-16 19:32:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (31, '4', 'category_id', '工厂类别', 'int(11)', 'Long', 'categoryId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-04-16 19:32:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (32, '4', 'create_time', NULL, 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-04-16 19:32:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (33, '4', 'update_time', NULL, 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-04-16 19:32:36', '', NULL);
INSERT INTO `gen_table_column` VALUES (34, '5', 'news_id', '新闻ID', 'int(4)', 'Integer', 'newsId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:13');
INSERT INTO `gen_table_column` VALUES (35, '5', 'news_title', '新闻标题', 'varchar(50)', 'String', 'newsTitle', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:13');
INSERT INTO `gen_table_column` VALUES (36, '5', 'news_type', '新闻类型（sys_map_news_type）', 'char(1)', 'String', 'newsType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'sys_map_news_type', 3, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:13');
INSERT INTO `gen_table_column` VALUES (37, '5', 'news_content', '新闻内容', 'longblob', 'String', 'newsContent', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'editor', '', 4, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:13');
INSERT INTO `gen_table_column` VALUES (38, '5', 'status', '新闻状态（0正常 1关闭）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 5, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:14');
INSERT INTO `gen_table_column` VALUES (39, '5', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:14');
INSERT INTO `gen_table_column` VALUES (40, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:14');
INSERT INTO `gen_table_column` VALUES (41, '5', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-04-19 00:01:03', '', '2022-04-19 00:03:14');
INSERT INTO `gen_table_column` VALUES (42, '5', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-04-19 00:01:04', '', '2022-04-19 00:03:14');
INSERT INTO `gen_table_column` VALUES (43, '5', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 10, 'admin', '2022-04-19 00:01:04', '', '2022-04-19 00:03:14');

-- ----------------------------
-- Table structure for map_banner
-- ----------------------------
DROP TABLE IF EXISTS `map_banner`;
CREATE TABLE `map_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `banner_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '超链接地址',
  `banner_type` tinyint(4) NOT NULL COMMENT '轮播图类型(0：首页，1:区县)',
  `file_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片id',
  `enable` tinyint(4) NULL DEFAULT 0 COMMENT '使能标(0：启用，1：禁用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '轮播图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of map_banner
-- ----------------------------
INSERT INTO `map_banner` VALUES (65, 'http://xiaomi.com', 1, NULL, 0);
INSERT INTO `map_banner` VALUES (66, 'https://juejin.cn/', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (67, 'http://baidu.com', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (73, 'https://www.baidu.com', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (74, 'https://www.baidu.com', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (75, 'https://123.com', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (76, 'https://w.123', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (77, 'http://123', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (78, 'https://123', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (79, 'https://123', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (80, 'https://ww', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (81, 'https://123', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (82, 'https://123', 2, NULL, 0);
INSERT INTO `map_banner` VALUES (89, 'http://zfcxjw.cq.gov.cn/', 2, NULL, 0);

-- ----------------------------
-- Table structure for map_category
-- ----------------------------
DROP TABLE IF EXISTS `map_category`;
CREATE TABLE `map_category`  (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL COMMENT '父id',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类别名称',
  `category_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别描述',
  `color_index` tinyint(4) NOT NULL COMMENT '颜色属性',
  `serial_number` int(11) NULL DEFAULT 0,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '企业类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of map_category
-- ----------------------------
INSERT INTO `map_category` VALUES (1, 0, '原材料企业', NULL, 0, 0, '2022-04-16 23:05:29', '2022-04-24 18:54:12');
INSERT INTO `map_category` VALUES (2, 1, '砂石', NULL, 0, 0, '2022-04-16 23:05:41', '2022-04-16 23:05:41');
INSERT INTO `map_category` VALUES (3, 1, '水泥', NULL, 1, 0, '2022-04-16 23:06:01', '2022-04-24 18:54:15');
INSERT INTO `map_category` VALUES (4, 1, '钢材', NULL, 2, 0, '2022-04-16 23:06:09', '2022-04-24 18:54:17');
INSERT INTO `map_category` VALUES (6, 1, '玻璃', NULL, 3, 0, '2022-04-16 23:06:35', '2022-04-24 18:54:21');
INSERT INTO `map_category` VALUES (7, 0, '制造业企业', NULL, 1, 0, '2022-04-16 23:07:19', '2022-04-24 18:53:55');
INSERT INTO `map_category` VALUES (8, 7, '混凝土部品', NULL, 0, 0, '2022-04-16 23:07:35', '2022-04-23 12:54:49');
INSERT INTO `map_category` VALUES (9, 7, '墙板', NULL, 1, 0, '2022-04-16 23:07:46', '2022-04-24 18:54:25');
INSERT INTO `map_category` VALUES (10, 7, '钢结构', NULL, 2, 0, '2022-04-16 23:07:55', '2022-04-24 18:54:27');
INSERT INTO `map_category` VALUES (11, 7, '玻璃', NULL, 3, 0, '2022-04-16 23:08:04', '2022-04-24 18:54:29');
INSERT INTO `map_category` VALUES (12, 0, '施工企业', NULL, 2, 0, '2022-04-16 23:08:24', '2022-04-24 18:53:51');
INSERT INTO `map_category` VALUES (13, 12, '小型企业', '&lt;= 5000万', 0, 0, '2022-04-16 23:08:40', '2022-04-16 23:08:40');
INSERT INTO `map_category` VALUES (14, 12, '中型企业', '5000万 &lt; 产值 &lt;= 1亿', 1, 0, '2022-04-16 23:08:56', '2022-04-24 18:54:31');
INSERT INTO `map_category` VALUES (15, 12, '大型企业', '1亿 &lt; 产值 &lt;= 5亿', 2, 0, '2022-04-16 23:09:11', '2022-04-24 18:54:33');
INSERT INTO `map_category` VALUES (16, 12, '龙头企业', '产值 =&gt; 5亿', 3, 0, '2022-04-16 23:09:22', '2022-04-24 18:54:36');
INSERT INTO `map_category` VALUES (17, 0, '物流企业', NULL, 3, 0, '2022-04-16 23:09:51', '2022-04-24 18:53:48');
INSERT INTO `map_category` VALUES (18, 17, '小型物流', '运量 &lt;= 1万吨', 0, 0, '2022-04-16 23:10:08', '2022-04-16 23:10:09');
INSERT INTO `map_category` VALUES (19, 17, '中型物流', '1万吨 &lt; 运量 &lt;= 5万吨', 1, 0, '2022-04-16 23:10:25', '2022-04-24 18:54:40');
INSERT INTO `map_category` VALUES (20, 17, '大型物流', '5万吨 &lt; 运量 &lt;= 10万吨', 2, 0, '2022-04-16 23:10:39', '2022-04-24 18:54:42');
INSERT INTO `map_category` VALUES (21, 17, '综合物流', '运量 &gt; 10万吨', 3, 0, '2022-04-16 23:11:34', '2022-04-24 18:54:46');
INSERT INTO `map_category` VALUES (22, 0, '建筑服务业', NULL, 4, 0, '2022-04-16 23:12:03', '2022-04-24 18:52:51');
INSERT INTO `map_category` VALUES (23, 22, 'BIM设计', NULL, 0, 0, '2022-04-16 23:12:10', '2022-04-24 18:54:50');
INSERT INTO `map_category` VALUES (24, 22, '装配式设计', NULL, 1, 0, '2022-04-16 23:12:16', '2022-04-24 18:54:56');
INSERT INTO `map_category` VALUES (25, 22, '咨询规划', NULL, 2, 0, '2022-04-16 23:12:21', '2022-04-24 18:54:56');
INSERT INTO `map_category` VALUES (26, 22, '创新中心', NULL, 3, 0, '2022-04-16 23:12:26', '2022-04-24 18:54:56');

-- ----------------------------
-- Table structure for map_marks
-- ----------------------------
DROP TABLE IF EXISTS `map_marks`;
CREATE TABLE `map_marks`  (
  `marks_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司名称',
  `company_profile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司简介',
  `talent_amount` int(11) NULL DEFAULT NULL COMMENT '人才数量',
  `city_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市id',
  `market_area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市场区域',
  `company_principal` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `company_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `company_products` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '终端产品',
  `design_capacity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计产能',
  `actual_capacity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实际产能',
  `company_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司地址',
  `company_level` float NOT NULL DEFAULT 3 COMMENT '公司星级(默认：3)',
  `longitude` float NULL DEFAULT NULL COMMENT '坐标经度',
  `latitude` float NULL DEFAULT NULL COMMENT '坐标纬度',
  `category_id` int(11) NOT NULL COMMENT '工厂类别',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`marks_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 143 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '企业管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of map_marks
-- ----------------------------
INSERT INTO `map_marks` VALUES (84, '中铁二十一局集团第五工程有限公司', '永川区文昌路877号', NULL, '500118', '', '张三1', '18523709707', '产品1', NULL, '270', '位于永川区文昌路877号，具有铁路工程施工总承包一级、房屋建筑工程施工总承包一级、市政公用工程施工总承包一级；公路工程施工总承包贰级、水利水电工程施工总承包贰级、矿山工程施工总承包贰级、机电工程施工总承包贰级；桥梁工程专业承包一级、隧道工程专业承包一级等资质。公司拥有施工机械、检测等设备400 余台（套），年度施工能力30亿元以上。', 3, 105.938, 29.3802, 16, '2022-04-24 12:38:53', '2022-04-24 13:24:58');
INSERT INTO `map_marks` VALUES (88, '重庆能华物流有限公司', '重庆市永川区工业园区凤凰湖工业园', NULL, '500118', '', '张三1', '18523709707', NULL, NULL, '270', '位于重庆市永川区工业园区凤凰湖工业园内，经营范围包括普通货运，货物专用运输（集装箱），货物专用运输（罐式）；建筑机械设备租赁；仓储服务（不含危险化学品）。', 3, 105.936, 29.3202, 21, '2022-04-24 12:40:46', '2022-04-24 13:24:58');
INSERT INTO `map_marks` VALUES (89, '重庆贝斯特物流有限公司', '重庆市永川区泸州街166号', NULL, '500118', '', '张三2', '18523709708', NULL, NULL, '315', '位于重庆市永川区泸州街166号，许可项目：普通货运（不含危险品）；大型物件运输；货物进出口；商品配送；物流策划；产品包装；货运代理、货运联营服务；仓储(不含危险品)、装卸、搬运服务。', 3, 105.898, 29.3441, 20, '2022-04-24 12:40:46', '2022-04-24 13:24:58');
INSERT INTO `map_marks` VALUES (90, '重庆市永川区申顺物流有限公司', '重庆市永川区朱沱镇龙川路115号', NULL, '500118', '', '张三3', '18523709709', NULL, NULL, '360', '位于重庆市永川区朱沱镇龙川路115号，所属行业为多式联运和运输代理业，经营范围包含：一般项目：国内货物运输代理，普通货物仓储服务（不含危险化学品等需许可审批的项目），装卸搬运。', 3, 105.846, 29.013, 19, '2022-04-24 12:40:46', '2022-04-25 00:03:11');
INSERT INTO `map_marks` VALUES (91, '重庆伍合物流有限公司永川分公司', '重庆市永川区南大街办事处大南村陈家沟村民小组300号', NULL, '500118', '', '张三4', '18523709710', NULL, NULL, '405', '位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。', 3, 105.885, 29.328, 18, '2022-04-24 12:40:46', '2022-04-26 08:48:33');
INSERT INTO `map_marks` VALUES (92, '重庆名威建设工程咨询有限公司', '重庆市永川区汇龙大道121号', NULL, '500118', '', '张三1', '18523709707', '产品1', NULL, '270', '位于重庆市永川区汇龙大道121号。经营范围包括许可项目：施工专业作业；建设工程质量检测。一般项目：工程造价咨询业务；建筑行业（建筑工程）设计（甲级）；工程测绘（丙级）；市政公用工程监理（甲级）；房屋建筑工程监理（甲级）；城乡规划编制（乙级）；风景园林工程设计专项（甲级），公路行业（公路）专业（丙级），市政行业（道路、桥梁工程）专业设计（乙级）；农林行业（农业综合开发生态工程、设施农业工程）专业乙级；工程咨询（乙级）；工程招标代理（甲级）；城市园林绿化监理（乙级）；中央投资项目招标代理（预备级）。', 3, 105.921, 29.357, 23, '2022-04-24 12:42:14', '2022-04-24 13:24:58');
INSERT INTO `map_marks` VALUES (93, '重庆筑天誉城建筑科技有限公司', '重庆市永川区星光大道999号1幢', NULL, '500118', '', '张三2', '18523709708', '产品2', NULL, '315', '位于重庆市永川区星光大道999号1幢（重庆永川工业园区凤凰湖工业园内），是一家集设计、生产、销售、BIM咨询为一体的建筑机电安装职支撑系统生产制造厂家，主要产品为抗震支架、装配式成品支架、地下综合管廊支架等。', 3, 105.928, 29.3307, 23, '2022-04-24 12:42:14', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (94, '重庆同盛建筑设计咨询有限公司', '永川区人民大道555号6幢', NULL, '500118', '', '张三3', '18523709709', '产品3', NULL, '360', '位于永川区人民大道555号6幢，经营范围包括许可项目：测绘服务，水利工程质量检测，建设工程质量检测，水利工程建设监理，工程造价咨询业务，建设工程设计。一般项目：地质勘查技术服务，工程管理服务，规划设计管理，专业设计服务，水文服务，水利相关咨询服务，水土流失防治服务，水资源管理。', 3, 105.933, 29.3594, 24, '2022-04-24 12:42:14', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (95, '重庆良益工程咨询有限公司', '重庆市永川区双石镇丁家岩村五洞桥村民小组', NULL, '500118', '', '张三4', '18523709710', '产品4', NULL, '405', '位于重庆市永川区双石镇丁家岩村五洞桥村民小组，经营范围包括建筑工程咨询；工程造价咨询；工业设计、包装装潢设计、展台设计、模型设计；市政公用工程施工总承包；建筑工程施工总承包；工程招标代理。', 3, 105.861, 29.4107, 25, '2022-04-24 12:42:14', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (96, '重庆国乔智能工程有限公司', '重庆市永川区内环南路777号一区1幢附2号', NULL, '500118', '', '张三4', '18523709710', '产品4', NULL, '405', '位于重庆市永川区内环南路777号一区1幢附2号，经营范围包括一般项目：智能工程的开发、运用、咨询服务；智能化管理系统开发应用；园林绿化；从事建筑相关业务。', 3, 105.906, 29.3415, 26, '2022-04-24 12:42:14', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (97, '重庆致鸿建筑科技有限公司', '重庆市永川区昌州大道东段801号重庆水利电力职业技术学院大学生创业园302号', NULL, '500118', '', '张三4', '18523709710', '产品4', NULL, '405', '位于重庆市永川区昌州大道东段801号重庆水利电力职业技术学院大学生创业园302号。经营范围包括一般项目：技术服务、技术开发、技术咨询、技术交流、技术转让、技术推广，计算机软硬件及辅助设备批发，计算机软硬件及辅助设备零售，工程管理服务。', 3, 105.934, 29.3668, 26, '2022-04-24 12:42:14', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (98, '重庆森成建筑科技有限公司', '永川区人民大道16号6幢3-7', NULL, '500118', '', '张三4', '18523709710', '产品4', NULL, '405', '位于永川区人民大道16号6幢3-7。是一家以节能、环保、生态为理念的大型建筑建材相关产业的平台公司，建立了强大的设计、生产、配送、施工、监理等相关联的SBC服务系统，有效解决了行业痛点，最大化地提升了工作效率和生产力。', 3, 105.918, 29.3482, 26, '2022-04-24 12:42:14', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (99, '中交一公局重庆城市建设发展有限公司', '永川区和顺大道 799 号', NULL, '500118', '', '张三1', '18523709707', '产品1', NULL, '270', '位于永川区和顺大道 799 号，厂址位于陈食街道。设计产能为15万立方米，主要生产钢筋桁架叠合板、叠合梁、预制楼梯、预制阳台、预制空调板、预制柱、预制剪力墙及部分市政预制混凝土构件。', 3, 105.949, 29.3554, 8, '2022-04-24 12:44:38', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (100, '重庆川盛建材科技有限公司', '重庆市永川区凤凰湖工业园区大安组团', NULL, '500118', '', '张三2', '18523709708', '产品2', NULL, '315', '位于重庆市永川区凤凰湖工业园区大安组团，是一家集产品研发、生产、销售为一体的国家重点研发绿色环保节能材料的公司。拥有员工300余人，总资产3.5亿元，公司占地面积300余亩，公司目前拥有三条德国先进技术的生产线，主要生产加气混凝土砌块（AAC）、加气混凝土板材(ALC)等装配式建筑节能环保建筑墙体材料，年产量150万m³，是西南地区领先的AAC、ALC生产企业。', 3, 105.999, 29.3836, 8, '2022-04-24 12:44:38', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (101, '重庆领固新材料科技有限公司', '重庆市永川区三教镇(重庆永川高新区三教产业园)', NULL, '500118', '', '张三3', '18523709709', '产品3', NULL, '360', '位于重庆市永川区三教镇(重庆永川高新区三教产业园)，是一家专业从事装配式轻质陶粒隔墙板、蒸压钢筋陶粒轻质隔墙板的生产、销售和施工为一体的新材料高科技企业。目前拥有年生产100万m²轻质陶粒隔墙板、高温蒸压钢筋陶粒轻质隔墙板（空心板、实心板、L型板、T型板）的生产能力', 3, 105.88, 29.484, 9, '2022-04-24 12:44:38', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (102, '中交世通重工有限公司', '重庆市永川区人民大道855号', NULL, '500118', '', '张三4', '18523709710', '产品4', NULL, '405', '位于重庆市永川区人民大道855号，总占地面积172亩，厂区面积为45595平方米。主要产品定位：房建钢结构、桥梁钢结构、交通工程护栏系列、钢结构桥梁U型肋，年产能将达5万吨；钢结构加工基地为西南片区设备一流、工艺一流、管理一流的前进性刚机构加工基地同时也是公司人才培养、培训学习基地。', 3, 105.927, 29.3553, 10, '2022-04-24 12:44:38', '2022-04-24 13:24:59');
INSERT INTO `map_marks` VALUES (105, '重庆渝永建设（集团）有限公司', '位于永川区昌州大道中段6号', NULL, '500118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 105.905, 29.3663, 15, '2022-04-24 13:09:48', '2022-04-25 18:32:59');
INSERT INTO `map_marks` VALUES (106, '重庆钰丰钢化玻璃有限公司', '重庆钰丰钢化玻璃有限公司', NULL, '500118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 105.841, 29.3429, 11, '2022-04-24 13:11:50', '2022-04-25 18:53:47');
INSERT INTO `map_marks` VALUES (107, '重庆首尊建设（集团）有限责任公司', '永川区汇龙大道236号', NULL, '500118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 105.919, 29.355, 14, '2022-04-24 13:15:08', '2022-04-24 13:25:00');
INSERT INTO `map_marks` VALUES (108, '重庆市永川区铭森建筑工程有限公司', '重庆市永川区红炉场镇', NULL, '500118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 105.747, 29.3619, 13, '2022-04-24 13:15:48', '2022-04-24 13:25:00');
INSERT INTO `map_marks` VALUES (128, '重庆市永恒砂石开采有限公司', '重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.782, 29.2071, 2, '2022-04-25 08:57:13', '2022-04-25 08:57:57');
INSERT INTO `map_marks` VALUES (129, '重庆长德聚砂石开采有限公司', '重庆市永川区大安街道云雾山村碗厂村民小组', NULL, '500118', '', '', '', NULL, '', '', '', 3, 106.041, 29.4061, 2, '2022-04-25 08:57:13', '2022-04-25 08:58:39');
INSERT INTO `map_marks` VALUES (130, '重庆市永川区参天砂岩开采有限公司', '重庆市永川区红炉镇龙井口村龙井村民小组', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.732, 29.3223, 2, '2022-04-25 08:57:13', '2022-04-25 09:42:39');
INSERT INTO `map_marks` VALUES (131, '重庆华新参天水泥有限公司', '永川市红炉镇会龙桥村', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.759, 29.341, 3, '2022-04-25 08:57:13', '2022-04-25 09:36:22');
INSERT INTO `map_marks` VALUES (132, '重庆市永川区禄仕水泥制品厂', '重庆市永川区禄仕水泥制品厂', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.84, 29.0689, 3, '2022-04-25 08:57:13', '2022-04-25 09:40:07');
INSERT INTO `map_marks` VALUES (133, '重庆市永川区友绍水泥制品加工厂', '重庆市永川区友绍水泥制品加工厂', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.927, 29.3561, 3, '2022-04-25 08:57:13', '2022-04-25 09:41:45');
INSERT INTO `map_marks` VALUES (134, '重庆市永川区宝巨钢材加工厂', '重庆市永川区南大街办事处华创大道31号', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.913, 29.3386, 4, '2022-04-25 08:57:13', '2022-04-25 09:44:51');
INSERT INTO `map_marks` VALUES (135, '重庆市永川区金利钢材有限公司', '重庆市永川区望城路39号', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.91, 29.3476, 4, '2022-04-25 08:57:13', '2022-04-25 09:45:30');
INSERT INTO `map_marks` VALUES (136, '重庆市永川区瑞豪钢材有限公司', '和旺物流园内B区4栋19号', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.914, 29.3379, 4, '2022-04-25 08:57:13', '2022-04-25 09:57:38');
INSERT INTO `map_marks` VALUES (137, '重庆市渝琥玻璃有限公司', '重庆永川工业园区凤凰湖工业园', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.937, 29.3042, 6, '2022-04-25 08:57:13', '2022-04-25 09:46:42');
INSERT INTO `map_marks` VALUES (138, '重庆市永川区三友玻璃有限公司', '重庆市永川区星光大道999号1幢', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.928, 29.3307, 6, '2022-04-25 08:57:13', '2022-04-25 09:47:05');
INSERT INTO `map_marks` VALUES (139, '重庆市永川区锐峰玻璃制品有限公司', '重庆市永川区三教镇玉峰村韩家场村民小组', NULL, '500118', '', '', '', NULL, '', '', '', 3, 105.887, 29.4915, 6, '2022-04-25 08:57:13', '2022-04-25 09:47:23');

-- ----------------------------
-- Table structure for map_news
-- ----------------------------
DROP TABLE IF EXISTS `map_news`;
CREATE TABLE `map_news`  (
  `news_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '新闻ID',
  `news_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '新闻标题',
  `news_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '新闻类型（sys_map_news_type）',
  `news_content` longblob NULL COMMENT '新闻内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '新闻状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`news_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新闻表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of map_news
-- ----------------------------
INSERT INTO `map_news` VALUES (1, '温馨提醒：2018-07-01 新版本发布啦', '2', 0x3C703E3C7374726F6E673EE696B0E78988E69CACE58685E5AEB9EFBBBFEFBBBF3C2F7374726F6E673E3C2F703E3C703E3C7374726F6E673E3C7370616E20636C6173733D22716C2D637572736F72223EEFBBBF3C2F7370616E3E3C2F7374726F6E673E3C696D67207372633D222F70726F642D6170692F70726F66696C652F75706C6F61642F323032322F30342F32362FE696BDE5B7A531382D325F3230323230343236303834323535413030312E6A7067223E3C696D67207372633D22223E3C696D67207372633D22223E3C696D67207372633D22223E3C2F703E, '0', 'admin', '2022-04-03 16:48:37', 'admin', '2022-04-26 08:43:00', '管理员');
INSERT INTO `map_news` VALUES (2, '维护通知：2018-07-01 系统凌晨维护', '1', 0x3C703EE7BBB4E68AA4E58685E5AEB93C2F703E, '0', 'admin', '2022-04-03 16:48:37', 'admin', '2022-04-22 21:34:06', '管理员');
INSERT INTO `map_news` VALUES (10, '政府新闻1政府新闻1政府新闻', '0', 0xE694BFE5BA9CE696B0E997BB31E58685E5AEB93C696D67207372633D2222202F3E, '0', '', '2022-04-20 14:01:04', '', '2022-04-20 15:33:07', NULL);
INSERT INTO `map_news` VALUES (11, '政府新闻2', '0', 0xE694BFE5BA9CE696B0E997BB32E58685E5AEB93C656D3EE58685E5AEB93C2F656D3E, '0', '', '2022-04-20 14:02:52', '', NULL, NULL);
INSERT INTO `map_news` VALUES (12, '地方政策1', '3', 0xE59CB0E696B9E694BFE7AD9631E59CB0E696B9E694BFE7AD9631E59CB0E696B9E694BFE7AD9631E59CB0E696B9E694BFE7AD9631, '0', '', '2022-04-20 14:03:59', '', NULL, NULL);
INSERT INTO `map_news` VALUES (13, '地方政策2', '3', 0xE59CB0E696B9E694BFE7AD9631E59CB0E696B9E694BFE7AD9631E59CB0E696B9E694BFE7AD9631E58685E5AEB933, '0', '', '2022-04-20 14:04:23', '', NULL, NULL);
INSERT INTO `map_news` VALUES (14, '行业动态11', '4', 0xE8A18CE4B89AE58AA8E6808131310AE5BC95E794A8E4BFA1E681AF, '0', '', '2022-04-20 14:04:55', '', NULL, NULL);
INSERT INTO `map_news` VALUES (18, '图片测试', '0', 0x3C703E32333432333C2F703E3C703E3C696D67207372633D222F6465762D6170692F70726F66696C652F75706C6F61642F323032322F30342F32362F74657374325F3230323230343236303934343236413030312E6A7067223E3C2F703E, '0', '', '2022-04-25 22:52:07', '', '2022-04-26 09:44:31', NULL);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-04-03 16:48:37', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-04-03 16:48:37', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-04-03 16:48:37', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2022-04-03 16:48:37', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-04-03 16:48:37', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', 'xx科技', 0, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, 'cqust_', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-03 16:48:34', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 140 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 0, 'https://www.mohurd.gov.cn/', '中华人民共和国住房和城乡建设部', 'sys_map_link', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:43:02', 'admin', '2022-04-23 09:45:12', NULL);
INSERT INTO `sys_dict_data` VALUES (101, 1, 'http://zfcxjw.cq.gov.cn/', '重庆市住房和城乡建设委员会', 'sys_map_link', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:43:47', 'admin', '2022-04-24 02:16:41', NULL);
INSERT INTO `sys_dict_data` VALUES (102, 0, '首页政府新闻', '0', 'sys_map_news_type', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:54:54', 'admin', '2022-04-18 23:55:26', NULL);
INSERT INTO `sys_dict_data` VALUES (103, 1, '首页行业动态', '1', 'sys_map_news_type', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:55:20', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 2, '首页项目解读', '2', 'sys_map_news_type', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:56:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 3, '区县地方政策', '3', 'sys_map_news_type', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:56:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 4, '区县行业动态', '4', 'sys_map_news_type', NULL, 'default', 'N', '0', 'admin', '2022-04-18 23:57:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 0, '产业发展指数', '11.22', 'sys_map_depict', NULL, 'default', 'N', '0', 'admin', '2022-04-19 23:28:03', 'admin', '2022-04-24 11:57:36', NULL);
INSERT INTO `sys_dict_data` VALUES (108, 1, '产业规模', '23423.4', 'sys_map_depict', NULL, 'default', 'N', '0', 'admin', '2022-04-19 23:28:14', 'admin', '2022-04-24 11:57:41', NULL);
INSERT INTO `sys_dict_data` VALUES (109, 2, '政策环境', '3.3443', 'sys_map_depict', NULL, 'default', 'N', '0', 'admin', '2022-04-19 23:28:24', 'admin', '2022-04-24 11:57:46', NULL);
INSERT INTO `sys_dict_data` VALUES (111, 0, '江北', '2233', 'map_district_ranking', NULL, 'default', 'N', '1', 'admin', '2022-04-21 22:21:48', 'admin', '2022-04-22 20:52:10', NULL);
INSERT INTO `sys_dict_data` VALUES (112, 1, '渝中', '215', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:22:06', 'admin', '2022-04-23 08:59:15', NULL);
INSERT INTO `sys_dict_data` VALUES (113, 2, '万州', '232', 'map_district_ranking', NULL, 'default', 'N', '1', 'admin', '2022-04-21 22:22:40', 'admin', '2022-04-22 20:53:46', NULL);
INSERT INTO `sys_dict_data` VALUES (114, 0, '企业A', '90', 'map_enterprise_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:25:30', 'admin', '2022-04-22 20:20:58', NULL);
INSERT INTO `sys_dict_data` VALUES (115, 1, '企业B', '34', 'map_enterprise_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:25:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (116, 0, '原材料', '345', 'map_industry_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:27:12', 'admin', '2022-04-23 09:14:05', NULL);
INSERT INTO `sys_dict_data` VALUES (117, 1, '制造业', '347', 'map_industry_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:27:30', 'admin', '2022-04-23 09:14:15', NULL);
INSERT INTO `sys_dict_data` VALUES (118, 0, '装配式制造', '667', 'map_talent_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:29:14', 'admin', '2022-04-23 09:15:51', NULL);
INSERT INTO `sys_dict_data` VALUES (119, 1, '原材料', '764', 'map_talent_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-21 22:29:30', 'admin', '2022-04-23 09:16:03', NULL);
INSERT INTO `sys_dict_data` VALUES (120, 0, '企业C', '50', 'map_enterprise_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-22 20:21:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (121, 0, '企业D', '58', 'map_enterprise_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-22 20:21:26', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (122, 0, '企业E', '23', 'map_enterprise_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-22 20:21:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (123, 0, '綦江', '123', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-22 20:47:09', 'admin', '2022-04-22 20:53:32', NULL);
INSERT INTO `sys_dict_data` VALUES (124, 0, '合川', '223', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-22 20:48:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (125, 0, '沙坪坝', '256', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 08:59:04', 'admin', '2022-04-23 08:59:23', NULL);
INSERT INTO `sys_dict_data` VALUES (126, 0, '南岸', '136', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 08:59:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (127, 0, '渝北', '289', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:10:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (128, 0, '巴南', '111', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:11:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (129, 0, '永川', '224', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:11:58', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (130, 0, '万州', '232', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:12:17', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (131, 0, '涪陵', '213', 'map_district_ranking', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:12:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (132, 0, '施工企业', '443', 'map_industry_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:13:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (133, 0, '物流企业', '341', 'map_industry_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:13:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (134, 0, '建筑服务', '467', 'map_industry_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:13:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (135, 0, '施工人才', '567', 'map_talent_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:16:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (136, 0, '物流人才', '467', 'map_talent_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:16:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (137, 0, '建筑服务业人才', '346', 'map_talent_mastermind', NULL, 'default', 'N', '0', 'admin', '2022-04-23 09:16:38', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-04-03 16:48:37', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '主界面友联管理', 'sys_map_link', '0', 'admin', '2022-04-18 23:39:12', '', NULL, '主界面底部友联管理');
INSERT INTO `sys_dict_type` VALUES (101, '地图富文本新闻类别', 'sys_map_news_type', '0', 'admin', '2022-04-18 23:52:51', '', NULL, '地图富文本新闻类别');
INSERT INTO `sys_dict_type` VALUES (102, '首页描述信息', 'sys_map_depict', '0', 'admin', '2022-04-19 23:27:13', '', NULL, '首页描述信息');
INSERT INTO `sys_dict_type` VALUES (103, '区县排名', 'map_district_ranking', '0', 'admin', '2022-04-21 22:21:22', '', NULL, '主页区县排名');
INSERT INTO `sys_dict_type` VALUES (104, '企业排名', 'map_enterprise_ranking', '0', 'admin', '2022-04-21 22:24:53', '', NULL, '主页企业排名');
INSERT INTO `sys_dict_type` VALUES (105, '产业总揽', 'map_industry_mastermind', '0', 'admin', '2022-04-21 22:27:01', '', NULL, '主页产业总揽');
INSERT INTO `sys_dict_type` VALUES (106, '人才总揽', 'map_talent_mastermind', '0', 'admin', '2022-04-21 22:28:25', '', NULL, '主页面人才总揽');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `file_relation_id` bigint(20) NOT NULL COMMENT '附件关联ID',
  `file_size` bigint(20) NOT NULL COMMENT '附件大小',
  `file_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件类型',
  `file_storage_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件储存类型',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件名称',
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件路径',
  `file_upload_user_id` bigint(20) NOT NULL COMMENT '附件上传者',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 80 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES (1, 1, 9702, '1', '0', 'test.xlsx', '/profile/upload/2022/04/16/test_20220416203339A001.xlsx', 1, '0', '', '2022-04-16 20:33:52', '', NULL);
INSERT INTO `sys_file` VALUES (2, 21, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/17/head_20220417111614A004.jpg', 1, '0', '', '2022-04-17 11:16:14', '', NULL);
INSERT INTO `sys_file` VALUES (3, 22, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/17/head_20220417111648A005.jpg', 1, '0', '', '2022-04-17 11:16:49', '', NULL);
INSERT INTO `sys_file` VALUES (4, 23, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417122923A001.jpg', 1, '0', '', '2022-04-17 12:29:24', '', NULL);
INSERT INTO `sys_file` VALUES (5, 24, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417124356A002.jpg', 1, '0', '', '2022-04-17 12:43:57', '', NULL);
INSERT INTO `sys_file` VALUES (6, 25, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417124504A003.jpg', 1, '0', '', '2022-04-17 12:45:04', '', NULL);
INSERT INTO `sys_file` VALUES (7, 26, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417124727A004.jpg', 1, '0', '', '2022-04-17 12:47:27', '', NULL);
INSERT INTO `sys_file` VALUES (8, 27, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417124759A005.jpg', 1, '0', '', '2022-04-17 12:48:00', '', NULL);
INSERT INTO `sys_file` VALUES (9, 28, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417125108A006.jpg', 1, '0', '', '2022-04-17 12:51:09', '', NULL);
INSERT INTO `sys_file` VALUES (10, 29, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417125148A007.jpg', 1, '0', '', '2022-04-17 12:51:49', '', NULL);
INSERT INTO `sys_file` VALUES (11, 30, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417125232A008.jpg', 1, '0', '', '2022-04-17 12:52:33', '', NULL);
INSERT INTO `sys_file` VALUES (12, 31, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417125354A009.jpg', 1, '0', '', '2022-04-17 12:53:55', '', NULL);
INSERT INTO `sys_file` VALUES (13, 32, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417125555A010.jpg', 1, '0', '', '2022-04-17 12:55:56', '', NULL);
INSERT INTO `sys_file` VALUES (14, 23, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417132527A011.jpg', 1, '0', '', '2022-04-17 13:25:27', '', NULL);
INSERT INTO `sys_file` VALUES (15, 47, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/17/head_20220417162616A001.jpg', 1, '0', '', '2022-04-17 16:26:16', '', NULL);
INSERT INTO `sys_file` VALUES (16, 48, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/17/head_20220417162658A002.jpg', 1, '0', '', '2022-04-17 16:26:59', '', NULL);
INSERT INTO `sys_file` VALUES (17, 49, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/17/head_20220417162748A003.jpg', 1, '0', '', '2022-04-17 16:27:48', '', NULL);
INSERT INTO `sys_file` VALUES (18, 50, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417204748A001.jpg', 1, '0', '', '2022-04-17 20:47:49', '', NULL);
INSERT INTO `sys_file` VALUES (19, 51, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417211303A001.jpg', 1, '0', '', '2022-04-17 21:13:03', '', NULL);
INSERT INTO `sys_file` VALUES (20, 52, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417211425A002.jpg', 1, '0', '', '2022-04-17 21:14:26', '', NULL);
INSERT INTO `sys_file` VALUES (21, 53, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417211527A003.jpg', 1, '0', '', '2022-04-17 21:15:27', '', NULL);
INSERT INTO `sys_file` VALUES (22, 54, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417211635A004.jpg', 1, '0', '', '2022-04-17 21:16:35', '', NULL);
INSERT INTO `sys_file` VALUES (23, 55, 3494, '1', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/17/Snipaste_2022-04-17_12-29-09_20220417211727A005.jpg', 1, '0', '', '2022-04-17 21:17:28', '', NULL);
INSERT INTO `sys_file` VALUES (24, 56, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417211822A006.jpg', 1, '0', '', '2022-04-17 21:18:22', '', NULL);
INSERT INTO `sys_file` VALUES (25, 57, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417212015A007.jpg', 1, '0', '', '2022-04-17 21:20:16', '', NULL);
INSERT INTO `sys_file` VALUES (26, 58, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417212103A008.jpg', 1, '0', '', '2022-04-17 21:21:04', '', NULL);
INSERT INTO `sys_file` VALUES (27, 59, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/17/test2_20220417212316A009.jpg', 1, '0', '', '2022-04-17 21:23:17', '', NULL);
INSERT INTO `sys_file` VALUES (28, 60, 4563989, '1', '0', '里面效果图.jpg', '/profile/upload/2022/04/18/里面效果图_20220418153726A001.jpg', 100, '0', '', '2022-04-18 15:37:27', '', NULL);
INSERT INTO `sys_file` VALUES (29, 61, 525020, '1', '0', '鸟瞰图.jpg', '/profile/upload/2022/04/18/鸟瞰图_20220418155802A002.jpg', 100, '0', '', '2022-04-18 15:58:03', '', NULL);
INSERT INTO `sys_file` VALUES (30, 62, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/18/head_20220418182033A001.jpg', 1, '0', '', '2022-04-18 18:20:34', '', NULL);
INSERT INTO `sys_file` VALUES (31, 63, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/18/head_20220418182208A002.jpg', 1, '0', '', '2022-04-18 18:22:08', '', NULL);
INSERT INTO `sys_file` VALUES (32, 64, 14447, '1', '0', 'head.jpg', '/profile/upload/2022/04/20/head_20220420123841A001.jpg', 100, '0', '', '2022-04-20 12:38:42', '', NULL);
INSERT INTO `sys_file` VALUES (33, 65, 475104, '1', '0', '钢材7-1.jpg', '/profile/upload/2022/04/20/钢材7-1_20220420125038A002.jpg', 100, '0', '', '2022-04-20 12:50:38', '', NULL);
INSERT INTO `sys_file` VALUES (34, 46, 751545, '2', '0', '钢材9-4.jpg', '/profile/upload/2022/04/20/钢材9-4_20220420125747A003.jpg', 100, '0', '', '2022-04-20 12:57:47', '', NULL);
INSERT INTO `sys_file` VALUES (35, 46, 121215, '3', '0', '钢材8-2.jpg', '/profile/upload/2022/04/20/钢材8-2_20220420125813A004.jpg', 100, '0', '', '2022-04-20 12:58:14', '', NULL);
INSERT INTO `sys_file` VALUES (36, 46, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/20/test2_20220420153535A001.jpg', 1, '0', '', '2022-04-20 15:35:35', '', NULL);
INSERT INTO `sys_file` VALUES (37, 46, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/23/test2_20220423220715A001.jpg', 1, '0', '', '2022-04-23 22:07:16', '', NULL);
INSERT INTO `sys_file` VALUES (38, 46, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/23/test2_20220423220858A002.jpg', 1, '0', '', '2022-04-23 22:08:58', '', NULL);
INSERT INTO `sys_file` VALUES (39, 46, 14447, '4', '0', 'head.jpg', '/profile/upload/2022/04/23/head_20220423224059A001.jpg', 1, '0', '', '2022-04-23 22:40:59', '', NULL);
INSERT INTO `sys_file` VALUES (40, 46, 14447, '4', '0', 'head.jpg', '/profile/upload/2022/04/23/head_20220423225057A002.jpg', 1, '0', '', '2022-04-23 22:50:58', '', NULL);
INSERT INTO `sys_file` VALUES (41, 60, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424095652A001.jpg', 1, '0', '', '2022-04-24 09:56:52', '', NULL);
INSERT INTO `sys_file` VALUES (42, 60, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424100805A002.jpg', 1, '0', '', '2022-04-24 10:08:05', '', NULL);
INSERT INTO `sys_file` VALUES (43, 60, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424102100A004.jpg', 1, '0', '', '2022-04-24 10:21:01', '', NULL);
INSERT INTO `sys_file` VALUES (44, 60, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424102141A005.jpg', 1, '0', '', '2022-04-24 10:21:42', '', NULL);
INSERT INTO `sys_file` VALUES (45, 60, 19114, '3', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424105642A006.jpg', 1, '0', '', '2022-04-24 10:56:43', '', NULL);
INSERT INTO `sys_file` VALUES (46, 60, 19114, '3', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424105642A007.jpg', 1, '0', '', '2022-04-24 10:56:43', '', NULL);
INSERT INTO `sys_file` VALUES (47, 38, 121215, '3', '0', '钢材8-2.jpg', '/profile/upload/2022/04/24/钢材8-2_20220424112813A003.jpg', 1, '0', '', '2022-04-24 11:28:14', '', NULL);
INSERT INTO `sys_file` VALUES (48, 38, 662507, '3', '0', '钢材7-4.jpg', '/profile/upload/2022/04/24/钢材7-4_20220424112813A002.jpg', 1, '0', '', '2022-04-24 11:28:14', '', NULL);
INSERT INTO `sys_file` VALUES (49, 38, 628520, '3', '0', '钢材8-1.jpg', '/profile/upload/2022/04/24/钢材8-1_20220424112813A001.jpg', 1, '0', '', '2022-04-24 11:28:14', '', NULL);
INSERT INTO `sys_file` VALUES (51, 71, 19114, '3', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424212637A001.jpg', 1, '0', '', '2022-04-24 21:26:37', '', NULL);
INSERT INTO `sys_file` VALUES (52, 71, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424213102A004.jpg', 1, '0', '', '2022-04-24 21:31:03', '', NULL);
INSERT INTO `sys_file` VALUES (53, 71, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/24/test2_20220424213102A003.jpg', 1, '0', '', '2022-04-24 21:31:03', '', NULL);
INSERT INTO `sys_file` VALUES (54, 90, 317726, '3', '0', '施工20-2.jpg', '/profile/upload/2022/04/25/施工20-2_20220425000311A003.jpg', 1, '0', '', '2022-04-25 00:03:11', '', NULL);
INSERT INTO `sys_file` VALUES (56, 83, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425174330A002.jpg', 1, '0', '', '2022-04-25 17:43:30', '', NULL);
INSERT INTO `sys_file` VALUES (57, 84, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425174408A003.jpg', 1, '0', '', '2022-04-25 17:44:08', '', NULL);
INSERT INTO `sys_file` VALUES (58, 85, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425174453A004.jpg', 1, '0', '', '2022-04-25 17:44:54', '', NULL);
INSERT INTO `sys_file` VALUES (59, 86, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425174549A005.jpg', 1, '0', '', '2022-04-25 17:45:50', '', NULL);
INSERT INTO `sys_file` VALUES (60, 105, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425180035A006.jpg', 1, '0', '', '2022-04-25 18:00:35', '', NULL);
INSERT INTO `sys_file` VALUES (61, 105, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425181704A007.jpg', 1, '0', '', '2022-04-25 18:17:05', '', NULL);
INSERT INTO `sys_file` VALUES (62, 105, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425182344A008.jpg', 1, '0', '', '2022-04-25 18:23:45', '', NULL);
INSERT INTO `sys_file` VALUES (63, 105, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425182652A009.jpg', 1, '0', '', '2022-04-25 18:26:53', '', NULL);
INSERT INTO `sys_file` VALUES (64, 105, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425183225A010.jpg', 1, '0', '', '2022-04-25 18:32:25', '', NULL);
INSERT INTO `sys_file` VALUES (65, 105, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425183258A011.jpg', 1, '0', '', '2022-04-25 18:32:59', '', NULL);
INSERT INTO `sys_file` VALUES (66, 87, 19114, '1', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425184309A012.jpg', 1, '0', '', '2022-04-25 18:43:09', '', NULL);
INSERT INTO `sys_file` VALUES (67, 106, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425184811A013.jpg', 1, '0', '', '2022-04-25 18:48:12', '', NULL);
INSERT INTO `sys_file` VALUES (68, 106, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425184937A014.jpg', 1, '0', '', '2022-04-25 18:49:38', '', NULL);
INSERT INTO `sys_file` VALUES (69, 106, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425185017A015.jpg', 1, '0', '', '2022-04-25 18:50:17', '', NULL);
INSERT INTO `sys_file` VALUES (70, 106, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425185102A016.jpg', 1, '0', '', '2022-04-25 18:51:02', '', NULL);
INSERT INTO `sys_file` VALUES (71, 106, 19114, '4', '0', 'test2.jpg', '/profile/upload/2022/04/25/test2_20220425185111A017.jpg', 1, '0', '', '2022-04-25 18:51:11', '', NULL);
INSERT INTO `sys_file` VALUES (72, 106, 3494, '3', '0', 'Snipaste_2022-04-17_12-29-09.jpg', '/profile/upload/2022/04/25/Snipaste_2022-04-17_12-29-09_20220425185347A018.jpg', 1, '0', '', '2022-04-25 18:53:47', '', NULL);
INSERT INTO `sys_file` VALUES (73, 91, 2006849, '4', '0', '施工18-2.jpg', '/profile/upload/2022/04/26/施工18-2_20220426084657A002.jpg', 1, '0', '', '2022-04-26 08:46:58', '', NULL);
INSERT INTO `sys_file` VALUES (74, 91, 470771, '4', '0', '施工21-4.jpg', '/profile/upload/2022/04/26/施工21-4_20220426084705A003.jpg', 1, '0', '', '2022-04-26 08:47:06', '', NULL);
INSERT INTO `sys_file` VALUES (75, 91, 652982, '4', '0', '施工21-2.jpg', '/profile/upload/2022/04/26/施工21-2_20220426084823A004.jpg', 1, '0', '', '2022-04-26 08:48:24', '', NULL);
INSERT INTO `sys_file` VALUES (76, 91, 240380, '4', '0', '施工19-1.jpg', '/profile/upload/2022/04/26/施工19-1_20220426084833A005.jpg', 1, '0', '', '2022-04-26 08:48:34', '', NULL);
INSERT INTO `sys_file` VALUES (77, 91, 903998, '4', '0', '施工18-1.jpg', '/profile/upload/2022/04/26/施工18-1_20220426084833A006.jpg', 1, '0', '', '2022-04-26 08:48:34', '', NULL);
INSERT INTO `sys_file` VALUES (78, 88, 141302, '1', '0', '溪流.jpg', '/profile/upload/2022/04/26/溪流_20220426210506A007.jpg', 1, '0', '', '2022-04-26 21:05:07', '', NULL);
INSERT INTO `sys_file` VALUES (79, 89, 108855, '1', '0', '河流.jpg', '/profile/upload/2022/04/26/河流_20220426210611A008.jpg', 1, '0', '', '2022-04-26 21:06:11', '', NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-04-03 16:48:37', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-04-03 16:48:37', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-04-03 16:48:37', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:45:55');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-23 20:54:43');
INSERT INTO `sys_logininfor` VALUES (3, 'test', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:55:18');
INSERT INTO `sys_logininfor` VALUES (4, 'test', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:56:57');
INSERT INTO `sys_logininfor` VALUES (5, 'test', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-23 20:57:19');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:57:28');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-23 20:58:04');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:58:18');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-23 20:58:59');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:59:13');
INSERT INTO `sys_logininfor` VALUES (11, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 20:59:48');
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-23 21:00:09');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 21:00:21');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-23 21:00:51');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-23 21:00:58');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-23 21:01:03');
INSERT INTO `sys_logininfor` VALUES (17, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-23 21:01:05');
INSERT INTO `sys_logininfor` VALUES (18, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 21:02:59');
INSERT INTO `sys_logininfor` VALUES (19, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 22:21:30');
INSERT INTO `sys_logininfor` VALUES (20, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 23:47:49');
INSERT INTO `sys_logininfor` VALUES (21, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 23:49:47');
INSERT INTO `sys_logininfor` VALUES (22, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-23 23:51:32');
INSERT INTO `sys_logininfor` VALUES (23, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 00:05:42');
INSERT INTO `sys_logininfor` VALUES (24, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 09:09:31');
INSERT INTO `sys_logininfor` VALUES (25, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-24 09:14:08');
INSERT INTO `sys_logininfor` VALUES (26, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 09:14:14');
INSERT INTO `sys_logininfor` VALUES (27, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-24 09:20:31');
INSERT INTO `sys_logininfor` VALUES (28, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-24 09:22:15');
INSERT INTO `sys_logininfor` VALUES (29, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '用户不存在/密码错误', '2022-04-24 09:22:20');
INSERT INTO `sys_logininfor` VALUES (30, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-04-24 09:22:38');
INSERT INTO `sys_logininfor` VALUES (31, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 09:22:40');
INSERT INTO `sys_logininfor` VALUES (32, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-24 09:23:03');
INSERT INTO `sys_logininfor` VALUES (33, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-24 09:23:07');
INSERT INTO `sys_logininfor` VALUES (34, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 11:15:28');
INSERT INTO `sys_logininfor` VALUES (35, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 12:07:23');
INSERT INTO `sys_logininfor` VALUES (36, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '退出成功', '2022-04-24 12:39:46');
INSERT INTO `sys_logininfor` VALUES (37, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-24 12:52:37');
INSERT INTO `sys_logininfor` VALUES (38, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-24 18:45:05');
INSERT INTO `sys_logininfor` VALUES (39, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 18:50:48');
INSERT INTO `sys_logininfor` VALUES (40, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 18:57:41');
INSERT INTO `sys_logininfor` VALUES (41, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 19:24:24');
INSERT INTO `sys_logininfor` VALUES (42, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-24 21:09:17');
INSERT INTO `sys_logininfor` VALUES (43, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 22:22:15');
INSERT INTO `sys_logininfor` VALUES (44, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 22:26:34');
INSERT INTO `sys_logininfor` VALUES (45, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 22:49:47');
INSERT INTO `sys_logininfor` VALUES (46, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 23:01:26');
INSERT INTO `sys_logininfor` VALUES (47, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 23:01:42');
INSERT INTO `sys_logininfor` VALUES (48, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-24 23:56:48');
INSERT INTO `sys_logininfor` VALUES (49, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-25 00:00:58');
INSERT INTO `sys_logininfor` VALUES (50, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-04-25 00:13:29');
INSERT INTO `sys_logininfor` VALUES (51, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 00:13:33');
INSERT INTO `sys_logininfor` VALUES (52, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-25 00:29:32');
INSERT INTO `sys_logininfor` VALUES (53, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 08:52:26');
INSERT INTO `sys_logininfor` VALUES (54, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 09:27:37');
INSERT INTO `sys_logininfor` VALUES (55, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-25 09:27:46');
INSERT INTO `sys_logininfor` VALUES (56, 'test', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-04-25 09:33:12');
INSERT INTO `sys_logininfor` VALUES (57, 'test', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 09:33:16');
INSERT INTO `sys_logininfor` VALUES (58, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-25 10:04:37');
INSERT INTO `sys_logininfor` VALUES (59, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 10:05:19');
INSERT INTO `sys_logininfor` VALUES (60, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 10:41:55');
INSERT INTO `sys_logininfor` VALUES (61, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-25 15:53:20');
INSERT INTO `sys_logininfor` VALUES (62, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 16:39:48');
INSERT INTO `sys_logininfor` VALUES (63, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 16:46:06');
INSERT INTO `sys_logininfor` VALUES (64, 'test', '113.205.128.148', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-04-25 16:55:23');
INSERT INTO `sys_logininfor` VALUES (65, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-25 18:58:33');
INSERT INTO `sys_logininfor` VALUES (66, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 19:59:19');
INSERT INTO `sys_logininfor` VALUES (67, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 21:15:39');
INSERT INTO `sys_logininfor` VALUES (68, 'test', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 21:22:57');
INSERT INTO `sys_logininfor` VALUES (69, 'test', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 22:23:43');
INSERT INTO `sys_logininfor` VALUES (70, 'test', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-04-25 22:27:42');
INSERT INTO `sys_logininfor` VALUES (71, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 22:27:50');
INSERT INTO `sys_logininfor` VALUES (72, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码已失效', '2022-04-25 23:23:57');
INSERT INTO `sys_logininfor` VALUES (73, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-25 23:23:59');
INSERT INTO `sys_logininfor` VALUES (74, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-25 23:24:04');
INSERT INTO `sys_logininfor` VALUES (75, 'test', '183.230.199.58', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 23:46:12');
INSERT INTO `sys_logininfor` VALUES (76, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-26 00:45:28');
INSERT INTO `sys_logininfor` VALUES (77, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '0', '退出成功', '2022-04-26 00:46:28');
INSERT INTO `sys_logininfor` VALUES (78, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-26 00:46:30');
INSERT INTO `sys_logininfor` VALUES (79, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '0', '退出成功', '2022-04-26 00:46:34');
INSERT INTO `sys_logininfor` VALUES (80, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 06:43:27');
INSERT INTO `sys_logininfor` VALUES (81, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 08:41:30');
INSERT INTO `sys_logininfor` VALUES (82, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-26 09:42:35');
INSERT INTO `sys_logininfor` VALUES (83, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 11:30:50');
INSERT INTO `sys_logininfor` VALUES (84, 'admin', '125.84.84.70', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 12:44:22');
INSERT INTO `sys_logininfor` VALUES (85, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '1', '验证码错误', '2022-04-26 16:08:53');
INSERT INTO `sys_logininfor` VALUES (86, 'admin', '183.230.199.58', 'XX XX', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-26 16:08:56');
INSERT INTO `sys_logininfor` VALUES (87, 'admin', '183.230.199.61', 'XX XX', 'Chrome 9', 'Windows 7', '0', '登录成功', '2022-04-26 21:01:56');
INSERT INTO `sys_logininfor` VALUES (88, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '1', '验证码已失效', '2022-04-26 22:06:36');
INSERT INTO `sys_logininfor` VALUES (89, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Mac OS X', '0', '登录成功', '2022-04-26 22:06:39');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2046 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 4, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-04-03 16:48:35', 'admin', '2022-04-23 10:33:10', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 3, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-04-03 16:48:35', 'admin', '2022-04-23 10:33:45', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 5, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-04-03 16:48:35', 'admin', '2022-04-23 10:33:15', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-04-03 16:48:35', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-04-03 16:48:35', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-04-03 16:48:35', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '1', '0', 'system:dept:list', 'tree', 'admin', '2022-04-03 16:48:35', 'admin', '2022-04-16 19:46:35', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '1', 'system:post:list', 'post', 'admin', '2022-04-03 16:48:35', 'admin', '2022-04-16 23:22:06', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-04-03 16:48:35', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-04-03 16:48:35', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-04-03 16:48:35', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-04-03 16:48:35', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-04-03 16:48:35', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-04-03 16:48:35', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-04-03 16:48:35', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-04-03 16:48:35', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-04-03 16:48:35', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-04-03 16:48:35', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-04-03 16:48:35', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-04-03 16:48:35', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-04-03 16:48:35', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-04-03 16:48:35', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-04-03 16:48:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '首页轮播图', 2024, 1, 'banner', 'system/banner/banner1/index', NULL, 1, 0, 'C', '0', '0', 'system:banner:list', 'select', 'admin', '2022-04-16 19:36:16', 'admin', '2022-04-16 23:22:22', '轮播图菜单');
INSERT INTO `sys_menu` VALUES (2001, '轮播图查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:banner:query', '#', 'admin', '2022-04-16 19:36:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2002, '轮播图新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:banner:add', '#', 'admin', '2022-04-16 19:36:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '轮播图修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:banner:edit', '#', 'admin', '2022-04-16 19:36:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '轮播图删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:banner:remove', '#', 'admin', '2022-04-16 19:36:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '轮播图导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:banner:export', '#', 'admin', '2022-04-16 19:36:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '企业类别', 2024, 3, 'category', 'system/category/index', NULL, 1, 0, 'C', '0', '0', 'system:category:list', 'slider', 'admin', '2022-04-16 19:36:26', 'admin', '2022-04-16 23:22:35', '企业类别菜单');
INSERT INTO `sys_menu` VALUES (2007, '企业类别查询', 2006, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:query', '#', 'admin', '2022-04-16 19:36:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '企业类别新增', 2006, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:add', '#', 'admin', '2022-04-16 19:36:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '企业类别修改', 2006, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:edit', '#', 'admin', '2022-04-16 19:36:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '企业类别删除', 2006, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:remove', '#', 'admin', '2022-04-16 19:36:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '企业类别导出', 2006, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:category:export', '#', 'admin', '2022-04-16 19:36:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '企业信息管理', 0, 1, 'marks', '', NULL, 1, 0, 'M', '0', '0', '', 'drag', 'admin', '2022-04-16 19:36:31', 'admin', '2022-04-23 10:31:28', '企业管理菜单');
INSERT INTO `sys_menu` VALUES (2013, '企业管理查询', 2012, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:marks:query', '#', 'admin', '2022-04-16 19:36:31', 'admin', '2022-04-22 23:09:35', '');
INSERT INTO `sys_menu` VALUES (2014, '企业管理新增', 2012, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:marks:add', '#', 'admin', '2022-04-16 19:36:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '企业管理修改', 2012, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:marks:edit', '#', 'admin', '2022-04-16 19:36:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '企业管理删除', 2012, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:marks:remove', '#', 'admin', '2022-04-16 19:36:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '企业管理导出', 2012, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:marks:export', '#', 'admin', '2022-04-16 19:36:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '附件信息', 3, 4, 'file', 'system/file/index', NULL, 1, 0, 'C', '0', '0', 'system:file:list', 'size', 'admin', '2022-04-16 19:37:56', 'admin', '2022-04-23 20:46:55', '附件信息菜单');
INSERT INTO `sys_menu` VALUES (2019, '附件信息查询', 2018, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:file:query', '#', 'admin', '2022-04-16 19:37:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '附件信息新增', 2018, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:file:add', '#', 'admin', '2022-04-16 19:37:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '附件信息修改', 2018, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:file:edit', '#', 'admin', '2022-04-16 19:37:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '附件信息删除', 2018, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:file:remove', '#', 'admin', '2022-04-16 19:37:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '附件信息导出', 2018, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:file:export', '#', 'admin', '2022-04-16 19:37:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '产业平台', 0, 0, 'construction', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'druid', 'admin', '2022-04-16 19:45:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '区县轮播图', 2024, 2, 'banner2', 'system/banner/banner2/index', NULL, 1, 0, 'C', '0', '0', 'system:banner:list', 'server', 'admin', '2022-04-16 22:31:47', 'admin', '2022-04-16 23:22:29', '');
INSERT INTO `sys_menu` VALUES (2026, '新闻管理', 2024, 1, 'news', 'system/news/index', NULL, 1, 0, 'C', '0', '0', 'system:news:list', 'component', 'admin', '2022-04-19 00:02:20', 'admin', '2022-04-20 13:16:10', '新闻菜单');
INSERT INTO `sys_menu` VALUES (2027, '新闻查询', 2026, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:news:query', '#', 'admin', '2022-04-19 00:02:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '新闻新增', 2026, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:news:add', '#', 'admin', '2022-04-19 00:02:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '新闻修改', 2026, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:news:edit', '#', 'admin', '2022-04-19 00:02:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '新闻删除', 2026, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:news:remove', '#', 'admin', '2022-04-19 00:02:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '新闻导出', 2026, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:news:export', '#', 'admin', '2022-04-19 00:02:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '基础数据管理', 0, 2, 'system/echarts', NULL, NULL, 1, 0, 'M', '0', '0', '', 'druid', 'admin', '2022-04-22 20:29:19', 'admin', '2022-04-23 10:33:39', '');
INSERT INTO `sys_menu` VALUES (2033, '区县排名', 2032, 0, 'echart1', 'system/echarts/echart1/index', '', 1, 0, 'C', '0', '0', 'system:dict:add', 'log', 'admin', '2022-04-22 20:30:14', 'admin', '2022-04-23 20:51:12', '');
INSERT INTO `sys_menu` VALUES (2034, '企业排名', 2032, 1, 'echart2', 'system/echarts/echart2/index', NULL, 1, 0, 'C', '0', '0', 'system:dict:edit', 'log', 'admin', '2022-04-22 20:56:54', 'admin', '2022-04-23 20:51:22', '');
INSERT INTO `sys_menu` VALUES (2035, '产业总览', 2032, 2, 'echart3', 'system/echarts/echart3/index', NULL, 1, 0, 'C', '0', '0', 'system:dict:remove', 'log', 'admin', '2022-04-22 21:00:46', 'admin', '2022-04-23 20:51:32', '');
INSERT INTO `sys_menu` VALUES (2036, '人才总览', 2032, 3, 'echart4', 'system/echarts/echart4/index', NULL, 1, 0, 'C', '0', '0', 'system:dict:remove', 'log', 'admin', '2022-04-22 21:01:31', 'admin', '2022-04-23 20:58:01', '');
INSERT INTO `sys_menu` VALUES (2038, '原材料企业', 2012, 0, 'mark1', 'system/marks/mark1/index', NULL, 1, 0, 'C', '0', '0', 'system:marks:list', 'table', 'admin', '2022-04-22 21:58:15', 'admin', '2022-04-23 19:46:59', '');
INSERT INTO `sys_menu` VALUES (2039, '制造业企业', 2012, 1, 'mark2', 'system/marks/mark2/index', NULL, 1, 0, 'C', '0', '0', 'system:marks:list', 'table', 'admin', '2022-04-22 23:09:02', 'admin', '2022-04-22 23:27:55', '');
INSERT INTO `sys_menu` VALUES (2040, '施工企业', 2012, 0, 'mark3', 'system/marks/mark3/index', NULL, 1, 0, 'C', '0', '0', 'system:marks:list', 'table', 'admin', '2022-04-22 23:10:23', 'admin', '2022-04-23 19:47:03', '');
INSERT INTO `sys_menu` VALUES (2041, '物流企业', 2012, 0, 'mark4', 'system/marks/mark4/index', NULL, 1, 0, 'C', '0', '0', 'system:marks:list', 'table', 'admin', '2022-04-22 23:11:12', 'admin', '2022-04-23 19:47:09', '');
INSERT INTO `sys_menu` VALUES (2042, '建筑服务业', 2012, 0, 'mark5', 'system/marks/mark5/index', NULL, 1, 0, 'C', '0', '0', 'system:marks:list', 'table', 'admin', '2022-04-22 23:11:54', 'admin', '2022-04-23 19:47:13', '');
INSERT INTO `sys_menu` VALUES (2043, '底部链接', 2032, 4, 'links', 'system/echarts/links/index', NULL, 1, 0, 'C', '0', '0', 'system:links:list', 'log', 'admin', '2022-04-23 10:40:01', 'admin', '2022-04-23 10:40:28', '');
INSERT INTO `sys_menu` VALUES (2044, '企业详情', 2012, 1, 'details', 'system/marks/details/index', NULL, 1, 0, 'C', '1', '0', 'system:marks:details:list', '#', 'admin', '2022-04-23 19:40:52', 'admin', '2022-04-23 19:48:59', '');
INSERT INTO `sys_menu` VALUES (2045, '描述信息', 2032, 5, 'depict', 'system/echarts/depict/index', NULL, 1, 0, 'C', '0', '0', 'system:dict:remove', 'log', 'admin', '2022-04-24 11:54:38', 'admin', '2022-04-24 11:58:06', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 新版本发布啦', '2', 0x3C696D67207372633D2222202F3E, '0', 'admin', '2022-04-03 16:48:37', 'admin', '2022-04-25 23:22:47', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 系统凌晨维护', '1', 0x3C703EE7BBB4E68AA4E58685E5AEB93C2F703E, '0', 'admin', '2022-04-03 16:48:37', 'admin', '2022-04-16 23:23:29', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 302 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '操作日志', 9, 'com.ruoyi.web.controller.monitor.SysOperlogController.clean()', 'DELETE', 1, 'test', NULL, '/monitor/operlog/clean', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:45:21');
INSERT INTO `sys_oper_log` VALUES (2, '登录日志', 9, 'com.ruoyi.web.controller.monitor.SysLogininforController.clean()', 'DELETE', 1, 'test', NULL, '/monitor/logininfor/clean', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:45:31');
INSERT INTO `sys_oper_log` VALUES (3, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1648975715000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2024,2012,1,2000,2001,2002,2003,2004,2005,2026,2027,2028,2029,2030,2031,2025,2006,2007,2008,2009,2010,2011,2038,2040,2041,2042,2013,2039,2014,2015,2016,2017,2032,2033,2034,2035,2036,2043,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,108,500,1040,1041,1042,501,1043,1044,1045],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:46:09');
INSERT INTO `sys_oper_log` VALUES (4, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1648975715000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[2024,1,2000,2001,2002,2003,2004,2005,2026,2027,2028,2029,2030,2031,2025,2006,2007,2008,2009,2010,2011,2012,2038,2040,2041,2042,2013,2039,2044,2014,2015,2016,2017,2032,2033,2034,2035,2036,2043,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,108,500,1040,1041,1042,501,1043,1044,1045],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:46:41');
INSERT INTO `sys_oper_log` VALUES (5, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"size\",\"orderNum\":4,\"menuName\":\"附件信息\",\"params\":{},\"parentId\":3,\"isCache\":\"0\",\"path\":\"file\",\"component\":\"system/file/index\",\"children\":[],\"createTime\":1650109076000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2018,\"menuType\":\"C\",\"perms\":\"system:file:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:46:55');
INSERT INTO `sys_oper_log` VALUES (6, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"log\",\"orderNum\":0,\"menuName\":\"区县排名\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"echart1\",\"component\":\"system/echarts/echart1/index\",\"children\":[],\"createTime\":1650630614000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2033,\"menuType\":\"C\",\"perms\":\"system:dict:add\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:51:12');
INSERT INTO `sys_oper_log` VALUES (7, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"log\",\"orderNum\":1,\"menuName\":\"企业排名\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"echart2\",\"component\":\"system/echarts/echart2/index\",\"children\":[],\"createTime\":1650632214000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2034,\"menuType\":\"C\",\"perms\":\"system:dict:edit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:51:22');
INSERT INTO `sys_oper_log` VALUES (8, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"log\",\"orderNum\":2,\"menuName\":\"产业总览\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"echart3\",\"component\":\"system/echarts/echart3/index\",\"children\":[],\"createTime\":1650632446000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2035,\"menuType\":\"C\",\"perms\":\"system:dict:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:51:32');
INSERT INTO `sys_oper_log` VALUES (9, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"log\",\"orderNum\":3,\"menuName\":\"人才总览\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"echart4\",\"component\":\"system/echarts/echart4/index\",\"children\":[],\"createTime\":1650632491000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2036,\"menuType\":\"C\",\"perms\":\"system:dict:export\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:51:41');
INSERT INTO `sys_oper_log` VALUES (10, '字典数据', 5, 'com.ruoyi.web.controller.system.SysDictDataController.export()', 'POST', 1, 'test', NULL, '/system/dict/data/export', '127.0.0.1', '内网IP', '{\"params\":{},\"default\":false}', NULL, 0, NULL, '2022-04-23 20:55:31');
INSERT INTO `sys_oper_log` VALUES (11, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"log\",\"orderNum\":3,\"menuName\":\"人才总览\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"echart4\",\"component\":\"system/echarts/echart4/index\",\"children\":[],\"createTime\":1650632491000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2036,\"menuType\":\"C\",\"perms\":\"system:dict:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 20:58:01');
INSERT INTO `sys_oper_log` VALUES (12, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650721989103,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 21:53:09');
INSERT INTO `sys_oper_log` VALUES (13, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650722391788,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 21:59:52');
INSERT INTO `sys_oper_log` VALUES (14, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650722417572,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:00:17');
INSERT INTO `sys_oper_log` VALUES (15, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650722536038,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:02:16');
INSERT INTO `sys_oper_log` VALUES (16, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650722586958,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:03:07');
INSERT INTO `sys_oper_log` VALUES (17, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650722624869,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:03:45');
INSERT INTO `sys_oper_log` VALUES (18, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650722634937,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:03:55');
INSERT INTO `sys_oper_log` VALUES (19, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":1,\"filePath\":\"blob:http://localhost:1024/968bd658-6ec0-472e-805c-91c69bbe3bdf\",\"params\":{},\"bannerLink\":\"http://xiaomi.com\",\"id\":69}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":69}', 0, NULL, '2022-04-23 22:04:51');
INSERT INTO `sys_oper_log` VALUES (20, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":1,\"filePath\":\"blob:http://localhost:1024/6eb2a91f-9c8a-4911-8c73-539e753bfb9a\",\"params\":{},\"bannerLink\":\"http://xiaomi.com\",\"id\":70}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":70}', 0, NULL, '2022-04-23 22:05:59');
INSERT INTO `sys_oper_log` VALUES (21, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":1,\"filePath\":\"blob:http://localhost:1024/af46729e-cc60-426b-a7fc-ea12f32f6e7e\",\"params\":{},\"bannerLink\":\"http://xiaomi.com\",\"id\":71}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":71}', 0, NULL, '2022-04-23 22:07:14');
INSERT INTO `sys_oper_log` VALUES (22, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/69', '127.0.0.1', '内网IP', '{ids=69}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:08:40');
INSERT INTO `sys_oper_log` VALUES (23, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/70', '127.0.0.1', '内网IP', '{ids=70}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:08:44');
INSERT INTO `sys_oper_log` VALUES (24, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":1,\"filePath\":\"blob:http://localhost:1024/91c242a6-8c77-4186-9950-f7a2845674a1\",\"params\":{},\"bannerLink\":\"http://xiaomi.com\",\"id\":72}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":72}', 0, NULL, '2022-04-23 22:08:58');
INSERT INTO `sys_oper_log` VALUES (25, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"12334\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":28,\"categoryParentId\":7,\"companyPhone\":\"123\",\"companyProducts\":\"123\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.759,\"marketArea\":\"123\",\"designCapacity\":\"123\",\"updateTime\":1650723002094,\"params\":{},\"companyLevel\":5,\"talentAmount\":11,\"createTime\":1650268120000,\"companyAddress\":\"123\",\"companyPrincipal\":\"123123\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 22:10:02');
INSERT INTO `sys_oper_log` VALUES (26, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"企业名称1\",\"latitude\":29.58336,\"marksId\":30,\"categoryParentId\":22,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"公司简介\",\"longitude\":106.569338,\"marketArea\":\"区域1\",\"updateTime\":1650728109431,\"params\":{},\"companyLevel\":3,\"createTime\":1650701437000,\"companyAddress\":\"地址1\",\"companyPrincipal\":\"张三1\",\"categoryId\":23}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 23:35:10');
INSERT INTO `sys_oper_log` VALUES (27, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500102\",\"companyName\":\"企业名称1\",\"latitude\":29.675285,\"marksId\":30,\"categoryParentId\":22,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"公司简介\",\"longitude\":107.307397,\"marketArea\":\"区域1\",\"updateTime\":1650728153093,\"params\":{},\"companyLevel\":3,\"createTime\":1650701437000,\"companyAddress\":\"地址1\",\"companyPrincipal\":\"张三1\",\"categoryId\":23}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-23 23:35:53');
INSERT INTO `sys_oper_log` VALUES (28, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 00:00:28');
INSERT INTO `sys_oper_log` VALUES (29, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"企业名称1\",\"latitude\":30.187647,\"marksId\":34,\"categoryParentId\":12,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"3343434\",\"longitude\":105.842919,\"marketArea\":\"\",\"updateTime\":1650729700747,\"params\":{},\"companyLevel\":3,\"createTime\":1650729629000,\"companyAddress\":\"地址1\",\"companyPrincipal\":\"张三1\",\"categoryId\":13}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:01:40');
INSERT INTO `sys_oper_log` VALUES (30, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/29,34,35,36,37', '125.84.84.70', 'XX XX', '{marksIds=29,34,35,36,37}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:23:49');
INSERT INTO `sys_oper_log` VALUES (31, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 00:23:55');
INSERT INTO `sys_oper_log` VALUES (32, '企业管理', 5, 'com.ruoyi.web.controller.map.MapMarksController.export()', 'POST', 1, 'admin', NULL, '/system/marks/export', '125.84.84.70', 'XX XX', '{\"categoryParentId\":12,\"params\":{}}', NULL, 0, NULL, '2022-04-24 00:25:24');
INSERT INTO `sys_oper_log` VALUES (33, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"中铁二十一局集团第五工程有限公司\",\"latitude\":29.380211,\"marksId\":38,\"categoryParentId\":12,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"位于永川区文昌路877号，具有铁路工程施工总承包一级、房屋建筑工程施工总承包一级、市政公用工程施工总承包一级；公路工程施工总承包贰级、水利水电工程施工总承包贰级、矿山工程施工总承包贰级、机电工程施工总承包贰级；桥梁工程专业承包一级、隧道工程专业承包一级等资质。公司拥有施工机械、检测等设备400 余台（套），年度施工能力30亿元以上。\",\"longitude\":105.937963,\"marketArea\":\"\",\"updateTime\":1650731230376,\"params\":{},\"companyLevel\":3,\"createTime\":1650731035000,\"companyAddress\":\"位于永川区文昌路877号，具有铁路工程施工总承包一级、房屋建筑工程施工总承包一级、市政公用工程施工总承包一级；公路工程施工总承包贰级、水利水电工程施工总承包贰级、矿山工程施工总承包贰级、机电工程施工总承包贰级；桥梁工程专业承包一级、隧道工程专业承包一级等资质。公司拥有施工机械、检测等设备400 余台（套），年度施工能力30亿元以上。\",\"companyPrincipal\":\"张三1\",\"categoryId\":16}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:27:10');
INSERT INTO `sys_oper_log` VALUES (34, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.366255,\"marksId\":39,\"categoryParentId\":12,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"位于永川区昌州大道中段6号，公司坚持以打造精品、创立名牌的发展战略，坚持干一项工程，树一座丰碑、干一项工程交一方朋友的理念。几十年来，无论是在初创阶段，还是在快速发展时期，无论是在市场低迷时，还是在高峰时，渝永建设集团始终注重企业品牌和形象建设，通过创造、打造名优品牌工程，使企业知名度和社会影响力在同行业内迅速提升。\",\"longitude\":105.905316,\"marketArea\":\"\",\"updateTime\":1650731274700,\"params\":{},\"companyLevel\":3,\"createTime\":1650731035000,\"companyAddress\":\"位于永川区昌州大道中段6号，公司坚持以打造精品、创立名牌的发展战略，坚持干一项工程，树一座丰碑、干一项工程交一方朋友的理念。几十年来，无论是在初创阶段，还是在快速发展时期，无论是在市场低迷时，还是在高峰时，渝永建设集团始终注重企业品牌和形象建设，通过创造、打造名优品牌工程，使企业知名度和社会影响力在同行业内迅速提升。\",\"companyPrincipal\":\"张三2\",\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:27:54');
INSERT INTO `sys_oper_log` VALUES (35, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆首尊建设（集团）有限责任公司\",\"latitude\":29.354968,\"marksId\":40,\"categoryParentId\":12,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProducts\":\"产品3\",\"companyProfile\":\"位于永川区汇龙大道236号，公司自成立以来，坚持在技术安全、项目质量上为客户提供具有竞争力的产品，具有建筑工程施工总承包壹级、市政公用工程施工总承包壹级、公路工程施工总承 包叁级、电子与智能化工程专业承包贰级、消防设施工程专业承包贰级、城市及道路照明工程专业承包叁级、地基基础工程专业承包叁级、环保工程专业承包叁级、建筑机电安装工程专业承包叁级、建筑幕墙工程专业承包贰级、钢结构工程专业承包叁级资质。\",\"longitude\":105.919248,\"marketArea\":\"\",\"updateTime\":1650731302525,\"params\":{},\"companyLevel\":3,\"createTime\":1650731035000,\"companyAddress\":\"位于永川区汇龙大道236号，公司自成立以来，坚持在技术安全、项目质量上为客户提供具有竞争力的产品，具有建筑工程施工总承包壹级、市政公用工程施工总承包壹级、公路工程施工总承 包叁级、电子与智能化工程专业承包贰级、消防设施工程专业承包贰级、城市及道路照明工程专业承包叁级、地基基础工程专业承包叁级、环保工程专业承包叁级、建筑机电安装工程专业承包叁级、建筑幕墙工程专业承包贰级、钢结构工程专业承包叁级资质。\",\"companyPrincipal\":\"张三3\",\"categoryId\":14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:28:22');
INSERT INTO `sys_oper_log` VALUES (36, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区铭森建筑工程有限公司\",\"latitude\":29.361876,\"marksId\":41,\"categoryParentId\":12,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"位于重庆市永川区红炉场镇，主营行业为建筑业，服务领域为房屋建筑工程施工总承包（叁级）、混凝土预制构件专业承包（叁级）（以上经营范围按建筑业企业资质证书核定的事项从事经营）；销售建筑材料（不含油漆及其他危险化学品）、五金、交电。\",\"longitude\":105.746746,\"marketArea\":\"\",\"updateTime\":1650731330174,\"params\":{},\"companyLevel\":3,\"createTime\":1650731035000,\"companyAddress\":\"位于重庆市永川区红炉场镇，主营行业为建筑业，服务领域为房屋建筑工程施工总承包（叁级）、混凝土预制构件专业承包（叁级）（以上经营范围按建筑业企业资质证书核定的事项从事经营）；销售建筑材料（不含油漆及其他危险化学品）、五金、交电。\",\"companyPrincipal\":\"张三4\",\"categoryId\":13}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:28:50');
INSERT INTO `sys_oper_log` VALUES (37, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 00:36:31');
INSERT INTO `sys_oper_log` VALUES (38, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/30,31,32,33', '125.84.84.70', 'XX XX', '{marksIds=30,31,32,33}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:38:10');
INSERT INTO `sys_oper_log` VALUES (39, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 00:38:17');
INSERT INTO `sys_oper_log` VALUES (40, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/27,28', '125.84.84.70', 'XX XX', '{marksIds=27,28}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:38:32');
INSERT INTO `sys_oper_log` VALUES (41, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 00:38:39');
INSERT INTO `sys_oper_log` VALUES (42, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆能华物流有限公司\",\"latitude\":29.320168,\"marksId\":42,\"categoryParentId\":17,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProfile\":\"位于重庆市永川区工业园区凤凰湖工业园内，经营范围包括普通货运，货物专用运输（集装箱），货物专用运输（罐式）；建筑机械设备租赁；仓储服务（不含危险化学品）。\",\"longitude\":105.935855,\"marketArea\":\"\",\"updateTime\":1650731974344,\"params\":{},\"companyLevel\":3,\"createTime\":1650731792000,\"companyAddress\":\"位于重庆市永川区工业园区凤凰湖工业园内，经营范围包括普通货运，货物专用运输（集装箱），货物专用运输（罐式）；建筑机械设备租赁；仓储服务（不含危险化学品）。\",\"companyPrincipal\":\"张三1\",\"categoryId\":21}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:39:34');
INSERT INTO `sys_oper_log` VALUES (43, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆贝斯特物流有限公司\",\"latitude\":29.344132,\"marksId\":43,\"categoryParentId\":17,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProfile\":\"位于重庆市永川区泸州街166号，许可项目：普通货运（不含危险品）；大型物件运输；货物进出口；商品配送；物流策划；产品包装；货运代理、货运联营服务；仓储(不含危险品)、装卸、搬运服务。\",\"longitude\":105.89772,\"marketArea\":\"\",\"updateTime\":1650731993540,\"params\":{},\"companyLevel\":3,\"createTime\":1650731792000,\"companyAddress\":\"位于重庆市永川区泸州街166号，许可项目：普通货运（不含危险品）；大型物件运输；货物进出口；商品配送；物流策划；产品包装；货运代理、货运联营服务；仓储(不含危险品)、装卸、搬运服务。\",\"companyPrincipal\":\"张三2\",\"categoryId\":20}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:39:53');
INSERT INTO `sys_oper_log` VALUES (44, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区申顺物流有限公司\",\"latitude\":29.012968,\"marksId\":44,\"categoryParentId\":17,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProfile\":\"位于重庆市永川区朱沱镇龙川路115号，所属行业为多式联运和运输代理业，经营范围包含：一般项目：国内货物运输代理，普通货物仓储服务（不含危险化学品等需许可审批的项目），装卸搬运。\",\"longitude\":105.846474,\"marketArea\":\"\",\"updateTime\":1650732015578,\"params\":{},\"companyLevel\":3,\"createTime\":1650731792000,\"companyAddress\":\"位于重庆市永川区朱沱镇龙川路115号，所属行业为多式联运和运输代理业，经营范围包含：一般项目：国内货物运输代理，普通货物仓储服务（不含危险化学品等需许可审批的项目），装卸搬运。\",\"companyPrincipal\":\"张三3\",\"categoryId\":19}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:40:15');
INSERT INTO `sys_oper_log` VALUES (45, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆伍合物流有限公司永川分公司\",\"latitude\":29.328024,\"marksId\":45,\"categoryParentId\":17,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProfile\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"longitude\":105.885257,\"marketArea\":\"\",\"updateTime\":1650732038477,\"params\":{},\"companyLevel\":3,\"createTime\":1650731792000,\"companyAddress\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:40:38');
INSERT INTO `sys_oper_log` VALUES (46, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆名威建设工程咨询有限公司\",\"latitude\":29.357011,\"marksId\":46,\"categoryParentId\":22,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"位于重庆市永川区汇龙大道121号。经营范围包括许可项目：施工专业作业；建设工程质量检测。一般项目：工程造价咨询业务；建筑行业（建筑工程）设计（甲级）；工程测绘（丙级）；市政公用工程监理（甲级）；房屋建筑工程监理（甲级）；城乡规划编制（乙级）；风景园林工程设计专项（甲级），公路行业（公路）专业（丙级），市政行业（道路、桥梁工程）专业设计（乙级）；农林行业（农业综合开发生态工程、设施农业工程）专业乙级；工程咨询（乙级）；工程招标代理（甲级）；城市园林绿化监理（乙级）；中央投资项目招标代理（预备级）。\",\"longitude\":105.920928,\"marketArea\":\"\",\"updateTime\":1650732087170,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于重庆市永川区汇龙大道121号。经营范围包括许可项目：施工专业作业；建设工程质量检测。一般项目：工程造价咨询业务；建筑行业（建筑工程）设计（甲级）；工程测绘（丙级）；市政公用工程监理（甲级）；房屋建筑工程监理（甲级）；城乡规划编制（乙级）；风景园林工程设计专项（甲级），公路行业（公路）专业（丙级），市政行业（道路、桥梁工程）专业设计（乙级）；农林行业（农业综合开发生态工程、设施农业工程）专业乙级；工程咨询（乙级）；工程招标代理（甲级）；城市园林绿化监理（乙级）；中央投资项目招标代理（预备级）。\",\"companyPrincipal\":\"张三1\",\"categoryId\":23}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:41:27');
INSERT INTO `sys_oper_log` VALUES (47, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆筑天誉城建筑科技有限公司\",\"latitude\":29.330699,\"marksId\":47,\"categoryParentId\":22,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"位于重庆市永川区星光大道999号1幢（重庆永川工业园区凤凰湖工业园内），是一家集设计、生产、销售、BIM咨询为一体的建筑机电安装职支撑系统生产制造厂家，主要产品为抗震支架、装配式成品支架、地下综合管廊支架等。\",\"longitude\":105.928384,\"marketArea\":\"\",\"updateTime\":1650732108566,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于重庆市永川区星光大道999号1幢（重庆永川工业园区凤凰湖工业园内），是一家集设计、生产、销售、BIM咨询为一体的建筑机电安装职支撑系统生产制造厂家，主要产品为抗震支架、装配式成品支架、地下综合管廊支架等。\",\"companyPrincipal\":\"张三2\",\"categoryId\":23}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:41:48');
INSERT INTO `sys_oper_log` VALUES (48, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆同盛建筑设计咨询有限公司\",\"latitude\":29.359407,\"marksId\":48,\"categoryParentId\":22,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProducts\":\"产品3\",\"companyProfile\":\"位于永川区人民大道555号6幢，经营范围包括许可项目：测绘服务，水利工程质量检测，建设工程质量检测，水利工程建设监理，工程造价咨询业务，建设工程设计。一般项目：地质勘查技术服务，工程管理服务，规划设计管理，专业设计服务，水文服务，水利相关咨询服务，水土流失防治服务，水资源管理。\",\"longitude\":105.933214,\"marketArea\":\"\",\"updateTime\":1650732136053,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于永川区人民大道555号6幢，经营范围包括许可项目：测绘服务，水利工程质量检测，建设工程质量检测，水利工程建设监理，工程造价咨询业务，建设工程设计。一般项目：地质勘查技术服务，工程管理服务，规划设计管理，专业设计服务，水文服务，水利相关咨询服务，水土流失防治服务，水资源管理。\",\"companyPrincipal\":\"张三3\",\"categoryId\":24}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:42:16');
INSERT INTO `sys_oper_log` VALUES (49, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆良益工程咨询有限公司\",\"latitude\":29.410705,\"marksId\":49,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"位于重庆市永川区双石镇丁家岩村五洞桥村民小组，经营范围包括建筑工程咨询；工程造价咨询；工业设计、包装装潢设计、展台设计、模型设计；市政公用工程施工总承包；建筑工程施工总承包；工程招标代理。\",\"longitude\":105.861439,\"marketArea\":\"\",\"updateTime\":1650732155647,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于重庆市永川区双石镇丁家岩村五洞桥村民小组，经营范围包括建筑工程咨询；工程造价咨询；工业设计、包装装潢设计、展台设计、模型设计；市政公用工程施工总承包；建筑工程施工总承包；工程招标代理。\",\"companyPrincipal\":\"张三4\",\"categoryId\":25}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:42:35');
INSERT INTO `sys_oper_log` VALUES (50, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆国乔智能工程有限公司\",\"latitude\":29.341492,\"marksId\":50,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"位于重庆市永川区内环南路777号一区1幢附2号，经营范围包括一般项目：智能工程的开发、运用、咨询服务；智能化管理系统开发应用；园林绿化；从事建筑相关业务。\",\"longitude\":105.906004,\"marketArea\":\"\",\"updateTime\":1650732190952,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于重庆市永川区内环南路777号一区1幢附2号，经营范围包括一般项目：智能工程的开发、运用、咨询服务；智能化管理系统开发应用；园林绿化；从事建筑相关业务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:43:11');
INSERT INTO `sys_oper_log` VALUES (51, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆致鸿建筑科技有限公司\",\"latitude\":29.366782,\"marksId\":51,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"位于重庆市永川区昌州大道东段801号重庆水利电力职业技术学院大学生创业园302号。经营范围包括一般项目：技术服务、技术开发、技术咨询、技术交流、技术转让、技术推广，计算机软硬件及辅助设备批发，计算机软硬件及辅助设备零售，工程管理服务。\",\"longitude\":105.933768,\"marketArea\":\"\",\"updateTime\":1650732227938,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于重庆市永川区昌州大道东段801号重庆水利电力职业技术学院大学生创业园302号。经营范围包括一般项目：技术服务、技术开发、技术咨询、技术交流、技术转让、技术推广，计算机软硬件及辅助设备批发，计算机软硬件及辅助设备零售，工程管理服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:43:47');
INSERT INTO `sys_oper_log` VALUES (52, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆森成建筑科技有限公司\",\"latitude\":29.34824,\"marksId\":52,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"位于永川区人民大道16号6幢3-7。是一家以节能、环保、生态为理念的大型建筑建材相关产业的平台公司，建立了强大的设计、生产、配送、施工、监理等相关联的SBC服务系统，有效解决了行业痛点，最大化地提升了工作效率和生产力。\",\"longitude\":105.917875,\"marketArea\":\"\",\"updateTime\":1650732255690,\"params\":{},\"companyLevel\":3,\"createTime\":1650731897000,\"companyAddress\":\"位于永川区人民大道16号6幢3-7。是一家以节能、环保、生态为理念的大型建筑建材相关产业的平台公司，建立了强大的设计、生产、配送、施工、监理等相关联的SBC服务系统，有效解决了行业痛点，最大化地提升了工作效率和生产力。\",\"companyPrincipal\":\"张三4\",\"categoryId\":26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:44:15');
INSERT INTO `sys_oper_log` VALUES (53, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"中交一公局重庆城市建设发展有限公司\",\"latitude\":29.355409,\"marksId\":53,\"categoryParentId\":7,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"位于永川区和顺大道 799 号，厂址位于陈食街道。设计产能为15万立方米，主要生产钢筋桁架叠合板、叠合梁、预制楼梯、预制阳台、预制空调板、预制柱、预制剪力墙及部分市政预制混凝土构件。\",\"longitude\":105.948931,\"marketArea\":\"\",\"updateTime\":1650732370674,\"params\":{},\"companyLevel\":3,\"createTime\":1650731920000,\"companyAddress\":\"位于永川区和顺大道 799 号，厂址位于陈食街道。设计产能为15万立方米，主要生产钢筋桁架叠合板、叠合梁、预制楼梯、预制阳台、预制空调板、预制柱、预制剪力墙及部分市政预制混凝土构件。\",\"companyPrincipal\":\"张三1\",\"categoryId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:46:10');
INSERT INTO `sys_oper_log` VALUES (54, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆川盛建材科技有限公司\",\"latitude\":29.383589,\"marksId\":54,\"categoryParentId\":7,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"位于重庆市永川区凤凰湖工业园区大安组团，是一家集产品研发、生产、销售为一体的国家重点研发绿色环保节能材料的公司。拥有员工300余人，总资产3.5亿元，公司占地面积300余亩，公司目前拥有三条德国先进技术的生产线，主要生产加气混凝土砌块（AAC）、加气混凝土板材(ALC)等装配式建筑节能环保建筑墙体材料，年产量150万m³，是西南地区领先的AAC、ALC生产企业。\",\"longitude\":105.999201,\"marketArea\":\"\",\"updateTime\":1650732403739,\"params\":{},\"companyLevel\":3,\"createTime\":1650731920000,\"companyAddress\":\"位于重庆市永川区凤凰湖工业园区大安组团，是一家集产品研发、生产、销售为一体的国家重点研发绿色环保节能材料的公司。拥有员工300余人，总资产3.5亿元，公司占地面积300余亩，公司目前拥有三条德国先进技术的生产线，主要生产加气混凝土砌块（AAC）、加气混凝土板材(ALC)等装配式建筑节能环保建筑墙体材料，年产量150万m³，是西南地区领先的AAC、ALC生产企业。\",\"companyPrincipal\":\"张三2\",\"categoryId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:46:43');
INSERT INTO `sys_oper_log` VALUES (55, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆川盛建材科技有限公司\",\"latitude\":29.383589,\"marksId\":54,\"categoryParentId\":7,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"位于重庆市永川区凤凰湖工业园区大安组团，是一家集产品研发、生产、销售为一体的国家重点研发绿色环保节能材料的公司。拥有员工300余人，总资产3.5亿元，公司占地面积300余亩，公司目前拥有三条德国先进技术的生产线，主要生产加气混凝土砌块（AAC）、加气混凝土板材(ALC)等装配式建筑节能环保建筑墙体材料，年产量150万m³，是西南地区领先的AAC、ALC生产企业。\",\"longitude\":105.999201,\"marketArea\":\"\",\"updateTime\":1650732417050,\"params\":{},\"companyLevel\":3,\"createTime\":1650731920000,\"companyAddress\":\"位于重庆市永川区凤凰湖工业园区大安组团，是一家集产品研发、生产、销售为一体的国家重点研发绿色环保节能材料的公司。拥有员工300余人，总资产3.5亿元，公司占地面积300余亩，公司目前拥有三条德国先进技术的生产线，主要生产加气混凝土砌块（AAC）、加气混凝土板材(ALC)等装配式建筑节能环保建筑墙体材料，年产量150万m³，是西南地区领先的AAC、ALC生产企业。\",\"companyPrincipal\":\"张三2\",\"categoryId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:46:57');
INSERT INTO `sys_oper_log` VALUES (56, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆领固新材料科技有限公司\",\"latitude\":29.483984,\"marksId\":55,\"categoryParentId\":7,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProducts\":\"产品3\",\"companyProfile\":\"位于重庆市永川区三教镇(重庆永川高新区三教产业园)，是一家专业从事装配式轻质陶粒隔墙板、蒸压钢筋陶粒轻质隔墙板的生产、销售和施工为一体的新材料高科技企业。目前拥有年生产100万m²轻质陶粒隔墙板、高温蒸压钢筋陶粒轻质隔墙板（空心板、实心板、L型板、T型板）的生产能力\",\"longitude\":105.880254,\"marketArea\":\"\",\"updateTime\":1650732439393,\"params\":{},\"companyLevel\":3,\"createTime\":1650731920000,\"companyAddress\":\"位于重庆市永川区三教镇(重庆永川高新区三教产业园)，是一家专业从事装配式轻质陶粒隔墙板、蒸压钢筋陶粒轻质隔墙板的生产、销售和施工为一体的新材料高科技企业。目前拥有年生产100万m²轻质陶粒隔墙板、高温蒸压钢筋陶粒轻质隔墙板（空心板、实心板、L型板、T型板）的生产能力\",\"companyPrincipal\":\"张三3\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:47:19');
INSERT INTO `sys_oper_log` VALUES (57, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"中交世通重工有限公司\",\"latitude\":29.355327,\"marksId\":56,\"categoryParentId\":7,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"位于重庆市永川区人民大道855号，总占地面积172亩，厂区面积为45595平方米。主要产品定位：房建钢结构、桥梁钢结构、交通工程护栏系列、钢结构桥梁U型肋，年产能将达5万吨；钢结构加工基地为西南片区设备一流、工艺一流、管理一流的前进性刚机构加工基地同时也是公司人才培养、培训学习基地。\",\"longitude\":105.926766,\"marketArea\":\"\",\"updateTime\":1650732463686,\"params\":{},\"companyLevel\":3,\"createTime\":1650731920000,\"companyAddress\":\"位于重庆市永川区人民大道855号，总占地面积172亩，厂区面积为45595平方米。主要产品定位：房建钢结构、桥梁钢结构、交通工程护栏系列、钢结构桥梁U型肋，年产能将达5万吨；钢结构加工基地为西南片区设备一流、工艺一流、管理一流的前进性刚机构加工基地同时也是公司人才培养、培训学习基地。\",\"companyPrincipal\":\"张三4\",\"categoryId\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:47:43');
INSERT INTO `sys_oper_log` VALUES (58, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 00:51:04');
INSERT INTO `sys_oper_log` VALUES (59, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/57', '125.84.84.70', 'XX XX', '{marksIds=57}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:53:03');
INSERT INTO `sys_oper_log` VALUES (60, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.240702,\"marksId\":58,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号，从事矿产资源（非煤矿山）开采、非金属矿及制品销售。\",\"longitude\":105.842121,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732813244,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:53:33');
INSERT INTO `sys_oper_log` VALUES (61, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆长德聚砂石开采有限公司\",\"latitude\":29.386579,\"marksId\":59,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区大安街道云雾山村碗厂村民小组，从事建筑用砂岩露天开采（按采矿许可证核定的事项及期限从事经营）； 建筑用砂岩销售等。\",\"longitude\":106.04771,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732830671,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:53:50');
INSERT INTO `sys_oper_log` VALUES (62, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.322263,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732337,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732844911,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:54:04');
INSERT INTO `sys_oper_log` VALUES (63, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":61,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于永川市红炉镇会龙桥村，产品辐射渝西片区和四川东南部地区。目前公司现拥有一条日产5000吨的新型干法生产线，拥有一处大型石灰石矿，公司年产水泥200万吨以上，配套有低温余热发电系统及水泥窑协同危废处置系统。兼备熟料、水泥、骨料、混凝土、免烧砖等各类建材产品的生产和销售及危险废弃物处置。\",\"longitude\":105.758637,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732876092,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:54:36');
INSERT INTO `sys_oper_log` VALUES (64, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区禄仕水泥制品厂\",\"latitude\":29.352131,\"marksId\":62,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于永川区胜利路办事处永青村谢家老院子村民小组，于2009年04月16日在重庆市工商行政管理局永川区分局注册成立，在工厂发展壮大的13年里，始终为客户提供好的产品和技术支持、健全的售后服务，主要经营水泥制品加工、销售。\",\"longitude\":105.89027,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732896092,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:54:56');
INSERT INTO `sys_oper_log` VALUES (65, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区友绍水泥制品加工厂\",\"latitude\":29.209972,\"marksId\":63,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区卫星湖街道石龟寺村圣塘湾村民小组，经营范围包括水泥制品加工、销售。\",\"longitude\":105.872217,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732913272,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:55:13');
INSERT INTO `sys_oper_log` VALUES (66, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区宝巨钢材加工厂\",\"latitude\":29.34277,\"marksId\":64,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区南大街办事处华创大道31号，主要从事钢材销售及加工。\",\"longitude\":105.896987,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650732927839,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:55:27');
INSERT INTO `sys_oper_log` VALUES (67, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区金利钢材有限公司\",\"latitude\":29.347611,\"marksId\":65,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区望城路39号，销售金属材料（不含稀贵金属）、机电设备、矿山机器零配件、橡胶制品、建筑材料、装饰材料、五金、交电。\",\"longitude\":105.909528,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650733007650,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:56:47');
INSERT INTO `sys_oper_log` VALUES (68, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.338605,\"marksId\":66,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区南大街办事处华创大道31号(和旺物流园内B区4栋19号)，经营范围包括：金属结构销售；金属材料销售；金属制品销售；金属制品研发；金属结构制造；销售钢材、钢管、阀门管件、石棉制品、橡胶制品、管材、五金工具。\",\"longitude\":105.913358,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650733030357,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:57:10');
INSERT INTO `sys_oper_log` VALUES (69, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.304152,\"marksId\":67,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆永川工业园区凤凰湖工业园（大安园），是重庆市靠前家浮法玻璃生产企业，占地面积500亩，总投入资金12亿元，现有职工600人。建有二条浮法玻璃生产线，两条低辐射镀膜节能玻璃线以及余热发电等相关配套设施。\",\"longitude\":105.936693,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650733057905,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:57:37');
INSERT INTO `sys_oper_log` VALUES (70, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区三友玻璃有限公司\",\"latitude\":29.330699,\"marksId\":68,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区星光大道999号1幢(重庆永川工业园区凤凰湖工业园区)，经营范围包含：玻璃制品加工、销售；建材（不含油漆及其他危险化学品）、五金、交电销售。\",\"longitude\":105.928384,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650733078600,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:57:58');
INSERT INTO `sys_oper_log` VALUES (71, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区锐峰玻璃制品有限公司\",\"latitude\":29.491495,\"marksId\":69,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区三教镇玉峰村韩家场村民小组，主营业务为玻璃制品加工、销售；道路普通货运。\",\"longitude\":105.887261,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650733103447,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 00:58:23');
INSERT INTO `sys_oper_log` VALUES (72, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/61,62,63,64,65,66,67', '125.84.84.70', 'XX XX', '{marksIds=61,62,63,64,65,66,67}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:03:47');
INSERT INTO `sys_oper_log` VALUES (73, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/68,69', '125.84.84.70', 'XX XX', '{marksIds=68,69}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:03:52');
INSERT INTO `sys_oper_log` VALUES (74, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/41', '125.84.84.70', 'XX XX', '{marksIds=41}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:03:58');
INSERT INTO `sys_oper_log` VALUES (75, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/45', '125.84.84.70', 'XX XX', '{marksIds=45}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:04:03');
INSERT INTO `sys_oper_log` VALUES (76, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/50,51,52,49', '125.84.84.70', 'XX XX', '{marksIds=50,51,52,49}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:04:33');
INSERT INTO `sys_oper_log` VALUES (77, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/56', '125.84.84.70', 'XX XX', '{marksIds=56}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:04:39');
INSERT INTO `sys_oper_log` VALUES (78, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/71', '125.84.84.70', 'XX XX', '{ids=71}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:19:37');
INSERT INTO `sys_oper_log` VALUES (79, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/72', '125.84.84.70', 'XX XX', '{ids=72}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 01:19:39');
INSERT INTO `sys_oper_log` VALUES (80, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '125.84.84.70', 'XX XX', '{\"dictValue\":\"重庆市住房和城乡建设委员会\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"sys_map_link\",\"dictLabel\":\"http://zfcxjw.cq.gov.cn/\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1650296627000,\"dictCode\":101,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 02:16:41');
INSERT INTO `sys_oper_log` VALUES (81, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, 'admin', NULL, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', 'admin13 admin1234', '{\"msg\":\"修改密码失败，旧密码错误\",\"code\":500}', 0, NULL, '2022-04-24 09:14:01');
INSERT INTO `sys_oper_log` VALUES (82, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, 'admin', NULL, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', 'admin123 admin1234', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:14:27');
INSERT INTO `sys_oper_log` VALUES (83, '个人信息', 2, 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', 1, 'admin', NULL, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', 'admin1234 admin123', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:22:58');
INSERT INTO `sys_oper_log` VALUES (84, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765120924,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:51:59');
INSERT INTO `sys_oper_log` VALUES (85, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765140931,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:52:19');
INSERT INTO `sys_oper_log` VALUES (86, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765198795,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:53:17');
INSERT INTO `sys_oper_log` VALUES (87, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765249599,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:54:08');
INSERT INTO `sys_oper_log` VALUES (88, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765411772,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:56:50');
INSERT INTO `sys_oper_log` VALUES (89, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765494482,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:58:12');
INSERT INTO `sys_oper_log` VALUES (90, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650765589847,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 09:59:48');
INSERT INTO `sys_oper_log` VALUES (91, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650766085038,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:08:03');
INSERT INTO `sys_oper_log` VALUES (92, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650766363321,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:12:41');
INSERT INTO `sys_oper_log` VALUES (93, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650766829053,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:20:28');
INSERT INTO `sys_oper_log` VALUES (94, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650766860638,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:20:59');
INSERT INTO `sys_oper_log` VALUES (95, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650766901039,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:21:39');
INSERT INTO `sys_oper_log` VALUES (96, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650766931590,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:22:10');
INSERT INTO `sys_oper_log` VALUES (97, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650768843290,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:54:01');
INSERT INTO `sys_oper_log` VALUES (98, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650768884969,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:54:43');
INSERT INTO `sys_oper_log` VALUES (99, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.3223,\"marksId\":60,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区红炉镇龙井口村龙井村民小组，经营范围包括一般项目：水泥配料用砂岩露天开采（按采矿许可证核定的期限从事经营）；水泥配料用砂岩销售。\",\"longitude\":105.732,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650769002346,\"params\":{},\"companyLevel\":3,\"createTime\":1650732664000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 10:56:40');
INSERT INTO `sys_oper_log` VALUES (100, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"中铁二十一局集团第五工程有限公司\",\"latitude\":29.3802,\"marksId\":38,\"categoryParentId\":12,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"位于永川区文昌路877号，具有铁路工程施工总承包一级、房屋建筑工程施工总承包一级、市政公用工程施工总承包一级；公路工程施工总承包贰级、水利水电工程施工总承包贰级、矿山工程施工总承包贰级、机电工程施工总承包贰级；桥梁工程专业承包一级、隧道工程专业承包一级等资质。公司拥有施工机械、检测等设备400 余台（套），年度施工能力30亿元以上。\",\"longitude\":105.938,\"marketArea\":\"\",\"updateTime\":1650770893227,\"params\":{},\"companyLevel\":3,\"createTime\":1650731035000,\"companyAddress\":\"位于永川区文昌路877号，具有铁路工程施工总承包一级、房屋建筑工程施工总承包一级、市政公用工程施工总承包一级；公路工程施工总承包贰级、水利水电工程施工总承包贰级、矿山工程施工总承包贰级、机电工程施工总承包贰级；桥梁工程专业承包一级、隧道工程专业承包一级等资质。公司拥有施工机械、检测等设备400 余台（套），年度施工能力30亿元以上。\",\"companyPrincipal\":\"张三1\",\"categoryId\":16}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:28:13');
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"orderNum\":5,\"menuName\":\"描述信息\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"1\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:dict:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:54:38');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":5,\"menuName\":\"描述信息\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"depict\",\"component\":\"system/echarts/depict/index\",\"children\":[],\"createTime\":1650772478000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2045,\"menuType\":\"C\",\"perms\":\"system:dict:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:55:06');
INSERT INTO `sys_oper_log` VALUES (103, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"11.22\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"sys_map_depict\",\"dictLabel\":\"产业发展指数\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1650382083000,\"dictCode\":107,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:57:36');
INSERT INTO `sys_oper_log` VALUES (104, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"23423.4\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"sys_map_depict\",\"dictLabel\":\"产业规模\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1650382094000,\"dictCode\":108,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:57:42');
INSERT INTO `sys_oper_log` VALUES (105, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"3.3443\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"sys_map_depict\",\"dictLabel\":\"政策环境\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1650382104000,\"dictCode\":109,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:57:46');
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"log\",\"orderNum\":5,\"menuName\":\"描述信息\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"depict\",\"component\":\"system/echarts/depict/index\",\"children\":[],\"createTime\":1650772478000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2045,\"menuType\":\"C\",\"perms\":\"system:dict:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 11:58:06');
INSERT INTO `sys_oper_log` VALUES (107, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":70,\"companyProfile\":\"位于永川市红炉镇会龙桥村\",\"longitude\":105.758637,\"params\":{},\"companyLevel\":4,\"createTime\":1650774133925,\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":70}', 0, NULL, '2022-04-24 12:22:14');
INSERT INTO `sys_oper_log` VALUES (108, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/58,59,60,70', '125.84.84.70', 'XX XX', '{marksIds=58,59,60,70}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:28:00');
INSERT INTO `sys_oper_log` VALUES (109, '企业管理', 5, 'com.ruoyi.web.controller.map.MapMarksController.export()', 'POST', 1, 'admin', NULL, '/system/marks/export', '125.84.84.70', 'XX XX', '{\"categoryParentId\":1,\"params\":{}}', NULL, 0, NULL, '2022-04-24 12:28:05');
INSERT INTO `sys_oper_log` VALUES (110, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 12:28:12');
INSERT INTO `sys_oper_log` VALUES (111, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.342619,\"marksId\":71,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.896114,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774534795,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:28:54');
INSERT INTO `sys_oper_log` VALUES (112, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆长德聚砂石开采有限公司\",\"latitude\":29.386579,\"marksId\":72,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区大安街道云雾山村碗厂村民小组\",\"longitude\":106.04771,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774551228,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:29:11');
INSERT INTO `sys_oper_log` VALUES (113, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.322263,\"marksId\":73,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区红炉镇龙井口村龙井村民小组\",\"longitude\":105.732337,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774568624,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:29:28');
INSERT INTO `sys_oper_log` VALUES (114, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":74,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川市红炉镇会龙桥村\",\"longitude\":105.758637,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774585501,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:29:45');
INSERT INTO `sys_oper_log` VALUES (115, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区禄仕水泥制品厂\",\"latitude\":29.352131,\"marksId\":75,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川区胜利路办事处永青村谢家老院子村民小组\",\"longitude\":105.89027,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774606979,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:30:07');
INSERT INTO `sys_oper_log` VALUES (116, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区友绍水泥制品加工厂\",\"latitude\":29.209972,\"marksId\":76,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区卫星湖街道石龟寺村圣塘湾村民小组\",\"longitude\":105.872217,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774627247,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:30:27');
INSERT INTO `sys_oper_log` VALUES (117, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区宝巨钢材加工厂\",\"latitude\":29.338605,\"marksId\":77,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.913358,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774650650,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:30:50');
INSERT INTO `sys_oper_log` VALUES (118, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区金利钢材有限公司\",\"latitude\":29.347611,\"marksId\":78,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区望城路39号\",\"longitude\":105.909528,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774668462,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:31:08');
INSERT INTO `sys_oper_log` VALUES (119, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.338605,\"marksId\":79,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.913358,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774686598,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:31:26');
INSERT INTO `sys_oper_log` VALUES (120, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.338605,\"marksId\":80,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.913358,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774717625,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:31:57');
INSERT INTO `sys_oper_log` VALUES (121, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区三友玻璃有限公司\",\"latitude\":29.330699,\"marksId\":81,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区星光大道999号1幢\",\"longitude\":105.928384,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774736894,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:32:16');
INSERT INTO `sys_oper_log` VALUES (122, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区锐峰玻璃制品有限公司\",\"latitude\":29.491495,\"marksId\":82,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区三教镇玉峰村韩家场村民小组\",\"longitude\":105.887261,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650774755628,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:32:35');
INSERT INTO `sys_oper_log` VALUES (123, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区铭森建筑工程有限公司\",\"latitude\":29.361876,\"marksId\":83,\"companyProfile\":\"位于重庆市永川区红炉场镇，主营行业为建筑业，服务领域为房屋建筑工程施工总承包（叁级）、混凝土预制构件专业承包（叁级）（以上经营范围按建筑业企业资质证书核定的事项从事经营）；销售建筑材料（不含油漆及其他危险化学品）、五金、交电。\",\"longitude\":105.746746,\"params\":{},\"companyLevel\":5,\"createTime\":1650774843430,\"categoryId\":16}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":83}', 0, NULL, '2022-04-24 12:34:03');
INSERT INTO `sys_oper_log` VALUES (124, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 12:38:53');
INSERT INTO `sys_oper_log` VALUES (125, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/38,39,40,83', '125.84.84.70', 'XX XX', '{marksIds=38,39,40,83}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:39:08');
INSERT INTO `sys_oper_log` VALUES (126, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"中铁二十一局集团第五工程有限公司\",\"latitude\":29.380211,\"marksId\":84,\"categoryParentId\":12,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"永川区文昌路877号\",\"longitude\":105.937963,\"marketArea\":\"\",\"updateTime\":1650775179695,\"params\":{},\"companyLevel\":3,\"createTime\":1650775133000,\"companyAddress\":\"位于永川区文昌路877号，具有铁路工程施工总承包一级、房屋建筑工程施工总承包一级、市政公用工程施工总承包一级；公路工程施工总承包贰级、水利水电工程施工总承包贰级、矿山工程施工总承包贰级、机电工程施工总承包贰级；桥梁工程专业承包一级、隧道工程专业承包一级等资质。公司拥有施工机械、检测等设备400 余台（套），年度施工能力30亿元以上。\",\"companyPrincipal\":\"张三1\",\"categoryId\":16}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:39:39');
INSERT INTO `sys_oper_log` VALUES (127, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.366255,\"marksId\":85,\"categoryParentId\":12,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"永川区昌州大道中段6号\",\"longitude\":105.905316,\"marketArea\":\"\",\"updateTime\":1650775196231,\"params\":{},\"companyLevel\":3,\"createTime\":1650775133000,\"companyAddress\":\"位于永川区昌州大道中段6号，公司坚持以打造精品、创立名牌的发展战略，坚持干一项工程，树一座丰碑、干一项工程交一方朋友的理念。几十年来，无论是在初创阶段，还是在快速发展时期，无论是在市场低迷时，还是在高峰时，渝永建设集团始终注重企业品牌和形象建设，通过创造、打造名优品牌工程，使企业知名度和社会影响力在同行业内迅速提升。\",\"companyPrincipal\":\"张三2\",\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:39:56');
INSERT INTO `sys_oper_log` VALUES (128, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆首尊建设（集团）有限责任公司\",\"latitude\":29.354968,\"marksId\":86,\"categoryParentId\":12,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProducts\":\"产品3\",\"companyProfile\":\"永川区汇龙大道236号\",\"longitude\":105.919248,\"marketArea\":\"\",\"updateTime\":1650775209671,\"params\":{},\"companyLevel\":3,\"createTime\":1650775133000,\"companyAddress\":\"位于永川区汇龙大道236号，公司自成立以来，坚持在技术安全、项目质量上为客户提供具有竞争力的产品，具有建筑工程施工总承包壹级、市政公用工程施工总承包壹级、公路工程施工总承 包叁级、电子与智能化工程专业承包贰级、消防设施工程专业承包贰级、城市及道路照明工程专业承包叁级、地基基础工程专业承包叁级、环保工程专业承包叁级、建筑机电安装工程专业承包叁级、建筑幕墙工程专业承包贰级、钢结构工程专业承包叁级资质。\",\"companyPrincipal\":\"张三3\",\"categoryId\":14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:40:09');
INSERT INTO `sys_oper_log` VALUES (129, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区铭森建筑工程有限公司\",\"latitude\":29.361876,\"marksId\":87,\"categoryParentId\":12,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"重庆市永川区红炉场镇\",\"longitude\":105.746746,\"marketArea\":\"\",\"updateTime\":1650775229641,\"params\":{},\"companyLevel\":3,\"createTime\":1650775133000,\"companyAddress\":\"位于重庆市永川区红炉场镇，主营行业为建筑业，服务领域为房屋建筑工程施工总承包（叁级）、混凝土预制构件专业承包（叁级）（以上经营范围按建筑业企业资质证书核定的事项从事经营）；销售建筑材料（不含油漆及其他危险化学品）、五金、交电。\",\"companyPrincipal\":\"张三4\",\"categoryId\":13}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:40:29');
INSERT INTO `sys_oper_log` VALUES (130, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/42,43,44', '125.84.84.70', 'XX XX', '{marksIds=42,43,44}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:40:40');
INSERT INTO `sys_oper_log` VALUES (131, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 12:40:45');
INSERT INTO `sys_oper_log` VALUES (132, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆能华物流有限公司\",\"latitude\":29.320168,\"marksId\":88,\"categoryParentId\":17,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProfile\":\"重庆市永川区工业园区凤凰湖工业园\",\"longitude\":105.935855,\"marketArea\":\"\",\"updateTime\":1650775264277,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区工业园区凤凰湖工业园内，经营范围包括普通货运，货物专用运输（集装箱），货物专用运输（罐式）；建筑机械设备租赁；仓储服务（不含危险化学品）。\",\"companyPrincipal\":\"张三1\",\"categoryId\":21}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:41:04');
INSERT INTO `sys_oper_log` VALUES (133, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆贝斯特物流有限公司\",\"latitude\":29.344132,\"marksId\":89,\"categoryParentId\":17,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProfile\":\"重庆市永川区泸州街166号\",\"longitude\":105.89772,\"marketArea\":\"\",\"updateTime\":1650775277773,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区泸州街166号，许可项目：普通货运（不含危险品）；大型物件运输；货物进出口；商品配送；物流策划；产品包装；货运代理、货运联营服务；仓储(不含危险品)、装卸、搬运服务。\",\"companyPrincipal\":\"张三2\",\"categoryId\":20}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:41:17');
INSERT INTO `sys_oper_log` VALUES (134, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区申顺物流有限公司\",\"latitude\":29.012968,\"marksId\":90,\"categoryParentId\":17,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProfile\":\"重庆市永川区朱沱镇龙川路115号\",\"longitude\":105.846474,\"marketArea\":\"\",\"updateTime\":1650775293981,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区朱沱镇龙川路115号，所属行业为多式联运和运输代理业，经营范围包含：一般项目：国内货物运输代理，普通货物仓储服务（不含危险化学品等需许可审批的项目），装卸搬运。\",\"companyPrincipal\":\"张三3\",\"categoryId\":19}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:41:34');
INSERT INTO `sys_oper_log` VALUES (135, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆伍合物流有限公司永川分公司\",\"latitude\":29.328024,\"marksId\":91,\"categoryParentId\":17,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProfile\":\"重庆市永川区南大街办事处大南村陈家沟村民小组300号\",\"longitude\":105.885257,\"marketArea\":\"\",\"updateTime\":1650775311116,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:41:51');
INSERT INTO `sys_oper_log` VALUES (136, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/46,47,48', '125.84.84.70', 'XX XX', '{marksIds=46,47,48}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:42:06');
INSERT INTO `sys_oper_log` VALUES (137, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 12:42:13');
INSERT INTO `sys_oper_log` VALUES (138, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆名威建设工程咨询有限公司\",\"latitude\":29.357011,\"marksId\":92,\"categoryParentId\":22,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"重庆市永川区汇龙大道121号\",\"longitude\":105.920928,\"marketArea\":\"\",\"updateTime\":1650775348276,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于重庆市永川区汇龙大道121号。经营范围包括许可项目：施工专业作业；建设工程质量检测。一般项目：工程造价咨询业务；建筑行业（建筑工程）设计（甲级）；工程测绘（丙级）；市政公用工程监理（甲级）；房屋建筑工程监理（甲级）；城乡规划编制（乙级）；风景园林工程设计专项（甲级），公路行业（公路）专业（丙级），市政行业（道路、桥梁工程）专业设计（乙级）；农林行业（农业综合开发生态工程、设施农业工程）专业乙级；工程咨询（乙级）；工程招标代理（甲级）；城市园林绿化监理（乙级）；中央投资项目招标代理（预备级）。\",\"companyPrincipal\":\"张三1\",\"categoryId\":23}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:42:28');
INSERT INTO `sys_oper_log` VALUES (139, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆筑天誉城建筑科技有限公司\",\"latitude\":29.330699,\"marksId\":93,\"categoryParentId\":22,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"重庆市永川区星光大道999号1幢\",\"longitude\":105.928384,\"marketArea\":\"\",\"updateTime\":1650775365261,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于重庆市永川区星光大道999号1幢（重庆永川工业园区凤凰湖工业园内），是一家集设计、生产、销售、BIM咨询为一体的建筑机电安装职支撑系统生产制造厂家，主要产品为抗震支架、装配式成品支架、地下综合管廊支架等。\",\"companyPrincipal\":\"张三2\",\"categoryId\":23}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:42:45');
INSERT INTO `sys_oper_log` VALUES (140, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆同盛建筑设计咨询有限公司\",\"latitude\":29.359407,\"marksId\":94,\"categoryParentId\":22,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProducts\":\"产品3\",\"companyProfile\":\"永川区人民大道555号6幢\",\"longitude\":105.933214,\"marketArea\":\"\",\"updateTime\":1650775382025,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于永川区人民大道555号6幢，经营范围包括许可项目：测绘服务，水利工程质量检测，建设工程质量检测，水利工程建设监理，工程造价咨询业务，建设工程设计。一般项目：地质勘查技术服务，工程管理服务，规划设计管理，专业设计服务，水文服务，水利相关咨询服务，水土流失防治服务，水资源管理。\",\"companyPrincipal\":\"张三3\",\"categoryId\":24}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:43:02');
INSERT INTO `sys_oper_log` VALUES (141, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆良益工程咨询有限公司\",\"latitude\":29.410705,\"marksId\":95,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"重庆市永川区双石镇丁家岩村五洞桥村民小组\",\"longitude\":105.861439,\"marketArea\":\"\",\"updateTime\":1650775401990,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于重庆市永川区双石镇丁家岩村五洞桥村民小组，经营范围包括建筑工程咨询；工程造价咨询；工业设计、包装装潢设计、展台设计、模型设计；市政公用工程施工总承包；建筑工程施工总承包；工程招标代理。\",\"companyPrincipal\":\"张三4\",\"categoryId\":25}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:43:22');
INSERT INTO `sys_oper_log` VALUES (142, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆国乔智能工程有限公司\",\"latitude\":29.341492,\"marksId\":96,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"重庆市永川区内环南路777号一区1幢附2号\",\"longitude\":105.906004,\"marketArea\":\"\",\"updateTime\":1650775417733,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于重庆市永川区内环南路777号一区1幢附2号，经营范围包括一般项目：智能工程的开发、运用、咨询服务；智能化管理系统开发应用；园林绿化；从事建筑相关业务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:43:37');
INSERT INTO `sys_oper_log` VALUES (143, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆致鸿建筑科技有限公司\",\"latitude\":29.366782,\"marksId\":97,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"重庆市永川区昌州大道东段801号重庆水利电力职业技术学院大学生创业园302号\",\"longitude\":105.933768,\"marketArea\":\"\",\"updateTime\":1650775435579,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于重庆市永川区昌州大道东段801号重庆水利电力职业技术学院大学生创业园302号。经营范围包括一般项目：技术服务、技术开发、技术咨询、技术交流、技术转让、技术推广，计算机软硬件及辅助设备批发，计算机软硬件及辅助设备零售，工程管理服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:43:55');
INSERT INTO `sys_oper_log` VALUES (144, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆森成建筑科技有限公司\",\"latitude\":29.34824,\"marksId\":98,\"categoryParentId\":22,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"永川区人民大道16号6幢3-7\",\"longitude\":105.917875,\"marketArea\":\"\",\"updateTime\":1650775451790,\"params\":{},\"companyLevel\":3,\"createTime\":1650775334000,\"companyAddress\":\"位于永川区人民大道16号6幢3-7。是一家以节能、环保、生态为理念的大型建筑建材相关产业的平台公司，建立了强大的设计、生产、配送、施工、监理等相关联的SBC服务系统，有效解决了行业痛点，最大化地提升了工作效率和生产力。\",\"companyPrincipal\":\"张三4\",\"categoryId\":26}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:44:11');
INSERT INTO `sys_oper_log` VALUES (145, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/53,54,55', '125.84.84.70', 'XX XX', '{marksIds=53,54,55}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:44:29');
INSERT INTO `sys_oper_log` VALUES (146, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 12:44:37');
INSERT INTO `sys_oper_log` VALUES (147, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.342929,\"marksId\":104,\"companyProfile\":\"位于重庆市永川区，拥有玻璃深加工生产设备（4200平钢生产线、中空生产线各一条，配套设备有直线磨边机二台，钻孔机一台，异型磨边机一台，清洗机一台等设备），完善的质量控制方法和齐全的检验仪器设备。公司目前的主要产品：各种规格的钢化玻璃、中空玻璃，产品已通过国家强制性产品认证，正常投产运营中。  \",\"longitude\":105.841238,\"params\":{},\"companyLevel\":4,\"createTime\":1650775559571,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":104}', 0, NULL, '2022-04-24 12:45:59');
INSERT INTO `sys_oper_log` VALUES (148, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"中交一公局重庆城市建设发展有限公司\",\"latitude\":29.355409,\"marksId\":99,\"categoryParentId\":7,\"actualCapacity\":\"270\",\"companyPhone\":\"18523709707\",\"companyProducts\":\"产品1\",\"companyProfile\":\"永川区和顺大道 799 号\",\"longitude\":105.948931,\"marketArea\":\"\",\"updateTime\":1650775578685,\"params\":{},\"companyLevel\":3,\"createTime\":1650775478000,\"companyAddress\":\"位于永川区和顺大道 799 号，厂址位于陈食街道。设计产能为15万立方米，主要生产钢筋桁架叠合板、叠合梁、预制楼梯、预制阳台、预制空调板、预制柱、预制剪力墙及部分市政预制混凝土构件。\",\"companyPrincipal\":\"张三1\",\"categoryId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:46:18');
INSERT INTO `sys_oper_log` VALUES (149, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆川盛建材科技有限公司\",\"latitude\":29.383589,\"marksId\":100,\"categoryParentId\":7,\"actualCapacity\":\"315\",\"companyPhone\":\"18523709708\",\"companyProducts\":\"产品2\",\"companyProfile\":\"重庆市永川区凤凰湖工业园区大安组团\",\"longitude\":105.999201,\"marketArea\":\"\",\"updateTime\":1650775596895,\"params\":{},\"companyLevel\":3,\"createTime\":1650775478000,\"companyAddress\":\"位于重庆市永川区凤凰湖工业园区大安组团，是一家集产品研发、生产、销售为一体的国家重点研发绿色环保节能材料的公司。拥有员工300余人，总资产3.5亿元，公司占地面积300余亩，公司目前拥有三条德国先进技术的生产线，主要生产加气混凝土砌块（AAC）、加气混凝土板材(ALC)等装配式建筑节能环保建筑墙体材料，年产量150万m³，是西南地区领先的AAC、ALC生产企业。\",\"companyPrincipal\":\"张三2\",\"categoryId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:46:36');
INSERT INTO `sys_oper_log` VALUES (150, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆领固新材料科技有限公司\",\"latitude\":29.483984,\"marksId\":101,\"categoryParentId\":7,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProducts\":\"产品3\",\"companyProfile\":\"重庆市永川区三教镇(重庆永川高新区三教产业园)\",\"longitude\":105.880254,\"marketArea\":\"\",\"updateTime\":1650775617076,\"params\":{},\"companyLevel\":3,\"createTime\":1650775478000,\"companyAddress\":\"位于重庆市永川区三教镇(重庆永川高新区三教产业园)，是一家专业从事装配式轻质陶粒隔墙板、蒸压钢筋陶粒轻质隔墙板的生产、销售和施工为一体的新材料高科技企业。目前拥有年生产100万m²轻质陶粒隔墙板、高温蒸压钢筋陶粒轻质隔墙板（空心板、实心板、L型板、T型板）的生产能力\",\"companyPrincipal\":\"张三3\",\"categoryId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:46:57');
INSERT INTO `sys_oper_log` VALUES (151, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"中交世通重工有限公司\",\"latitude\":29.355327,\"marksId\":102,\"categoryParentId\":7,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProducts\":\"产品4\",\"companyProfile\":\"重庆市永川区人民大道855号\",\"longitude\":105.926766,\"marketArea\":\"\",\"updateTime\":1650775632038,\"params\":{},\"companyLevel\":3,\"createTime\":1650775478000,\"companyAddress\":\"位于重庆市永川区人民大道855号，总占地面积172亩，厂区面积为45595平方米。主要产品定位：房建钢结构、桥梁钢结构、交通工程护栏系列、钢结构桥梁U型肋，年产能将达5万吨；钢结构加工基地为西南片区设备一流、工艺一流、管理一流的前进性刚机构加工基地同时也是公司人才培养、培训学习基地。\",\"companyPrincipal\":\"张三4\",\"categoryId\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:47:12');
INSERT INTO `sys_oper_log` VALUES (152, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/103', '125.84.84.70', 'XX XX', '{marksIds=103}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:48:16');
INSERT INTO `sys_oper_log` VALUES (153, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.304152,\"marksId\":80,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.936693,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650776370432,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 12:59:30');
INSERT INTO `sys_oper_log` VALUES (154, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.3042,\"marksId\":80,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆永川工业园区凤凰湖工业园\",\"longitude\":105.937,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650776503609,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 13:01:43');
INSERT INTO `sys_oper_log` VALUES (155, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/85,86,87', '125.84.84.70', 'XX XX', '{marksIds=85,86,87}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 13:06:25');
INSERT INTO `sys_oper_log` VALUES (156, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.366255,\"marksId\":105,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905316,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988296,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":105}', 0, NULL, '2022-04-24 13:09:48');
INSERT INTO `sys_oper_log` VALUES (157, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/104', '125.84.84.70', 'XX XX', '{marksIds=104}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 13:10:40');
INSERT INTO `sys_oper_log` VALUES (158, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.342929,\"marksId\":106,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841238,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110178,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":106}', 0, NULL, '2022-04-24 13:11:50');
INSERT INTO `sys_oper_log` VALUES (159, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆首尊建设（集团）有限责任公司\",\"latitude\":29.354968,\"marksId\":107,\"companyProfile\":\"永川区汇龙大道236号\",\"longitude\":105.919248,\"params\":{},\"companyLevel\":4,\"createTime\":1650777308117,\"categoryId\":14}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":107}', 0, NULL, '2022-04-24 13:15:08');
INSERT INTO `sys_oper_log` VALUES (160, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区铭森建筑工程有限公司\",\"latitude\":29.361876,\"marksId\":108,\"companyProfile\":\"重庆市永川区红炉场镇\",\"longitude\":105.746746,\"params\":{},\"companyLevel\":4,\"createTime\":1650777348445,\"categoryId\":13}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":108}', 0, NULL, '2022-04-24 13:15:48');
INSERT INTO `sys_oper_log` VALUES (161, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500113\",\"companyName\":\"重庆市永川区禄仕水泥制品厂\",\"latitude\":29.348413,\"marksId\":75,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川区胜利路办事处永青村谢家老院子村民小组\",\"longitude\":106.696207,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650777733803,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 13:22:14');
INSERT INTO `sys_oper_log` VALUES (162, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"companyName\":\"123\",\"latitude\":29.094974,\"marksId\":109,\"companyProfile\":\"123\",\"longitude\":106.261777,\"params\":{},\"companyLevel\":3,\"createTime\":1650777817139,\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":109}', 0, NULL, '2022-04-24 13:23:37');
INSERT INTO `sys_oper_log` VALUES (163, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500154\",\"companyName\":\"123\",\"latitude\":31.431252,\"marksId\":110,\"companyProfile\":\"123\",\"longitude\":108.547718,\"params\":{},\"companyLevel\":3,\"createTime\":1650777913228,\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":110}', 0, NULL, '2022-04-24 13:25:13');
INSERT INTO `sys_oper_log` VALUES (164, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.337862,\"marksId\":79,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.914205,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650780789505,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 14:13:09');
INSERT INTO `sys_oper_log` VALUES (165, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.3426,\"marksId\":71,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.896,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650806797210,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 21:26:37');
INSERT INTO `sys_oper_log` VALUES (166, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.3426,\"marksId\":71,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"位于重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.896,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650807062592,\"params\":{},\"companyLevel\":3,\"createTime\":1650774492000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 21:31:03');
INSERT INTO `sys_oper_log` VALUES (167, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/109,110', '125.84.84.70', 'XX XX', '{marksIds=109,110}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:04:12');
INSERT INTO `sys_oper_log` VALUES (168, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"111\",\"latitude\":30.807621,\"marksId\":111,\"companyProfile\":\"111\",\"longitude\":108.408591,\"params\":{},\"companyLevel\":4,\"createTime\":1650812718751,\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":111}', 0, NULL, '2022-04-24 23:05:18');
INSERT INTO `sys_oper_log` VALUES (169, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"333\",\"latitude\":31.160416,\"marksId\":112,\"companyProfile\":\"333\",\"longitude\":108.39336,\"params\":{},\"companyLevel\":4,\"createTime\":1650812736654,\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":112}', 0, NULL, '2022-04-24 23:05:36');
INSERT INTO `sys_oper_log` VALUES (170, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"444\",\"latitude\":30.327548,\"marksId\":113,\"companyProfile\":\"44\",\"longitude\":107.332511,\"params\":{},\"companyLevel\":4,\"createTime\":1650812768274,\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":113}', 0, NULL, '2022-04-24 23:06:08');
INSERT INTO `sys_oper_log` VALUES (171, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/111,112,113', '125.84.84.70', 'XX XX', '{marksIds=111,112,113}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:06:41');
INSERT INTO `sys_oper_log` VALUES (172, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/71,72,73,74,75,76,77,78,79,80', '125.84.84.70', 'XX XX', '{marksIds=71,72,73,74,75,76,77,78,79,80}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:15:56');
INSERT INTO `sys_oper_log` VALUES (173, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/81,82', '125.84.84.70', 'XX XX', '{marksIds=81,82}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:16:02');
INSERT INTO `sys_oper_log` VALUES (174, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-24 23:16:27');
INSERT INTO `sys_oper_log` VALUES (175, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.240702,\"marksId\":114,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.842121,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813433406,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:17:13');
INSERT INTO `sys_oper_log` VALUES (176, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆长德聚砂石开采有限公司\",\"latitude\":29.386579,\"marksId\":115,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区大安街道云雾山村碗厂村民小组\",\"longitude\":106.04771,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813483678,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:18:03');
INSERT INTO `sys_oper_log` VALUES (177, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.322263,\"marksId\":116,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区红炉镇龙井口村龙井村民小组\",\"longitude\":105.732337,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813540537,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:19:00');
INSERT INTO `sys_oper_log` VALUES (178, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":117,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川市红炉镇会龙桥村\",\"longitude\":105.758637,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813581084,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:19:41');
INSERT INTO `sys_oper_log` VALUES (179, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区禄仕水泥制品厂\",\"latitude\":29.352131,\"marksId\":118,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川区胜利路办事处永青村谢家老院子村民小组\",\"longitude\":105.89027,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813621411,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:20:21');
INSERT INTO `sys_oper_log` VALUES (180, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区友绍水泥制品加工厂\",\"latitude\":29.209972,\"marksId\":119,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区卫星湖街道石龟寺村圣塘湾村民小组\",\"longitude\":105.872217,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813663514,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:21:03');
INSERT INTO `sys_oper_log` VALUES (181, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区宝巨钢材加工厂\",\"latitude\":29.338605,\"marksId\":120,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.913358,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813769678,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:22:49');
INSERT INTO `sys_oper_log` VALUES (182, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区金利钢材有限公司\",\"latitude\":29.347611,\"marksId\":121,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区望城路39号\",\"longitude\":105.909528,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813811101,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:23:31');
INSERT INTO `sys_oper_log` VALUES (183, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.337862,\"marksId\":122,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川和旺物流园内B区4栋19号\",\"longitude\":105.914205,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813873426,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:24:33');
INSERT INTO `sys_oper_log` VALUES (184, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.304152,\"marksId\":123,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆永川工业园区凤凰湖工业园\",\"longitude\":105.936693,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813922894,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:25:22');
INSERT INTO `sys_oper_log` VALUES (185, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区三友玻璃有限公司\",\"latitude\":29.330699,\"marksId\":124,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区星光大道999号1幢\",\"longitude\":105.928384,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650813976572,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:26:16');
INSERT INTO `sys_oper_log` VALUES (186, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区锐峰玻璃制品有限公司\",\"latitude\":29.491495,\"marksId\":125,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区三教镇玉峰村韩家场村民小组\",\"longitude\":105.887261,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650814013559,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:26:53');
INSERT INTO `sys_oper_log` VALUES (187, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"333\",\"latitude\":29.216532,\"marksId\":126,\"companyProfile\":\"2222\",\"longitude\":105.943972,\"params\":{},\"companyLevel\":4,\"createTime\":1650814450363,\"categoryId\":14}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":126}', 0, NULL, '2022-04-24 23:34:10');
INSERT INTO `sys_oper_log` VALUES (188, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '125.84.84.70', 'XX XX', '{\"createBy\":\"admin\",\"newsContent\":\"<strong>新版本内容</strong><strong>﻿</strong><img src=\\\"\\\" />\",\"newsId\":1,\"createTime\":1648975717000,\"updateBy\":\"admin\",\"newsTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"remark\":\"管理员\",\"updateTime\":1650815445319,\"params\":{},\"newsType\":\"2\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:50:45');
INSERT INTO `sys_oper_log` VALUES (189, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '125.84.84.70', 'XX XX', '{\"createBy\":\"admin\",\"newsContent\":\"<strong>新版本内容﻿</strong><img src=\\\"\\\" /><img src=\\\"\\\" />\",\"newsId\":1,\"createTime\":1648975717000,\"updateBy\":\"admin\",\"newsTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"remark\":\"管理员\",\"updateTime\":1650815516379,\"params\":{},\"newsType\":\"2\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-24 23:51:56');
INSERT INTO `sys_oper_log` VALUES (190, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500102\",\"companyName\":\"111\",\"latitude\":29.656193,\"marksId\":127,\"companyProfile\":\"11\",\"longitude\":107.327996,\"params\":{},\"companyLevel\":4,\"createTime\":1650816094142,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":127}', 0, NULL, '2022-04-25 00:01:34');
INSERT INTO `sys_oper_log` VALUES (191, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500120\",\"companyName\":\"333\",\"latitude\":29.471049,\"marksId\":126,\"categoryParentId\":12,\"companyProfile\":\"2222\",\"longitude\":106.169715,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"updateTime\":1650816190540,\"params\":{},\"companyLevel\":4,\"createTime\":1650814450000,\"categoryId\":14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:03:10');
INSERT INTO `sys_oper_log` VALUES (192, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区申顺物流有限公司\",\"latitude\":29.013,\"marksId\":90,\"categoryParentId\":17,\"actualCapacity\":\"360\",\"companyPhone\":\"18523709709\",\"companyProfile\":\"重庆市永川区朱沱镇龙川路115号\",\"longitude\":105.846,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"updateTime\":1650816190928,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区朱沱镇龙川路115号，所属行业为多式联运和运输代理业，经营范围包含：一般项目：国内货物运输代理，普通货物仓储服务（不含危险化学品等需许可审批的项目），装卸搬运。\",\"companyPrincipal\":\"张三3\",\"categoryId\":19}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:03:10');
INSERT INTO `sys_oper_log` VALUES (193, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.29831,\"marksId\":114,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.796,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650816259681,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:04:19');
INSERT INTO `sys_oper_log` VALUES (194, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆长德聚砂石开采有限公司\",\"latitude\":29.386579,\"marksId\":115,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区大安街道云雾山村碗厂村民小组\",\"longitude\":106.04771,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650816492903,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:08:12');
INSERT INTO `sys_oper_log` VALUES (195, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区禄仕水泥制品厂\",\"latitude\":29.352131,\"marksId\":118,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区胜利路办事处永青村谢家老院子村民小组\",\"longitude\":105.89027,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650816552792,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:09:12');
INSERT INTO `sys_oper_log` VALUES (196, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.304152,\"marksId\":123,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆永川工业园区凤凰湖工业园\",\"longitude\":105.936693,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650816830152,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:13:50');
INSERT INTO `sys_oper_log` VALUES (197, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.363096,\"marksId\":123,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆永川工业园区凤凰湖工业园\",\"longitude\":105.916095,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650817054984,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:17:35');
INSERT INTO `sys_oper_log` VALUES (198, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.337862,\"marksId\":122,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川和旺物流园内B区4栋19号\",\"longitude\":105.914205,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650817149232,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:19:09');
INSERT INTO `sys_oper_log` VALUES (199, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.337862,\"marksId\":122,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川和旺物流园内B区4栋19号\",\"longitude\":105.914205,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650817232745,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:20:32');
INSERT INTO `sys_oper_log` VALUES (200, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.332931,\"marksId\":122,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川和旺物流园内B区4栋19号\",\"longitude\":105.910166,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650817248089,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:20:48');
INSERT INTO `sys_oper_log` VALUES (201, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/127', '127.0.0.1', '内网IP', '{marksIds=127}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:29:49');
INSERT INTO `sys_oper_log` VALUES (202, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500110\",\"companyName\":\"333\",\"latitude\":28.615484,\"marksId\":126,\"categoryParentId\":12,\"companyProfile\":\"2222\",\"longitude\":106.679779,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"updateTime\":1650818079712,\"params\":{},\"companyLevel\":4,\"createTime\":1650814450000,\"categoryId\":14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 00:34:40');
INSERT INTO `sys_oper_log` VALUES (203, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.240702,\"marksId\":114,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.842121,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650847968399,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:52:48');
INSERT INTO `sys_oper_log` VALUES (204, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆长德聚砂石开采有限公司\",\"latitude\":29.240702,\"marksId\":115,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.842121,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650848000516,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:53:20');
INSERT INTO `sys_oper_log` VALUES (205, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.200949,\"marksId\":114,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.765445,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650848117366,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:55:17');
INSERT INTO `sys_oper_log` VALUES (206, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区友绍水泥制品加工厂\",\"latitude\":29.093004,\"marksId\":119,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区卫星湖街道石龟寺村圣塘湾村民小组\",\"longitude\":105.777804,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650848148234,\"params\":{},\"companyLevel\":3,\"createTime\":1650813387000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:55:48');
INSERT INTO `sys_oper_log` VALUES (207, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/114,115,116,117,118,119,120,121,122,123', '125.84.84.70', 'XX XX', '{marksIds=114,115,116,117,118,119,120,121,122,123}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:56:57');
INSERT INTO `sys_oper_log` VALUES (208, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/124,125', '125.84.84.70', 'XX XX', '{marksIds=124,125}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:57:00');
INSERT INTO `sys_oper_log` VALUES (209, '企业管理批量导入', 6, 'com.ruoyi.web.controller.map.MapMarksController.importData()', 'POST', 1, 'admin', NULL, '/system/marks/importData', '125.84.84.70', 'XX XX', '', '{\"msg\":\"\",\"code\":200}', 0, NULL, '2022-04-25 08:57:12');
INSERT INTO `sys_oper_log` VALUES (210, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永恒砂石开采有限公司\",\"latitude\":29.207111,\"marksId\":128,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处黄瓜山村八角丘村民小组63号\",\"longitude\":105.781924,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650848276726,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:57:56');
INSERT INTO `sys_oper_log` VALUES (211, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆长德聚砂石开采有限公司\",\"latitude\":29.406099,\"marksId\":129,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区大安街道云雾山村碗厂村民小组\",\"longitude\":106.041476,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650848318556,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 08:58:38');
INSERT INTO `sys_oper_log` VALUES (212, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.322263,\"marksId\":130,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区红炉镇龙井口村龙井村民小组\",\"longitude\":105.732337,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850308402,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:31:48');
INSERT INTO `sys_oper_log` VALUES (213, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":131,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川市红炉镇会龙桥村\",\"longitude\":105.758637,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850390671,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:33:10');
INSERT INTO `sys_oper_log` VALUES (214, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'test', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.322263,\"marksId\":130,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区红炉镇龙井口村龙井村民小组\",\"longitude\":105.732337,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850434845,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:33:54');
INSERT INTO `sys_oper_log` VALUES (215, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":131,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川市红炉镇会龙桥村\",\"longitude\":105.758637,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850532787,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:35:32');
INSERT INTO `sys_oper_log` VALUES (216, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'test', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆华新参天水泥有限公司\",\"latitude\":29.341,\"marksId\":131,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"永川市红炉镇会龙桥村\",\"longitude\":105.758637,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850581770,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:36:21');
INSERT INTO `sys_oper_log` VALUES (217, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'test', NULL, '/system/marks', '183.230.199.58', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区禄仕水泥制品厂\",\"latitude\":29.068944,\"marksId\":132,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区禄仕水泥制品厂\",\"longitude\":105.840208,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850806622,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:40:06');
INSERT INTO `sys_oper_log` VALUES (218, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区友绍水泥制品加工厂\",\"latitude\":29.356117,\"marksId\":133,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区友绍水泥制品加工厂\",\"longitude\":105.927376,\"categoryColorIndex\":1,\"categoryParentColorIndex\":1,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850905191,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:41:45');
INSERT INTO `sys_oper_log` VALUES (219, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区参天砂岩开采有限公司\",\"latitude\":29.322263,\"marksId\":130,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区红炉镇龙井口村龙井村民小组\",\"longitude\":105.732337,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650850958870,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:42:38');
INSERT INTO `sys_oper_log` VALUES (220, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区宝巨钢材加工厂\",\"latitude\":29.338605,\"marksId\":134,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区南大街办事处华创大道31号\",\"longitude\":105.913358,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851090968,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:44:51');
INSERT INTO `sys_oper_log` VALUES (221, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区金利钢材有限公司\",\"latitude\":29.347611,\"marksId\":135,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区望城路39号\",\"longitude\":105.909528,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851129858,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:45:29');
INSERT INTO `sys_oper_log` VALUES (222, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500120\",\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.44971,\"marksId\":136,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"和旺物流园内B区4栋19号\",\"longitude\":106.234002,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851180694,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:46:20');
INSERT INTO `sys_oper_log` VALUES (223, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市渝琥玻璃有限公司\",\"latitude\":29.304152,\"marksId\":137,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆永川工业园区凤凰湖工业园\",\"longitude\":105.936693,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851202265,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:46:42');
INSERT INTO `sys_oper_log` VALUES (224, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区三友玻璃有限公司\",\"latitude\":29.330699,\"marksId\":138,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区星光大道999号1幢\",\"longitude\":105.928384,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851224537,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:47:04');
INSERT INTO `sys_oper_log` VALUES (225, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区锐峰玻璃制品有限公司\",\"latitude\":29.491495,\"marksId\":139,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"重庆市永川区三教镇玉峰村韩家场村民小组\",\"longitude\":105.887261,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851243128,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:47:23');
INSERT INTO `sys_oper_log` VALUES (226, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆市永川区瑞豪钢材有限公司\",\"latitude\":29.337862,\"marksId\":136,\"categoryParentId\":1,\"actualCapacity\":\"\",\"companyPhone\":\"\",\"companyProfile\":\"和旺物流园内B区4栋19号\",\"longitude\":105.914205,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"marketArea\":\"\",\"designCapacity\":\"\",\"updateTime\":1650851423480,\"params\":{},\"companyLevel\":3,\"createTime\":1650848233000,\"companyAddress\":\"\",\"companyPrincipal\":\"\",\"categoryId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:50:23');
INSERT INTO `sys_oper_log` VALUES (227, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '125.84.84.70', 'XX XX', '{\"createBy\":\"admin\",\"newsContent\":\"<strong>新版本内容﻿</strong><strong>﻿</strong><img src=\\\"\\\" /><img src=\\\"\\\" /><img src=\\\"\\\" />\",\"newsId\":1,\"createTime\":1648975717000,\"updateBy\":\"admin\",\"newsTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"remark\":\"管理员\",\"updateTime\":1650851815373,\"params\":{},\"newsType\":\"2\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 09:56:55');
INSERT INTO `sys_oper_log` VALUES (228, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500116\",\"companyName\":\"111\",\"latitude\":29.167511,\"marksId\":140,\"companyProfile\":\"111\",\"longitude\":106.223324,\"params\":{},\"companyLevel\":5,\"createTime\":1650852171563,\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":140}', 0, NULL, '2022-04-25 10:02:51');
INSERT INTO `sys_oper_log` VALUES (229, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'admin', NULL, '/system/marks/140', '125.84.84.70', 'XX XX', '{marksIds=140}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 10:03:27');
INSERT INTO `sys_oper_log` VALUES (230, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'test', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500116\",\"companyName\":\"111\",\"latitude\":29.093136,\"marksId\":141,\"companyProfile\":\"111\",\"longitude\":106.278256,\"params\":{},\"companyLevel\":4,\"createTime\":1650852466155,\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":141}', 0, NULL, '2022-04-25 10:07:46');
INSERT INTO `sys_oper_log` VALUES (231, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'test', NULL, '/system/marks/141', '125.84.84.70', 'XX XX', '{marksIds=141}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 10:08:14');
INSERT INTO `sys_oper_log` VALUES (232, '企业管理', 1, 'com.ruoyi.web.controller.map.MapMarksController.add()', 'POST', 1, 'test', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500113\",\"companyName\":\"111\",\"latitude\":29.402348,\"marksId\":142,\"companyProfile\":\"111\",\"longitude\":106.540603,\"params\":{},\"companyLevel\":5,\"createTime\":1650876395332,\"categoryId\":3}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":142}', 0, NULL, '2022-04-25 16:46:35');
INSERT INTO `sys_oper_log` VALUES (233, '企业管理', 3, 'com.ruoyi.web.controller.map.MapMarksController.remove()', 'DELETE', 1, 'test', NULL, '/system/marks/142', '125.84.84.70', 'XX XX', '{marksIds=142}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 16:46:57');
INSERT INTO `sys_oper_log` VALUES (234, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/89f4ae06-3218-45dc-8d1f-0a1046869290\",\"params\":{},\"bannerLink\":\"https://www.baidu.com\",\"id\":73}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":73}', 0, NULL, '2022-04-25 17:15:15');
INSERT INTO `sys_oper_log` VALUES (235, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/425287b6-1572-46e2-800b-3e1da0e880c5\",\"params\":{},\"bannerLink\":\"https://www.baidu.com\",\"id\":74}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":74}', 0, NULL, '2022-04-25 17:15:53');
INSERT INTO `sys_oper_log` VALUES (236, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/1fdac822-8d42-4cee-940d-f13e99045e39\",\"params\":{},\"bannerLink\":\"https://123.com\",\"id\":75}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":75}', 0, NULL, '2022-04-25 17:17:47');
INSERT INTO `sys_oper_log` VALUES (237, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/33084db7-0f7b-453a-af1b-423bd24318ae\",\"params\":{},\"bannerLink\":\"https://w.123\",\"id\":76}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":76}', 0, NULL, '2022-04-25 17:23:30');
INSERT INTO `sys_oper_log` VALUES (238, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/da8f9efb-3b7a-4b79-9bbc-a75c291f48d9\",\"params\":{},\"bannerLink\":\"http://123\",\"id\":77}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":77}', 0, NULL, '2022-04-25 17:24:14');
INSERT INTO `sys_oper_log` VALUES (239, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650878743088,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:25:43');
INSERT INTO `sys_oper_log` VALUES (240, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650878840022,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:27:21');
INSERT INTO `sys_oper_log` VALUES (241, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650878857633,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:27:38');
INSERT INTO `sys_oper_log` VALUES (242, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650878896853,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:28:17');
INSERT INTO `sys_oper_log` VALUES (243, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650878949100,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:29:09');
INSERT INTO `sys_oper_log` VALUES (244, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/87e65595-e618-4c7b-9f17-390f984903ed\",\"params\":{},\"bannerLink\":\"https:/123\"}', '{\"msg\":\"新增失败，地址必须以http(s)://开头\",\"code\":500}', 0, NULL, '2022-04-25 17:30:15');
INSERT INTO `sys_oper_log` VALUES (245, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/87e65595-e618-4c7b-9f17-390f984903ed\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":78}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":78}', 0, NULL, '2022-04-25 17:30:26');
INSERT INTO `sys_oper_log` VALUES (246, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/87a27740-0548-482e-871f-aa16b2bcdfb6\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":79}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":79}', 0, NULL, '2022-04-25 17:31:43');
INSERT INTO `sys_oper_log` VALUES (247, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/30a10134-f284-472f-a82c-0408f96dcd23\",\"params\":{},\"bannerLink\":\"https://ww\",\"id\":80}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":80}', 0, NULL, '2022-04-25 17:34:28');
INSERT INTO `sys_oper_log` VALUES (248, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/f44f616c-3039-47c7-af72-45352578f376\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":81}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":81}', 0, NULL, '2022-04-25 17:35:43');
INSERT INTO `sys_oper_log` VALUES (249, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/b58c57c2-2dae-4635-866e-51c3d03f1a09\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":82}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":82}', 0, NULL, '2022-04-25 17:36:28');
INSERT INTO `sys_oper_log` VALUES (250, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/0292c286-34fd-4236-8430-04e969bd385d\",\"params\":{},\"bannerLink\":\"https://www.baidu.com\",\"id\":83}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":83}', 0, NULL, '2022-04-25 17:43:30');
INSERT INTO `sys_oper_log` VALUES (251, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/83', '127.0.0.1', '内网IP', '{ids=83}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:43:58');
INSERT INTO `sys_oper_log` VALUES (252, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/9edb4e6a-ce4d-4d2c-8377-153a5d2a7f60\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":84}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":84}', 0, NULL, '2022-04-25 17:44:08');
INSERT INTO `sys_oper_log` VALUES (253, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/478d63ac-887a-4830-a7e3-a53ac24e2d21\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":85}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":85}', 0, NULL, '2022-04-25 17:44:53');
INSERT INTO `sys_oper_log` VALUES (254, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/d070b261-67f7-4718-a588-03c3c1666855\",\"params\":{},\"bannerLink\":\"https://we\",\"id\":86}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":86}', 0, NULL, '2022-04-25 17:45:49');
INSERT INTO `sys_oper_log` VALUES (255, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650880764967,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 17:59:25');
INSERT INTO `sys_oper_log` VALUES (256, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650880834994,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:00:35');
INSERT INTO `sys_oper_log` VALUES (257, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650881021873,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:03:42');
INSERT INTO `sys_oper_log` VALUES (258, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650881308572,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:08:28');
INSERT INTO `sys_oper_log` VALUES (259, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650881824592,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:17:05');
INSERT INTO `sys_oper_log` VALUES (260, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650882224692,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:23:45');
INSERT INTO `sys_oper_log` VALUES (261, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650882412549,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:26:52');
INSERT INTO `sys_oper_log` VALUES (262, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650882745074,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:32:25');
INSERT INTO `sys_oper_log` VALUES (263, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆渝永建设（集团）有限公司\",\"latitude\":29.3663,\"marksId\":105,\"categoryParentId\":12,\"companyProfile\":\"位于永川区昌州大道中段6号\",\"longitude\":105.905,\"categoryColorIndex\":2,\"categoryParentColorIndex\":2,\"updateTime\":1650882778979,\"params\":{},\"companyLevel\":5,\"createTime\":1650776988000,\"categoryId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:32:59');
INSERT INTO `sys_oper_log` VALUES (264, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '127.0.0.1', '内网IP', '{\"bannerType\":2,\"filePath\":\"blob:http://localhost:1024/df8efe87-2614-428c-b8f4-50337fbbaceb\",\"params\":{},\"bannerLink\":\"https://123\",\"id\":87}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":87}', 0, NULL, '2022-04-25 18:43:09');
INSERT INTO `sys_oper_log` VALUES (265, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883412992,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:43:33');
INSERT INTO `sys_oper_log` VALUES (266, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883501788,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:45:04');
INSERT INTO `sys_oper_log` VALUES (267, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883659067,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:47:39');
INSERT INTO `sys_oper_log` VALUES (268, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883691796,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:48:12');
INSERT INTO `sys_oper_log` VALUES (269, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883777688,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:49:38');
INSERT INTO `sys_oper_log` VALUES (270, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883817004,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:50:17');
INSERT INTO `sys_oper_log` VALUES (271, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883862179,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:51:02');
INSERT INTO `sys_oper_log` VALUES (272, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650883871318,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:51:11');
INSERT INTO `sys_oper_log` VALUES (273, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '127.0.0.1', '内网IP', '{\"cityCode\":\"500118\",\"companyName\":\"重庆钰丰钢化玻璃有限公司\",\"latitude\":29.3429,\"marksId\":106,\"categoryParentId\":7,\"companyProfile\":\"重庆钰丰钢化玻璃有限公司\",\"longitude\":105.841,\"categoryColorIndex\":3,\"categoryParentColorIndex\":3,\"updateTime\":1650884027105,\"params\":{},\"companyLevel\":4,\"createTime\":1650777110000,\"categoryId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 18:53:47');
INSERT INTO `sys_oper_log` VALUES (274, '新闻', 1, 'com.ruoyi.web.controller.map.MapNewsController.add()', 'POST', 1, 'test', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"newsContent\":\"<img src=\\\"\\\" />测试\",\"newsId\":15,\"createTime\":1650896704432,\"newsTitle\":\"测试图片\",\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:25:05');
INSERT INTO `sys_oper_log` VALUES (275, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeContent\":\"<p><img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425222803A003.jpg\\\">新版本内容</p>\",\"createBy\":\"admin\",\"createTime\":1648975717000,\"updateBy\":\"admin\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"updateTime\":1650368409000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:28:04');
INSERT INTO `sys_oper_log` VALUES (276, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<img src=\\\"\\\" />测试<img src=\\\"\\\" />\",\"newsId\":15,\"createTime\":1650896704000,\"updateBy\":\"\",\"newsTitle\":\"测试图片\",\"updateTime\":1650897373520,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:36:13');
INSERT INTO `sys_oper_log` VALUES (277, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<img src=\\\"\\\" />\",\"newsId\":15,\"createTime\":1650896704000,\"updateBy\":\"\",\"newsTitle\":\"测试图片\",\"updateTime\":1650897581490,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:39:41');
INSERT INTO `sys_oper_log` VALUES (278, '新闻', 3, 'com.ruoyi.web.controller.map.MapNewsController.remove()', 'DELETE', 1, 'admin', NULL, '/system/news/15', '127.0.0.1', '内网IP', '{newsIds=15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:42:57');
INSERT INTO `sys_oper_log` VALUES (279, '新闻', 1, 'com.ruoyi.web.controller.map.MapNewsController.add()', 'POST', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"newsContent\":\"首页<img src=\\\"\\\" />\",\"newsId\":16,\"createTime\":1650897800510,\"newsTitle\":\"新闻1\",\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:43:20');
INSERT INTO `sys_oper_log` VALUES (280, '新闻', 3, 'com.ruoyi.web.controller.map.MapNewsController.remove()', 'DELETE', 1, 'admin', NULL, '/system/news/16', '127.0.0.1', '内网IP', '{newsIds=16}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:46:49');
INSERT INTO `sys_oper_log` VALUES (281, '新闻', 1, 'com.ruoyi.web.controller.map.MapNewsController.add()', 'POST', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"newsContent\":\"测试图片<img src=\\\"\\\" />\",\"newsId\":17,\"createTime\":1650898027601,\"newsTitle\":\"测试图片\",\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:47:07');
INSERT INTO `sys_oper_log` VALUES (282, '新闻', 3, 'com.ruoyi.web.controller.map.MapNewsController.remove()', 'DELETE', 1, 'admin', NULL, '/system/news/17', '127.0.0.1', '内网IP', '{newsIds=17}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:49:41');
INSERT INTO `sys_oper_log` VALUES (283, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeContent\":\"<p>新版本内容</p><p><img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425225014A002.jpg\\\"></p>\",\"createBy\":\"admin\",\"createTime\":1648975717000,\"updateBy\":\"admin\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"updateTime\":1650896884000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:50:20');
INSERT INTO `sys_oper_log` VALUES (284, '新闻', 1, 'com.ruoyi.web.controller.map.MapNewsController.add()', 'POST', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"newsContent\":\"<p>新版本内容</p><p><img src=\\\\\\\"/dev-api/profile/upload/2022/04/25/head_20220425225014A002.jpg\\\\\\\"></p>\",\"newsId\":18,\"createTime\":1650898326555,\"newsTitle\":\"图片测试\",\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:52:06');
INSERT INTO `sys_oper_log` VALUES (285, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<p>新版本内容</p><p><img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425225014A002.jpg\\\"></p>\",\"newsId\":18,\"createTime\":1650898327000,\"updateBy\":\"\",\"newsTitle\":\"图片测试\",\"updateTime\":1650898499962,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 22:55:00');
INSERT INTO `sys_oper_log` VALUES (286, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<p>新版本内容</p><p><img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425225014A002.jpg\\\"></p>\",\"newsId\":18,\"createTime\":1650898327000,\"updateBy\":\"\",\"newsTitle\":\"图片测试\",\"updateTime\":1650898990381,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 23:03:10');
INSERT INTO `sys_oper_log` VALUES (287, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeContent\":\"<p>1111<img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425230453A001.jpg\\\"></p>\",\"createBy\":\"admin\",\"createTime\":1648975717000,\"updateBy\":\"admin\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"updateTime\":1650898220000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 23:04:55');
INSERT INTO `sys_oper_log` VALUES (288, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<p>新版本内容</p><p><img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425225014A002.jpg\\\"></p>\",\"newsId\":18,\"createTime\":1650898327000,\"updateBy\":\"\",\"newsTitle\":\"图片测试\",\"updateTime\":1650899174051,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 23:06:14');
INSERT INTO `sys_oper_log` VALUES (289, '通知公告', 2, 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice1', '127.0.0.1', '内网IP', '{\"noticeContent\":\"<img src=\\\"\\\" />\",\"createBy\":\"admin\",\"createTime\":1648975717000,\"updateBy\":\"admin\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"updateTime\":1650899095000,\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 23:22:47');
INSERT INTO `sys_oper_log` VALUES (290, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<p>23423<img src=\\\"/dev-api/profile/upload/2022/04/25/logo_20220425233156A001.png\\\"><img src=\\\"/dev-api/profile/upload/2022/04/25/head_20220425233205A002.jpg\\\"></p>\",\"newsId\":18,\"createTime\":1650898327000,\"updateBy\":\"\",\"newsTitle\":\"图片测试\",\"updateTime\":1650900726936,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-25 23:32:07');
INSERT INTO `sys_oper_log` VALUES (291, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '125.84.84.70', 'XX XX', '{\"createBy\":\"admin\",\"newsContent\":\"<p><strong>新版本内容﻿﻿</strong></p><p><strong><span class=\\\"ql-cursor\\\">﻿</span></strong><img src=\\\"/prod-api/profile/upload/2022/04/26/施工18-2_20220426084255A001.jpg\\\"><img src=\\\"\\\"><img src=\\\"\\\"><img src=\\\"\\\"></p>\",\"newsId\":1,\"createTime\":1648975717000,\"updateBy\":\"admin\",\"newsTitle\":\"温馨提醒：2018-07-01 新版本发布啦\",\"remark\":\"管理员\",\"updateTime\":1650933780198,\"params\":{},\"newsType\":\"2\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 08:43:00');
INSERT INTO `sys_oper_log` VALUES (292, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆伍合物流有限公司永川分公司\",\"latitude\":29.328,\"marksId\":91,\"categoryParentId\":17,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProfile\":\"重庆市永川区南大街办事处大南村陈家沟村民小组300号\",\"longitude\":105.885,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"updateTime\":1650934016962,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 08:46:57');
INSERT INTO `sys_oper_log` VALUES (293, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆伍合物流有限公司永川分公司\",\"latitude\":29.328,\"marksId\":91,\"categoryParentId\":17,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProfile\":\"重庆市永川区南大街办事处大南村陈家沟村民小组300号\",\"longitude\":105.885,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"updateTime\":1650934025493,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 08:47:05');
INSERT INTO `sys_oper_log` VALUES (294, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆伍合物流有限公司永川分公司\",\"latitude\":29.328,\"marksId\":91,\"categoryParentId\":17,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProfile\":\"重庆市永川区南大街办事处大南村陈家沟村民小组300号\",\"longitude\":105.885,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"updateTime\":1650934103515,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 08:48:23');
INSERT INTO `sys_oper_log` VALUES (295, '企业管理', 2, 'com.ruoyi.web.controller.map.MapMarksController.edit()', 'PUT', 1, 'admin', NULL, '/system/marks', '125.84.84.70', 'XX XX', '{\"cityCode\":\"500118\",\"companyName\":\"重庆伍合物流有限公司永川分公司\",\"latitude\":29.328,\"marksId\":91,\"categoryParentId\":17,\"actualCapacity\":\"405\",\"companyPhone\":\"18523709710\",\"companyProfile\":\"重庆市永川区南大街办事处大南村陈家沟村民小组300号\",\"longitude\":105.885,\"categoryColorIndex\":0,\"categoryParentColorIndex\":0,\"marketArea\":\"\",\"updateTime\":1650934113387,\"params\":{},\"companyLevel\":3,\"createTime\":1650775246000,\"companyAddress\":\"位于重庆市永川区南大街办事处大南村陈家沟村民小组300号，经营范围包括许可项目：道路货物运输（不含危险货物）， 一般项目：普通货物仓储服务（不含危险化学品等需许可审批的项目），道路货物运输站经营，运输货物打包服务，停车场服务。\",\"companyPrincipal\":\"张三4\",\"categoryId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 08:48:33');
INSERT INTO `sys_oper_log` VALUES (296, '新闻', 2, 'com.ruoyi.web.controller.map.MapNewsController.edit()', 'PUT', 1, 'admin', NULL, '/system/news', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"newsContent\":\"<p>23423</p><p><img src=\\\"/dev-api/profile/upload/2022/04/26/test2_20220426094426A001.jpg\\\"></p>\",\"newsId\":18,\"createTime\":1650898327000,\"updateBy\":\"\",\"newsTitle\":\"图片测试\",\"updateTime\":1650937471270,\"params\":{},\"newsType\":\"0\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 09:44:31');
INSERT INTO `sys_oper_log` VALUES (297, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/84', '183.230.199.61', 'XX XX', '{ids=84}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 21:03:37');
INSERT INTO `sys_oper_log` VALUES (298, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/85,86,87', '183.230.199.61', 'XX XX', '{ids=85,86,87}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 21:03:40');
INSERT INTO `sys_oper_log` VALUES (299, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '183.230.199.61', 'XX XX', '{\"bannerType\":2,\"filePath\":\"blob:http://81.68.190.172:8081/0e7f4791-1168-48a7-8520-164dc35b5abe\",\"params\":{},\"bannerLink\":\"http://www.sohu.com\",\"id\":88}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":88}', 0, NULL, '2022-04-26 21:05:06');
INSERT INTO `sys_oper_log` VALUES (300, '轮播图', 3, 'com.ruoyi.web.controller.map.MapBannerController.remove()', 'DELETE', 1, 'admin', NULL, '/system/banner/88', '183.230.199.61', 'XX XX', '{ids=88}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-04-26 21:05:25');
INSERT INTO `sys_oper_log` VALUES (301, '轮播图', 1, 'com.ruoyi.web.controller.map.MapBannerController.add()', 'POST', 1, 'admin', NULL, '/system/banner', '183.230.199.61', 'XX XX', '{\"bannerType\":2,\"filePath\":\"blob:http://81.68.190.172:8081/c457d947-35df-4e2d-a542-64f470938d2d\",\"params\":{},\"bannerLink\":\"http://zfcxjw.cq.gov.cn/\",\"id\":89}', '{\"msg\":\"操作成功\",\"code\":200,\"data\":89}', 0, NULL, '2022-04-26 21:06:11');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-04-03 16:48:34', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-04-03 16:48:34', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-04-03 16:48:34', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-04-03 16:48:34', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-04-03 16:48:35', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '1', 1, 1, '0', '0', 'admin', '2022-04-03 16:48:35', 'admin', '2022-04-23 20:46:41', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2008);
INSERT INTO `sys_role_menu` VALUES (2, 2009);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2011);
INSERT INTO `sys_role_menu` VALUES (2, 2012);
INSERT INTO `sys_role_menu` VALUES (2, 2013);
INSERT INTO `sys_role_menu` VALUES (2, 2014);
INSERT INTO `sys_role_menu` VALUES (2, 2015);
INSERT INTO `sys_role_menu` VALUES (2, 2016);
INSERT INTO `sys_role_menu` VALUES (2, 2017);
INSERT INTO `sys_role_menu` VALUES (2, 2024);
INSERT INTO `sys_role_menu` VALUES (2, 2025);
INSERT INTO `sys_role_menu` VALUES (2, 2026);
INSERT INTO `sys_role_menu` VALUES (2, 2027);
INSERT INTO `sys_role_menu` VALUES (2, 2028);
INSERT INTO `sys_role_menu` VALUES (2, 2029);
INSERT INTO `sys_role_menu` VALUES (2, 2030);
INSERT INTO `sys_role_menu` VALUES (2, 2031);
INSERT INTO `sys_role_menu` VALUES (2, 2032);
INSERT INTO `sys_role_menu` VALUES (2, 2033);
INSERT INTO `sys_role_menu` VALUES (2, 2034);
INSERT INTO `sys_role_menu` VALUES (2, 2035);
INSERT INTO `sys_role_menu` VALUES (2, 2036);
INSERT INTO `sys_role_menu` VALUES (2, 2038);
INSERT INTO `sys_role_menu` VALUES (2, 2039);
INSERT INTO `sys_role_menu` VALUES (2, 2040);
INSERT INTO `sys_role_menu` VALUES (2, 2041);
INSERT INTO `sys_role_menu` VALUES (2, 2042);
INSERT INTO `sys_role_menu` VALUES (2, 2043);
INSERT INTO `sys_role_menu` VALUES (2, 2044);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '/profile/avatar/2022/04/16/blob_20220416202445A001.jpeg', '$2a$10$zh.sp4NQY4ongVAFQECa3Ojo6ldqETtMDVLRgJNxlK.lwnigflE8y', '0', '0', '127.0.0.1', '2022-04-26 22:06:39', 'admin', '2022-04-03 16:48:34', '', '2022-04-26 22:06:38', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-04-03 16:48:34', 'admin', '2022-04-03 16:48:34', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (100, 100, 'test', '测试用户', '00', '1222@qq.com', '18523709707', '0', '', '$2a$10$V4mas/iFA/WAKe/HMuXJi.jmqjucd5uawp6iWR4LVgXlzmADeRLF.', '0', '0', '183.230.199.58', '2022-04-25 23:46:04', 'admin', '2022-04-18 10:11:34', '', '2022-04-25 23:46:04', NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
INSERT INTO `sys_user_post` VALUES (100, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (100, 2);

SET FOREIGN_KEY_CHECKS = 1;
