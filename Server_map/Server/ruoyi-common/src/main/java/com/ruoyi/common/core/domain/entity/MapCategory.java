package com.ruoyi.common.core.domain.entity;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 企业类别对象 map_category
 *
 * @author cqust_icc
 * @date 2022-04-16
 */
@Data
public class MapCategory extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long categoryId;

    /**
     * 父菜单ID
     */
    private Long parentId;

    private Long colorIndex;
    /**
     * 类别名称
     */
    @Excel(name = "类别名称")
    private String categoryName;

    public Long getParentId() {
        return parentId;
    }

    public MapCategory setParentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }

    /**
     * 类别描述
     */
    @Excel(name = "类别描述")
    private String categoryDescription;

    /**
     * 序号
     */
    @Excel(name = "序号")
    private Long serialNumber;

    private List<MapCategory> children = new ArrayList<MapCategory>();

    public List<MapCategory> getChildren() {
        return children;
    }

    public MapCategory setChildren(List<MapCategory> children) {
        this.children = children;
        return this;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("categoryId", getCategoryId())
                .append("parentId", getParentId())
                .append("categoryName", getCategoryName())
                .append("categoryDescription", getCategoryDescription())
                .append("serialNumber", getSerialNumber())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
