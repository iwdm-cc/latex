package com.ruoyi.common.utils;

import com.ruoyi.common.utils.file.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * @author 张成
 * @version 1.0
 * @date 2022/4/23 13:20
 */
@Component
public class TemplateExportUtil {

    @Value("${ruoyi.templatePath}")
    private String templatePath;

    public void exportExcel(HttpServletResponse response, HttpServletRequest request, String fileName) throws IOException {
        //资源地址
        String DownloadTemplatePath = templatePath + File.separator + fileName;
        FileUtils.writeBytes(DownloadTemplatePath, response.getOutputStream());
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");


    }
}
