import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Cookies from 'js-cookie'
import Element from 'element-ui'
import './assets/styles/element-variables.scss'
import './plugins/element.js'
//导入全局样式表
import './assets/css/global.css'
//导入nprogress
import 'nprogress/nprogress.css'
import './utils/request'
import permission from './directive/permission'
import Pagination from '@/components/Pagination'
import RightToolbar from '@/components/RightToolbar'
//import 'font-awesome/css/font-awesome.min.css'
//自定义全局组件
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
import moment from "moment"
Vue.prototype.$moment = moment;


// 通用下载方法
export function download(fileName) {
  window.location.href = '/common/download?fileName=' + encodeURI(fileName) + '&delete=' + true
}


Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})
Vue.prototype.msgSuccess = function(msg) {
  this.$message({ showClose: true, message: msg, type: 'success' })
}

Vue.prototype.msgError = function(msg) {
  this.$message({ showClose: true, message: msg, type: 'error' })
}

Vue.prototype.msgInfo = function(msg) {
  this.$message.info(msg)
}
// 全局组件挂载
Vue.component('Pagination', Pagination)
Vue.component('RightToolbar', RightToolbar)
// 日期格式化
 function parseTime(time, pattern) {
  if (arguments.length === 0 || !time) {
    return null;
  }
  const format = pattern || "{y}-{m}-{d} {h}:{i}:{s}";
  let date;
  if (typeof time === "object") {
    date = time;
  } else {
    if (typeof time === "string" && /^[0-9]+$/.test(time)) {
      time = parseInt(time);
    } else if (typeof time === "string") {
      time = time.replace(new RegExp(/-/gm), "/");
    }
    if (typeof time === "number" && time.toString().length === 10) {
      time = time * 1000;
    }
    date = new Date(time);
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  };
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key];
    // Note: getDay() returns 0 on Sunday
    if (key === "a") {
      return ["日", "一", "二", "三", "四", "五", "六"][value];
    }
    if (result.length > 0 && value < 10) {
      value = "0" + value;
    }
    return value || 0;
  });
  return time_str;
}

// 表单重置
 function resetForm(refName) {
  if (this.$refs[refName]) {
    this.$refs[refName].resetFields();
  }
}
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.config.productionTip = false
Vue.use(permission)


new Vue({
  router,
  store,

  render: h => h(App)
}).$mount('#app')
