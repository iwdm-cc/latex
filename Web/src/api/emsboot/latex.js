import request from '@/utils/request'

// 查询密切接触列表
export function listMiqie(query) {
  return request({
    url: '/system/file/list',
    method: 'get',
    params: query
  })
}

// 查询密切接触详细
export function getMiqie(id) {
  return request({
    url: '/system/file/' + id,
    method: 'get'
  })
}

// 新增密切接触
export function addMiqie(data) {
  return request({
    url: '/emsboot/miqie',
    method: 'post',
    data: data
  })
}

// 修改密切接触
export function updateMiqie(data) {
  return request({
    url: '/emsboot/miqie',
    method: 'put',
    data: data
  })
}

// 删除密切接触
export function delMiqie(id) {
  return request({
    url: '/system/file/' + id,
    method: 'delete'
  })
}

// 导出密切接触
export function exportMiqie(query) {
  return request({
    url: '/emsboot/miqie/export',
    method: 'get',
    params: query
  })
}
