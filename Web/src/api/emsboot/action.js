import request from '@/utils/request'

// 查询action列表
export function listAction(query) {
  return request({
    url: '/emsboot/action/list',
    method: 'get',
    params: query
  })
}

// 查询action详细
export function getAction(id) {
  return request({
    url: '/emsboot/action/' + id,
    method: 'get'
  })
}

// 新增action
export function addAction(data) {
  return request({
    url: '/emsboot/action',
    method: 'post',
    data: data
  })
}

// 修改action
export function updateAction(data) {
  return request({
    url: '/emsboot/action',
    method: 'put',
    data: data
  })
}

// 删除action
export function delAction(id) {
  return request({
    url: '/emsboot/action/' + id,
    method: 'delete'
  })
}

// 导出action
export function exportAction(query) {
  return request({
    url: '/emsboot/action/export',
    method: 'get',
    params: query
  })
}