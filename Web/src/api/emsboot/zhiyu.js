import request from '@/utils/request'

// 查询列表
export function listZhiyu(query) {
  return request({
    url: '/emsboot/zhiyu/list',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getZhiyu(name) {
  return request({
    url: '/emsboot/zhiyu/' + name,
    method: 'get'
  })
}

// 新增
export function addZhiyu(data) {
  return request({
    url: '/emsboot/zhiyu',
    method: 'post',
    data: data
  })
}

// 修改
export function updateZhiyu(data) {
  return request({
    url: '/emsboot/zhiyu',
    method: 'put',
    data: data
  })
}

// 删除
export function delZhiyu(name) {
  return request({
    url: '/emsboot/zhiyu/' + name,
    method: 'delete'
  })
}

// 导出
export function exportZhiyu(query) {
  return request({
    url: '/emsboot/zhiyu/export',
    method: 'get',
    params: query
  })
}
