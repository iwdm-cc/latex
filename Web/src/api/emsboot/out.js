import request from '@/utils/request'

// 查询外出信息列表
export function listOut(query) {
  return request({
    url: '/emsboot/out/list',
    method: 'get',
    params: query
  })
}

// 查询外出信息详细
export function getOut(id) {
  return request({
    url: '/emsboot/out/' + id,
    method: 'get'
  })
}

// 新增外出信息
export function addOut(data) {
  return request({
    url: '/emsboot/out',
    method: 'post',
    data: data
  })
}

// 修改外出信息
export function updateOut(data) {
  return request({
    url: '/emsboot/out',
    method: 'put',
    data: data
  })
}

// 删除外出信息
export function delOut(id) {
  return request({
    url: '/emsboot/out/' + id,
    method: 'delete'
  })
}

// 导出外出信息
export function exportOut(query) {
  return request({
    url: '/emsboot/out/export',
    method: 'get',
    params: query
  })
}