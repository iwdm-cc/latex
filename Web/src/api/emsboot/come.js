import request from '@/utils/request'

// 查询来访者信息列表
export function listCome(query) {
  return request({
    url: '/emsboot/come/list',
    method: 'get',
    params: query
  })
}

// 查询来访者信息详细
export function getCome(id) {
  return request({
    url: '/emsboot/come/' + id,
    method: 'get'
  })
}

// 新增来访者信息
export function addCome(data) {
  return request({
    url: '/emsboot/come',
    method: 'post',
    data: data
  })
}

// 修改来访者信息
export function updateCome(data) {
  return request({
    url: '/emsboot/come',
    method: 'put',
    data: data
  })
}

// 删除来访者信息
export function delCome(id) {
  return request({
    url: '/emsboot/come/' + id,
    method: 'delete'
  })
}

// 导出来访者信息
export function exportCome(query) {
  return request({
    url: '/emsboot/come/export',
    method: 'get',
    params: query
  })
}