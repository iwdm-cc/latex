import request from '@/utils/request'

// 查询确诊列表
export function listQuezhen(query) {
  return request({
    url: '/emsboot/quezhen/list',
    method: 'get',
    params: query
  })
}

// 查询确诊详细
export function getQuezhen(id) {
  return request({
    url: '/emsboot/quezhen/' + id,
    method: 'get'
  })
}

// 新增确诊
export function addQuezhen(data) {
  return request({
    url: '/emsboot/quezhen',
    method: 'post',
    data: data
  })
}

// 修改确诊
export function updateQuezhen(data) {
  return request({
    url: '/emsboot/quezhen',
    method: 'put',
    data: data
  })
}

// 删除确诊
export function delQuezhen(id) {
  return request({
    url: '/emsboot/quezhen/' + id,
    method: 'delete'
  })
}

// 导出确诊
export function exportQuezhen(query) {
  return request({
    url: '/emsboot/quezhen/export',
    method: 'get',
    params: query
  })
}