import request from '@/utils/request'

//权限列表
export function updateUser(data) {
  return request({
    url: '/employee/updateUser',
    method: 'put',
    data: data
  })
}

//权限列表
export function addUr(url) {
  return request({
    url: url,
    method: 'put'
  })
}

//权限列表
export function editRole(data) {
  return request({
    url: '/staff/editRole',
    method: 'put',
    data: data
  })
}

//权限列表
export function updateEnabled(userInfo) {
  return request({
    url: '/employee/updateEnabled/' + userInfo.id + '/' + userInfo.enabled,
    method: 'put'
  })
}

//权限列表
export function allRole() {
  return request({
    url: '/employee/allRole',
    method: 'get'

  })
}

//权限列表
export function deleteUser(url) {
  return request({
    url: url,
    method: 'delete'

  })
}

//权限列表
export function powList(query) {
  return request({
    url: '/staff/powList/',
    method: 'get',
    params: query
  })
}

//角色列表
export function roleList(query) {
  return request({
    url: '/staff/roleList',
    method: 'get',
    params: query
  })
}

//用户列表
export function userList(query) {
  return request({
    url: '/employee/userList/',
    method: 'get',
    params: query
  })
}

export function login1(data) {

  return request({
    url: '/login1',
    method: 'post',
    data: data
  })
}

export function addUser(data) {
  return request({
    url: '/employee/addUser/',
    method: 'post',
    data: data
  })
}
export function addRolePost(data) {
  return request({
    url: '/staff/addRole',
    method: 'post',
    data: data
  })
}

export function assPow(data) {

  return request({
    url: '/staff/assPow',
    method: 'post',
    data: data
  })
}

export function deletePow(url) {
  return request({
    url: url,
    method: 'delete'
  })
}
export function deleteRole(url) {
  return request({
    url: url,
    method: 'delete'
  })
}

export function changeMenuEnable(url) {
  return request({
    url: url,
    method: 'put'
  })
}

export function updatePwd(data) {
  return request({
    url: '/updatePwd',
    method: 'get',
    params: data
  })
}

export function menuList() {
  return request({
    url: '/menuList',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'get'
  })
}
