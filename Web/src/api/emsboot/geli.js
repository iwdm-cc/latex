import request from '@/utils/request'

// 查询隔离列表
export function listGeli(query) {
  return request({
    url: '/emsboot/geli/list',
    method: 'get',
    params: query
  })
}

// 查询隔离详细
export function getGeli(id) {
  return request({
    url: '/emsboot/geli/' + id,
    method: 'get'
  })
}

// 新增隔离
export function addGeli(data) {
  return request({
    url: '/emsboot/geli',
    method: 'post',
    data: data
  })
}

// 修改隔离
export function updateGeli(data) {
  return request({
    url: '/emsboot/geli',
    method: 'put',
    data: data
  })
}

// 删除隔离
export function delGeli(id) {
  return request({
    url: '/emsboot/geli/' + id,
    method: 'delete'
  })
}

// 导出隔离
export function exportGeli(query) {
  return request({
    url: '/emsboot/geli/export',
    method: 'get',
    params: query
  })
}