import request from '@/utils/request'

// 查询物资信息列表
export function listSwuzi(query) {
  return request({
    url: '/emsboot/swuzi/list',
    method: 'get',
    params: query
  })
}

// 查询物资信息详细
export function getSwuzi(id) {
  return request({
    url: '/emsboot/swuzi/' + id,
    method: 'get'
  })
}

// 新增物资信息
export function addSwuzi(data) {
  return request({
    url: '/emsboot/swuzi',
    method: 'post',
    data: data
  })
}

// 修改物资信息
export function updateSwuzi(data) {
  return request({
    url: '/emsboot/swuzi',
    method: 'put',
    data: data
  })
}

// 删除物资信息
export function delSwuzi(id) {
  return request({
    url: '/emsboot/swuzi/' + id,
    method: 'delete'
  })
}

// 导出物资信息
export function exportSwuzi(query) {
  return request({
    url: '/emsboot/swuzi/export',
    method: 'get',
    params: query
  })
}