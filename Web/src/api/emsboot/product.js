import request from '@/utils/request'

// 查询仓库信息列表
export function listProduct(query) {
  return request({
    url: '/emsboot/product/list',
    method: 'get',
    params: query
  })
}

// 查询仓库信息详细
export function getProduct(id) {
  return request({
    url: '/emsboot/product/' + id,
    method: 'get'
  })
}

// 新增仓库信息
export function addProduct(data) {
  return request({
    url: '/emsboot/product',
    method: 'post',
    data: data
  })
}

// 修改仓库信息
export function updateProduct(data) {
  return request({
    url: '/emsboot/product',
    method: 'put',
    data: data
  })
}

// 删除仓库信息
export function delProduct(id) {
  return request({
    url: '/emsboot/product/' + id,
    method: 'delete'
  })
}

// 导出仓库信息
export function exportProduct(query) {
  return request({
    url: '/emsboot/product/export',
    method: 'get',
    params: query
  })
}