import request from '@/utils/request'

// 查询notice列表
export function listNotice(query) {
  return request({
    url: '/emsboot/notice/list',
    method: 'get',
    params: query
  })
}

// 查询notice详细
export function getNotice(id) {
  return request({
    url: '/emsboot/notice/' + id,
    method: 'get'
  })
}

// 新增notice
export function addNotice(data) {
  return request({
    url: '/emsboot/notice',
    method: 'post',
    data: data
  })
}

// 修改notice
export function updateNotice(data) {
  return request({
    url: '/emsboot/notice',
    method: 'put',
    data: data
  })
}

// 删除notice
export function delNotice(id) {
  return request({
    url: '/emsboot/notice/' + id,
    method: 'delete'
  })
}

// 导出notice
export function exportNotice(query) {
  return request({
    url: '/emsboot/notice/export',
    method: 'get',
    params: query
  })
}