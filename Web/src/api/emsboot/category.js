import request from '@/utils/request'

// 查询仓库信息列表
export function listCategory(query) {
  return request({
    url: '/emsboot/category/list',
    method: 'get',
    params: query
  })
}

// 查询仓库信息详细
export function getCategory(id) {
  return request({
    url: '/emsboot/category/' + id,
    method: 'get'
  })
}

// 新增仓库信息
export function addCategory(data) {
  return request({
    url: '/emsboot/category',
    method: 'post',
    data: data
  })
}

// 修改仓库信息
export function updateCategory(data) {
  return request({
    url: '/emsboot/category',
    method: 'put',
    data: data
  })
}

// 删除仓库信息
export function delCategory(id) {
  return request({
    url: '/emsboot/category/' + id,
    method: 'delete'
  })
}

// 导出仓库信息
export function exportCategory(query) {
  return request({
    url: '/emsboot/category/export',
    method: 'get',
    params: query
  })
}