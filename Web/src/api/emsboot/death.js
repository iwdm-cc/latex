import request from '@/utils/request'

// 查询死者信息列表
export function listDeath(query) {
  return request({
    url: '/emsboot/death/list',
    method: 'get',
    params: query
  })
}

// 查询死者信息详细
export function getDeath(id) {
  return request({
    url: '/emsboot/death/' + id,
    method: 'get'
  })
}

// 新增死者信息
export function addDeath(data) {
  return request({
    url: '/emsboot/death',
    method: 'post',
    data: data
  })
}

// 修改死者信息
export function updateDeath(data) {
  return request({
    url: '/emsboot/death',
    method: 'put',
    data: data
  })
}

// 删除死者信息
export function delDeath(id) {
  return request({
    url: '/emsboot/death/' + id,
    method: 'delete'
  })
}

// 导出死者信息
export function exportDeath(query) {
  return request({
    url: '/emsboot/death/export',
    method: 'get',
    params: query
  })
}