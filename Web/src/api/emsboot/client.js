import request from '@/utils/request'

// 查询1列表
export function listClient(query) {
  return request({
    url: '/emsboot/client/list',
    method: 'get',
    params: query
  })
}

// 查询1详细
export function getClient(id) {
  return request({
    url: '/emsboot/client/' + id,
    method: 'get'
  })
}

// 新增1
export function addClient(data) {
  return request({
    url: '/emsboot/client',
    method: 'post',
    data: data
  })
}

// 修改1
export function updateClient(data) {
  return request({
    url: '/emsboot/client',
    method: 'put',
    data: data
  })
}

// 删除1
export function delClient(id) {
  return request({
    url: '/emsboot/client/' + id,
    method: 'delete'
  })
}

// 导出1
export function exportClient(query) {
  return request({
    url: '/emsboot/client/export',
    method: 'get',
    params: query
  })
}