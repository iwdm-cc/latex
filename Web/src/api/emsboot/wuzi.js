import request from '@/utils/request'

// 查询wuzi列表
export function listWuzi(query) {
  return request({
    url: '/emsboot/wuzi/list',
    method: 'get',
    params: query
  })
}

// 查询wuzi详细
export function getWuzi(classify) {
  return request({
    url: '/emsboot/wuzi/' + classify,
    method: 'get'
  })
}

// 新增wuzi
export function addWuzi(data) {
  return request({
    url: '/emsboot/wuzi',
    method: 'post',
    data: data
  })
}

// 修改wuzi
export function updateWuzi(data) {
  return request({
    url: '/emsboot/wuzi',
    method: 'put',
    data: data
  })
}

// 删除wuzi
export function delWuzi(classify) {
  return request({
    url: '/emsboot/wuzi/' + classify,
    method: 'delete'
  })
}

// 导出wuzi
export function exportWuzi(query) {
  return request({
    url: '/emsboot/wuzi/export',
    method: 'get',
    params: query
  })
}