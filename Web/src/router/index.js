import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login'
import Home from '../components/Home'
import UserList from '../components/user/userList'
import PowList from '../components/power/PowList'
import RoleList from '../components/power/RoleList'
import Enter from '../components/warehouse/Enter'
import Check from '../components/warehouse/Check'

import client from '../views/emsboot/client'
import category from '../views/emsboot/category'
import product from '../views/emsboot/product'
import warehouse from '../views/emsboot/warehouse'
import Welcome from '../components/Welcome'
import order from '../views/emsboot/order/index'
import orderout from '../views/emsboot/order/out'

Vue.use(VueRouter)
const whiteRouter = ['/latex/index','/login'];
const routes = [{ path: '/', redirect: '/home' }, { path: '/login', component: Login }, {
  path: '/latex/index', component: () => import('../views/emsboot/latex/index')
}, {
  path: '/home', component: Home, redirect: '/welcome', children: [{
    path: '/welcome', component: () => import('../views/emsboot/latexlist/index')
  }, {
    path: '/userList', component: UserList
  }, {
    path: '/powList', component: PowList
  }, {
    path: '/roleList', component: RoleList
  }, {
    path: '/enter', component: Enter
  }, {
    path: '/orderout', component: orderout
  }, {
    path: '/check', component: Check
  }, {
    path: '/category', component: category
  }, {
    path: '/product', component: product
  }, {
    path: '/clientList', component: client
  }, {
    path: '/warehouse', component: warehouse
  }, {
    path: '/order', component: order
  }, {
    path: '/emsboot/come/index', component: () => import('../views/emsboot/come/index')
  }, {
    path: '/emsboot/zhiyu/index', component: () => import('../views/emsboot/zhiyu/index')
  }
    , {
      path: '/emsboot/swuzi/index', component: () => import('../views/emsboot/swuzi/index')
    }, {
      path: '/emsboot/wuzi/index', component: () => import('../views/emsboot/wuzi/index')
    }
    , {
      path: '/emsboot/quezhen/index', component: () => import('../views/emsboot/quezhen/index')
    }, {
      path: '/emsboot/out/index', component: () => import('../views/emsboot/out/index')
    },  {
      path: '/emsboot/geli/index', component: () => import('../views/emsboot/geli/index')
    }, {
      path: '/emsboot/death/index', component: () => import('../views/emsboot/death/index')
    }, {
      path: '/emsboot/action/index', component: () => import('../views/emsboot/action/index')
    }, {
      path: '/emsboot/message/index', component: () => import('../views/emsboot/message/index')
    }, {
      path: '/emsboot/notice/index', component: () => import('../views/emsboot/notice/index')
    }

  ]
}]

const router = new VueRouter({
  routes
})
//挂载路由导航守卫
router.beforeEach((to, from, next) => {
  //to将要访问的路径
  //from代表从哪个路径跳转过来
  //next是一个函数表示放行
  //next()放行  next('/login') 跳转到登登录页

  if (whiteRouter.indexOf(to.path) !== -1) {  //当to.path == '/login' 的时候，存在，执行next(),跳到login页面,不触发beforeEach
    next(); //指向(进入)to钩子--进入下一个页面,to.path == '/login'
  }else{
    //获取用户数据
    const token = window.sessionStorage.getItem('token')
    if (!token) return next('/login')
    next()
  }



})
export default router
