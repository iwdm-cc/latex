package com.iwdm.latex;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.iwdm.latex.bean.JsonRootBean;
import com.iwdm.latex.bean.Location;
import com.iwdm.latex.bean.Words_result;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    //线程池
    private final Executor executor = Executors.newSingleThreadExecutor();
    private ImageView imageView;
    private TextView text;
    private Toast toast;
    private List<Words_result> words_result;
    private String originalPath;
    private String result;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.image);
        text = findViewById(R.id.text);
        originalPath = getIntent().getStringExtra("originalPath");


        imageView.setOnTouchListener(this);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap = Utils.getLocalBitmap(originalPath).copy(Bitmap.Config.ARGB_8888, true);
                Canvas canvas = new Canvas(bitmap);
                result = Formula.formula(originalPath);
                Gson gson = new Gson();
                JsonRootBean jsonRootBean = gson.fromJson(result, JsonRootBean.class);
                words_result = jsonRootBean.getWords_result();
                for (Words_result wordsResult : words_result) {
                    Paint red = new Paint();
                    red.setColor(Color.RED);
                    red.setStyle(Paint.Style.STROKE);
                    Location location = wordsResult.getLocation();
                    canvas.drawRect(location.getLeft(),
                            location.getTop(),
                            location.getLeft() + location.getWidth(),
                            location.getTop() + location.getHeight(), red);
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);
                    }
                });

            }
        });

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {// 手机离开屏幕对应的事件
            float startX = event.getX();
            float startY = event.getY();
            getInfo(startX, startY);
            text.setText(getInfo(startX, startY));
        }
        return true;
    }

    private String getInfo(float startX, float startY) {
        for (Words_result wordsResult : words_result) {
            Location location = wordsResult.getLocation();
            if (startX >= location.getLeft() && startX <= (location.getLeft() + location.getWidth())) {
                if (startY >= location.getTop() && startY <= (location.getTop() + location.getHeight())) {
                    return wordsResult.getWords();
                }
            }
        }
        return "未识别";
    }

    public void commitPic(View view) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    new ImageUpload().uploadImage(result, new File(originalPath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "上传成功~", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}

