package com.iwdm.latex;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ImageUpload{

    //1.创建对应的MediaType
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    private final OkHttpClient client = new OkHttpClient();

    public void uploadImage(String result, File file) throws  IOException {


        //2.创建RequestBody
        RequestBody fileBody = RequestBody.create(MEDIA_TYPE_PNG, file);


        SimpleDateFormat simpleFormatter = new SimpleDateFormat("yyyy-MM-dd HHmmss");
        //3.构建MultipartBody
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", simpleFormatter.format(new Date())+".png", fileBody)
                .addFormDataPart("result", result)
                .build();

        //4.构建请求
        Request request = new Request.Builder()
                .url("http://81.68.190.172:8083/system/file/latex")
                .post(requestBody)
                .build();

        //5.发送请求
        Response response = client.newCall(request).execute();
    }
}
